﻿#ifndef ADDITEM_H
#define ADDITEM_H

#include <head/g_pch.h>
#include <head/db_helper.h>
#include "m_fhs.h"
#include "head/m_message.h"

namespace Ui {
class AddItem;
}

class AddItem : public QDialog
{
    Q_OBJECT

public:
    explicit AddItem(QWidget *parent,int id=-1);
    ~AddItem();

    void updateTopic();

    void setTopic(const QString &topic);

    void setItem(const QString &topic,
                 const QString &label,
                 const QString &detail,
                 const QString &alias);

    bool addItem(const QString &topic,
                 const QString &label,
                 const QString &alias,
                 const QString &detail);

    bool updateItem(const QString &topic,
                    const QString &label,
                    const QString &alias,
                    const QString &detail);

private slots:
    void on_btn_paste_clicked();

    void on_topics_currentTextChanged(const QString &text);

    void on_btn_submit_clicked();

private:
    Ui::AddItem *ui;

    int id_;
    QString table;
    QString topic_,label_,detail_,alias_;
};

#endif // ADDITEM_H
