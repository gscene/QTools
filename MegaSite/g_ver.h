﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "MegaSite"
#define SP_VER 1007
#define SP_UID "{6c5e802d-c7cf-4bb1-858b-2ffbcd43fb6e}"
#define SP_TYPE "prop"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "MegaSite.ini"
#define SP_INFO "MegaSite.json"
#define SP_LINK "MegaSite.lnk"

/*
 * 1002 基本功能
 * 1003 TrayWidget,扩展功能
 * 1004 修正了删除的bug
 * 1005 quickConnectFromSHM
 * 1006 label,alias,detail
 * 1007 addItem,updateItem
 */

#endif // G_VER_H
