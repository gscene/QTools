﻿#include "additem.h"
#include "ui_additem.h"

AddItem::AddItem(QWidget *parent, int id) :
    QDialog(parent),
    ui(new Ui::AddItem)
{
    ui->setupUi(this);
    table=P_SITE;
    id_=id;
    if(id_ != -1)   // 更新模式
    {
        setWindowTitle(QStringLiteral("更新条目"));
        ui->btn_submit->setText(QStringLiteral("保存"));
        ui->btn_submit->setAutoDefault(true);
    }
    else
        ui->btn_paste->setAutoDefault(true);

    updateTopic();

}

AddItem::~AddItem()
{
    delete ui;
}

void AddItem::updateTopic()
{
    if(ui->topics->count() != 0)
        ui->topics->clear();

    auto items=sp_getTopic(table);
    if(!items.isEmpty())
        ui->topics->addItems(items);
    ui->topics->addItem(MANUAL_INPUT);
    ui->topics->setCurrentIndex(-1);
}

void AddItem::setTopic(const QString &topic)
{
    topic_ = topic;
    ui->topics->setCurrentText(topic);
}

void AddItem::setItem(const QString &topic,
                      const QString &label,
                      const QString &detail,
                      const QString &alias)
{
    topic_ = topic;
    label_ = label;
    detail_ = detail;
    alias_= alias;

    ui->topics->setCurrentText(topic);
    ui->label->setText(label);
    ui->detail->setText(detail);
    ui->alias->setText(alias);
}

bool AddItem::addItem(const QString &topic,
                      const QString &label,
                      const QString &alias,
                      const QString &detail)
{
    QSqlQuery query;
    query.prepare(QString("insert into %1 (topic,label,alias,detail) values (?,?,?,?)")
                  .arg(table));
    query.addBindValue(topic);
    query.addBindValue(label);
    query.addBindValue(alias);
    query.addBindValue(detail);
    if(query.exec())
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

bool AddItem::updateItem(const QString &topic,
                         const QString &label,
                         const QString &alias,
                         const QString &detail)
{
    QSqlQuery query;
    QString sql=QString("update %1 set topic='%2',label='%3',alias='%4',detail='%5' where id=%6")
            .arg(table)
            .arg(topic)
            .arg(label)
            .arg(alias)
            .arg(detail)
            .arg(id_);
    if(query.exec(sql))
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void AddItem::on_topics_currentTextChanged(const QString &text)
{
    if(text == MANUAL_INPUT)
    {
        ui->topic->clear();
        ui->topic->setEnabled(true);
    }
    else
    {
        ui->topic->setText(text);
        ui->topic->setEnabled(false);
    }
}

void AddItem::on_btn_submit_clicked()
{
    QString topic=ui->topic->text().trimmed();
    if(topic.isEmpty())
        topic=DEF_TOPIC;

    QString label=ui->label->text().trimmed();
    if(label.isEmpty())
    {
        MESSAGE_LABEL_EMPTY
    }

    QString alias=ui->alias->text().trimmed();

    QString detail=ui->detail->text().trimmed();
    if(detail.isEmpty())
    {
        MESSAGE_DETAIL_EMPTY
    }

    if(id_ != -1)
    {
        if(topic_ ==topic && label_ == label && detail_ == detail && alias_ == alias)
        {
            MESSAGE_NOT_CHANGE
        }
        else
        {
            if(updateItem(topic,label,alias,detail))
                accept();
            else
                MESSAGE_CANNOT_UPDATE
        }
    }
    else
    {
        if(addItem(topic,label,alias,detail))
            accept();
        else
            MESSAGE_CANNOT_SUBMIT
    }
}

void AddItem::on_btn_paste_clicked()
{
    QString text=qApp->clipboard()->text();
    if(!text.isEmpty())
        ui->detail->setText(text);
}
