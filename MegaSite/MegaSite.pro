#-------------------------------------------------
#
# Project created by QtCreator 2018-06-21T21:41:52
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MegaSite
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
        main.cpp \
        widget.cpp \
    additem.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    additem.h \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/common/traywidget.h

FORMS += \
        widget.ui \
    additem.ui

RESOURCES += \
    res.qrc

RC_ICONS = click_cloud.ico
