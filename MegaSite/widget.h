﻿#ifndef WIDGET_H
#define WIDGET_H

#include <common/traywidget.h>
#include "additem.h"
#include "g_ver.h"

namespace Ui {
class Widget;
}

class Widget : public TrayWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void generateMenu();
    void updateTopicView();
    void updateView();
    void do_addItem();
    void do_updateItem();
    void do_removeItem();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_topicView_clicked(const QModelIndex &index);
    void on_labelView_doubleClicked(const QModelIndex &index);
    void on_kw_returnPressed();

private:
    Ui::Widget *ui;

    QString table;
    QString selectTopic;

    QStringListModel *topicModel;
    QStringListModel *labelModel;
};

#endif // WIDGET_H
