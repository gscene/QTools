﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    TrayWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    table=P_SITE;
    topicModel=new QStringListModel(this);
    ui->topicView->setModel(topicModel);

    labelModel=new QStringListModel(this);
    ui->labelView->setModel(labelModel);

    updateTopicView();
    createTray(QIcon(":/click_cloud.ico"),SP_VERSION);
    createMenu();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::generateMenu()
{
    menu->addAction(QStringLiteral("添加... (F1)"),this,&Widget::do_addItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("编辑... (F3)"),this,&Widget::do_updateItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新主题 (F4)"),this,&Widget::updateTopicView);
    menu->addSeparator();
    menu->addAction(QStringLiteral("删除 (Delete)"),this,&Widget::do_removeItem);
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_F1:
        do_addItem();
        break;
    case Qt::Key_F3:
        do_updateItem();
        break;
    case Qt::Key_F4:
        updateTopicView();
        break;
    case Qt::Key_Delete:
        do_removeItem();
        break;
    case Qt::Key_Escape:
        close();
        break;
    }
}

void Widget::do_addItem()
{
    AddItem item(this);
    if(!selectTopic.isEmpty())
        item.setTopic(selectTopic);

    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Widget::do_updateItem()
{
    if(selectTopic.isEmpty())
        return;

    QModelIndex index=ui->labelView->currentIndex();
    if(index.isValid())
    {
        QString label=index.data().toString();
        int id=sp_getIdByTopic(table,selectTopic,label);
        QString detail=sp_getItemById("detail",table,id).toString();
        QString alias=sp_getItemById("alias",table,id).toString();

        AddItem item(this,id);
        item.setItem(selectTopic,label,detail,alias);
        if(item.exec() == QDialog::Accepted)
            updateView();
    }
}

void Widget::do_removeItem()
{
    if(selectTopic.isEmpty())
        return;

    QModelIndex index=ui->labelView->currentIndex();
    if(index.isValid())
    {
        QMessageBox::StandardButton result=MESSAGE_DELETE_CONFIRM
                if(result == QMessageBox::Yes)
        {
                QString label=index.data().toString();
                if(sp_removeItemByTopic(table,selectTopic,label))
                updateView();
                else
                MESSAGE_CANNOT_DELETE
    }
    }
}

void Widget::updateTopicView()
{
    if(topicModel->rowCount() != 0)
        topicModel->setStringList(QStringList());

    auto items=sp_getTopic(table);
    if(!items.isEmpty())
    {
        items.removeOne(PRE_REF);
        topicModel->setStringList(items);
    }
}

void Widget::updateView()
{
    if(labelModel->rowCount() != 0)
        labelModel->setStringList(QStringList());

    if(selectTopic.isEmpty())
        return;

    auto items=sp_getItemsByTopic("label",table,selectTopic);
    if(!items.isEmpty())
        labelModel->setStringList(items);
}

void Widget::on_topicView_clicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        selectTopic=index.data().toString();
        updateView();
    }
}

void Widget::on_labelView_doubleClicked(const QModelIndex &index)
{
    if(selectTopic.isEmpty())
        return;

    if(index.isValid())
    {
        QString label=index.data().toString();
        QString detail=sp_getItemByTopic("detail",table,selectTopic,label).toString();
        if(!detail.isEmpty())
            QDesktopServices::openUrl(QUrl(detail));
    }
}

void Widget::on_kw_returnPressed()
{
    QString kw=ui->kw->text().trimmed();
    if(kw.isEmpty())
        return;

    QString detail=sp_findItem("detail",table,"alias",kw);
    if(!detail.isEmpty())
    {
        QDesktopServices::openUrl(QUrl(detail));
        return;
    }

    detail=sp_getItem("detail",table,"label",kw).toString();
    if(!detail.isEmpty())
    {
        QDesktopServices::openUrl(QUrl(detail));
        return;
    }

    QDesktopServices::openUrl(QUrl(BAIDU_PREFIX + kw));
}
