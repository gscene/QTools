#ifndef PLAYLISTDIALOG_H
#define PLAYLISTDIALOG_H

#include <head/g_pch.h>

namespace Ui {
class PlaylistDialog;
}

class PlaylistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PlaylistDialog(QWidget *parent = nullptr);
    ~PlaylistDialog();

    void createMenu();
    void do_remove();
    void do_playPlaylist();
    void onPlaylistChanged(const QStringList &list);

signals:
    void playAt(int pos);
    void removeAt(int pos);
    void playPlaylist();

private slots:
    void on_listView_doubleClicked(const QModelIndex &index);

protected:
    void contextMenuEvent(QContextMenuEvent *);

private:
    Ui::PlaylistDialog *ui;

    QMenu *menu;
    QStringListModel *listModel;
};

#endif // PLAYLISTDIALOG_H
