#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "MusicFolder"
#define SP_VER 1015
#define SP_UID "{c9bd6c4d-e517-4b4e-a5e0-c34a3264e848}"
#define SP_TYPE "shell"

#define SP_CFG "MusicFolder.ini"
#define SP_INFO "MusicFolder.json"
#define SP_LINK "MusicFolder.lnk"

#endif // G_VER_H
