#include "playlistdialog.h"
#include "ui_playlistdialog.h"

PlaylistDialog::PlaylistDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlaylistDialog)
{
    ui->setupUi(this);

    listModel=new QStringListModel(this);
    ui->listView->setModel(listModel);

    createMenu();
}

PlaylistDialog::~PlaylistDialog()
{
    delete ui;
}

void PlaylistDialog::createMenu()
{
    menu=new QMenu(this);
    menu->addAction("列表播放",this,&PlaylistDialog::do_playPlaylist);
    menu->addSeparator();
    menu->addAction("删除",this,&PlaylistDialog::do_remove);
}

void PlaylistDialog::contextMenuEvent(QContextMenuEvent *)
{
    menu->exec(QCursor::pos());
}

void PlaylistDialog::onPlaylistChanged(const QStringList &list)
{
    listModel->setStringList(list);
}

void PlaylistDialog::on_listView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
        emit playAt(index.row());
}

void PlaylistDialog::do_remove()
{
    QModelIndex index=ui->listView->currentIndex();
    if(index.isValid())
        emit removeAt(index.row());
}

void PlaylistDialog::do_playPlaylist()
{
    if(listModel->rowCount() > 0)
        emit playPlaylist();
}