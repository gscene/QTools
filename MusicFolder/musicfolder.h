#ifndef MUSICFOLDER_H
#define MUSICFOLDER_H

#include "mediafolder.h"
#include "element/mediaplayer.h"
#include "playlistdialog.h"

namespace Ui {
class MusicFolder;
}

class MusicFolder : public MediaFolder
{
    Q_OBJECT

public:
    explicit MusicFolder(QWidget *parent = 0);
    ~MusicFolder();

    void createMenu();
    void updateView();
    void addToFavorite();
    void addToPlaylist();
    void toggleEscape();

    void play(const QFileInfo &fileInfo);
    void playNext();
    void playPrevious();

    void statusChanged(QMediaPlayer::MediaStatus status);
    void seekableChanged(bool seekable);
    void positionChanged(qint64 position);
    void currentMediaChanged(const QMediaContent &media);

    void setPlayMode_TrackOnce();
    void setPlayMode_TrackLoop();
    void setPlayMode_ListSequence();
    void setPlayMode_ListLoop();
    void setPlayMode_ListRandom();

    void pl_pick();
    void pl_add();
    void pl_onPlayPlaylist();
    void pl_save();
    void showPlaylistDialog();

signals:
    void playlistChanged(const QStringList &list);

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void error(QMediaPlayer::Error error);
    void on_treeView_clicked(const QModelIndex &index);
    void on_listView_doubleClicked(const QModelIndex &index);
    void on_btn_continue_clicked();
    void on_slider_sliderMoved(int position);

private:
    Ui::MusicFolder *ui;

    bool isFolderPlayer;    // 文件夹播放器和列表播放器
    bool isFirstBoot;    //第一次启动

    MediaPlayer *player;
    QFileInfo locator;
    QFileInfoList fileList;
    QString playlistFile;
    PlaylistDialog *playlistDialog;
};

#endif // MUSICFOLDER_H
