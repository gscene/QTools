#-------------------------------------------------
#
# Project created by QtCreator 2015-12-26T10:50:13
#
#-------------------------------------------------

QT       += core gui sql network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = MusicFolder
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

SOURCES += main.cpp\
        musicfolder.cpp \
    ../../elfproj/support/sp_env.cpp \
    playlistdialog.cpp

HEADERS  += musicfolder.h \
    g_ver.h \
    ../../elfproj/support/mysql_helper.h \
    ../common/m_fhs.h \
    ../../elfproj/support/sp_env.h \
    ../common/mediafolder.h \
    playlistdialog.h \
    ../../elfproj/element/mediaplayer.h

FORMS    += musicfolder.ui \
    playlistdialog.ui

RESOURCES += \
    res.qrc

RC_ICONS = music.ico
