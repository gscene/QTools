﻿#ifndef ADDITEM_H
#define ADDITEM_H

#include "ui_additem.h"
#include <head/g_pch.h>
#include <head/db_helper.h>
#include <head/m_message.h>
#include "m_fhs.h"

class AddItem : public QDialog, private Ui::AddItem
{
    Q_OBJECT

public:
    explicit AddItem(QWidget *parent = nullptr);

    void updateTopic();
    void updateLabel();
    void setTopic(const QString &topic);
    void setLabel(const QString &label);
    bool addItem(const QString &topic,
                 const QString &label,
                 double detail,
                 const QString &addition);

private slots:
    void on_topics_currentTextChanged(const QString &text);
    void on_labels_currentTextChanged(const QString &text);
    void on_btn_pasteLabel_clicked();
    void on_btn_pasteAddition_clicked();
    void on_btn_submit_clicked();

private:
    QString table;
    QString selectTopic;
};

#endif // ADDITEM_H
