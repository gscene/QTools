#-------------------------------------------------
#
# Project created by QtCreator 2018-10-17T16:21:27
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PriceTrend
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

CONFIG += c++11

SOURCES += \
        main.cpp \
        widget.cpp \
    additem.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    additem.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/common/baseeditor.h

FORMS += \
        widget.ui \
    additem.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc

RC_ICONS = trend.ico
