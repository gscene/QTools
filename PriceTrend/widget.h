﻿#ifndef WIDGET_H
#define WIDGET_H

#include "ui_widget.h"
#include "common/baseeditor.h"
#include "additem.h"

class Widget : public BaseEditor, private Ui::Widget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);

    void generateMenu();
    void updateTopic();
    void updateView();
    void newItem();
    void openUrl();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_topics_currentTextChanged(const QString &text);
    void on_listView_doubleClicked(const QModelIndex &index);

private:
    QStringListModel *listModel;
    QString selectTopic,selectLabel;
};

#endif // WIDGET_H
