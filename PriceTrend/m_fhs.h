﻿#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"

#define TD_TREND "td_price_trend"
#define MANUAL_INPUT QStringLiteral("-手动输入-")
#define DEF_WIDTH 480

#define ID_COL 0    //ID
#define TOP_COL 1 //TOPIC
#define LAB_COL 2   //LAB
#define DATE_COL 3  //DATE_
#define DET_COL 4   //DETAIL
#define ADD_COL 5   //ADDITION

#endif // M_FHS_H
