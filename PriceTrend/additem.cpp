﻿#include "additem.h"

AddItem::AddItem(QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);

    table=TD_TREND;
    this->dateEdit->setDate(QDate::currentDate());
    updateTopic();
}

void AddItem::updateTopic()
{
    if(this->topics->count() != 0)
        this->topics->clear();

    QStringList items=sp_getTopic(table);
    if(!items.isEmpty())
        this->topics->addItems(items);

    this->topics->addItem(MANUAL_INPUT);
    this->topics->setCurrentIndex(-1);
}

void AddItem::updateLabel()
{    
    if(this->labels->count() != 0)
        this->labels->clear();

    if(!selectTopic.isEmpty() && selectTopic != MANUAL_INPUT)
    {
        QStringList items=sp_getItemsByTopic("label",table,selectTopic);
        if(!items.isEmpty())
            this->labels->addItems(items);
    }

    this->labels->addItem(MANUAL_INPUT);
    this->labels->setCurrentIndex(-1);
}

void AddItem::setTopic(const QString &topic)
{
    this->topics->setCurrentText(topic);
}

void AddItem::setLabel(const QString &label)
{
    this->labels->setCurrentText(label);
}

void AddItem::on_topics_currentTextChanged(const QString &text)
{
    selectTopic=text;
    if(selectTopic == MANUAL_INPUT)
    {
        this->topic->clear();
        this->topic->setEnabled(true);
    }
    else
    {
        this->topic->setText(text);
        this->topic->setEnabled(false);
    }
    updateLabel();
}

void AddItem::on_labels_currentTextChanged(const QString &text)
{
    if(text == MANUAL_INPUT)
    {
        this->label->clear();
        this->label->setEnabled(true);
    }
    else
    {
        this->label->setText(text);
        this->label->setEnabled(false);
    }
}

void AddItem::on_btn_pasteLabel_clicked()
{
    QString label=qApp->clipboard()->text();
    if(!label.isEmpty())
        this->label->setText(label);
}

void AddItem::on_btn_pasteAddition_clicked()
{
    QString addition=qApp->clipboard()->text();
    if(!addition.isEmpty())
        this->addition->setText(addition);
}

bool AddItem::addItem(const QString &topic,
                      const QString &label,
                      double detail,
                      const QString &addition)
{
    QSqlQuery query;
    query.prepare(QString("insert into %1 (topic,label,date_,detail,addition) values (?,?,?,?,?)")
                  .arg(table));
    query.addBindValue(topic);
    query.addBindValue(label);
    query.addBindValue(this->dateEdit->date().toString(DATE_FORMAT));
    query.addBindValue(detail);
    query.addBindValue(addition);
    if(query.exec())
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void AddItem::on_btn_submit_clicked()
{
    QString topic=this->topic->text().trimmed();
    if(topic.isEmpty())
    {
        MESSAGE_CATEGORY_EMPTY
    }

    QString label=this->label->text().trimmed();
    QString detailStr=this->detail->text().trimmed();
    if(label.isEmpty() || detailStr.isEmpty())
        return;

    double detail=detailStr.toDouble();
    if(detail == 0.0)
    {
        MESSAGE_INPUT_ERROR
    }

    QString addition=this->addition->text().trimmed();
    if(addItem(topic,label,detail,addition))
        accept();
    else
        MESSAGE_CANNOT_SUBMIT
}
