﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "PriceTrend"
#define SP_VER 1002
#define SP_UID "{61c35961-072a-4843-b18e-3a33bce85b8a}"
#define SP_TYPE "prop"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "PriceTrend.ini"
#define SP_INFO "PriceTrend.json"
#define SP_LINK "PriceTrend.lnk"

#endif // G_VER_H
