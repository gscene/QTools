﻿#include "additem.h"
#include "ui_additem.h"

AddItem::AddItem(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddItem)
{
    ui->setupUi(this);
}

AddItem::~AddItem()
{
    delete ui;
}

void AddItem::on_btn_pick_clicked()
{
    file = QFileDialog::getOpenFileName(this, QStringLiteral("选择图片文件"),QString(),("Image Files (*.png *.jpg)"));
    if (file.isEmpty())
        return;

    QPixmap pix=QPixmap(file).scaled(ui->preview->size());
    if(pix.isNull())
        return;

    ui->preview->setPixmap(pix);
}

void AddItem::on_btn_submit_clicked()
{
    Item item;
    item.category=ui->place->text().trimmed();
    item.label=ui->label->text().trimmed();
    if(item.category.isEmpty() || item.label.isEmpty())
        return;

    item.detail=ui->detail->toPlainText();
    if(item.detail.isEmpty())
        return;

    if(file.isEmpty())
    {
        file = QFileDialog::getOpenFileName(this, QStringLiteral("选择图片文件"),QString(),("Image Files (*.png *.jpg)"));
        if (file.isEmpty())
            return;
    }

    QFileInfo fi(file);
    QString ext="." + fi.suffix();
    pixFile=QUuid::createUuid().toString() + ext;
    QFile::copy(file,PRE_PIC + pixFile);

    item.addition=pixFile;
    sendItem(item);

    ui->label->clear();
    ui->detail->clear();
    ui->preview->clear();
}
