﻿#ifndef ADDITEM_H
#define ADDITEM_H

#include "head/g_pch.h"
#include "m_fhs.h"

namespace Ui {
class AddItem;
}

struct Item{
    QString category;
    QString label;
    QString detail;
    QString addition;
};

class AddItem : public QDialog
{
    Q_OBJECT

public:
    explicit AddItem(QWidget *parent = 0);
    ~AddItem();

signals:
    void sendItem(const Item &item);

private slots:
    void on_btn_pick_clicked();

    void on_btn_submit_clicked();

private:
    Ui::AddItem *ui;

    QString file;
    QString pixFile;
};

#endif // ADDITEM_H
