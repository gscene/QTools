#-------------------------------------------------
#
# Project created by QtCreator 2017-03-07T12:16:07
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = CarLogo
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)

INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
        widget.cpp \
    additem.cpp

HEADERS  += widget.h \
    m_fhs.h \
    ../../elfproj/template/m_sql.h \
    additem.h \
    g_ver.h

FORMS    += widget.ui \
    additem.ui

RESOURCES += \
    res.qrc

RC_ICONS = logo.ico
