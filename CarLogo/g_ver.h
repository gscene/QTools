﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "CarLogo"
#define SP_VER 1004
#define SP_UID "{39be84a4-bc8e-44cd-af84-8f7873728a56}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "CarLogo.ini"
#define SP_INFO "CarLogo.json"
#define SP_LINK "CarLogo.lnk"

/*
 * 1003 增加了category/产地列
 * 1004 listView
*/

#endif // G_VER_H
