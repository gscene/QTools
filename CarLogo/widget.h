﻿#ifndef WIDGET_H
#define WIDGET_H

#include "head/g_pch.h"
#include "template/m_sql.h"
#include "additem.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void contextMenuEvent(QContextMenuEvent *);
    void createMenu();

    void detectDB();
    void loadView();
    void newItem();
    bool db_try();

    void receiveItem(const Item &item);

private slots:

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_listView_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QSqlDatabase db;
    QString dbName,table;
    QMenu *menu;
    QStringListModel *listModel;

    AddItem *addItem;
};

#endif // WIDGET_H
