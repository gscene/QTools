﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : BaseEditor(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    table=TD_NOTE;
    selectId=0;
    dClickCopyFlag=false;
    listModel=new QStringListModel(this);
    ui->listView->setModel(listModel);

    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);

    updateTopics();
    createMenu();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::updateTopics()
{
    if(listModel->rowCount() != 0)
        listModel->setStringList(QStringList());

    topics=sp_getTopic(table);
    if(!topics.isEmpty())
        listModel->setStringList(topics);
}

void Widget::generateMenu()
{
    updateMenu();
}

void Widget::updateMenu()
{
    if(!menu->isEmpty())
        menu->clear();

    menu->addAction(QStringLiteral("新记录... (F1)"),this,&Widget::newItem);
    menu->addSeparator();

    QMenu *m_update=menu->addMenu(QStringLiteral("更新"));
    m_update->addAction(QStringLiteral("更新列表 (F4)"),this,&Widget::updateTopics);
    m_update->addAction(QStringLiteral("更新视图 (F5)"),this,&Widget::updateView);
    menu->addSeparator();
    menu->addAction(QStringLiteral("保存 (Ctrl+S)"),this,&Widget::save);
    menu->addSeparator();
    QMenu *m_option=menu->addMenu(QStringLiteral("选项"));
    if(!dClickCopyFlag)
        m_option->addAction(QStringLiteral("启用双击复制"),this,&Widget::do_dClickCopyTrigger);
    else
        m_option->addAction(QStringLiteral("禁用双击复制"),this,&Widget::do_dClickCopyTrigger);
    m_option->addSeparator();
    m_option->addAction(QStringLiteral("【保存】"),this,&Widget::save);
    m_option->addAction(QStringLiteral("【删除】"),this,&Widget::removeItem);
}

void Widget::do_dClickCopyTrigger()
{
    dClickCopyFlag = !dClickCopyFlag;
    updateMenu();
}

void Widget::newItem()
{
    AddItem item(this,topics);
    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Widget::removeItem()
{
    auto index=ui->tableView->currentIndex();
    if(index.isValid())
        model->removeRow(index.row());
}

void Widget::saveItem()
{
    if(model->rowCount() < 1)
        return;

    if(selectId <= 0)
        return;

    QString detail=ui->detail->toPlainText();
    if(detail.isEmpty() || detail == previousDetail)
        return;

    if(sp_updateColumnById(table,"detail",detail,selectId))
        updateView();
    else
        MESSAGE_CANNOT_SAVE
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::ControlModifier && event->key()==Qt::Key_S)
        saveItem();
    else
    {
        switch (event->key()) {
        case Qt::Key_F1:
            newItem();
            break;
        case Qt::Key_F4:
            updateTopics();
            break;
        case Qt::Key_F5:
            updateView();
            break;
        }
    }
}

void Widget::on_listView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        selectTopic=index.data().toString();
        if(!selectTopic.isEmpty())
            updateView();
    }
}

void Widget::updateView()
{
    if(selectTopic.isEmpty())
        return;

    model->setTable(table);
    model->setFilter(QString("topic='%1'").arg(selectTopic));
    model->select();

    ui->tableView->hideColumn(ID_COL);
    ui->tableView->hideColumn(TOP_COL);
    model->setHeaderData(DET_COL,Qt::Horizontal,QStringLiteral("内容")); //detail
    ui->tableView->hideColumn(ADD_COL);

    ui->detail->clear();
}

void Widget::on_tableView_clicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        selectId=sp_fetchId(index);
        previousDetail=sp_fetchString(index,DET_COL);
        ui->detail->setPlainText(previousDetail);
    }
}

void Widget::on_tableView_doubleClicked(const QModelIndex &)
{
    if(!dClickCopyFlag)
        return;

    if(previousDetail.isEmpty())
        return;

    qApp->clipboard()->setText(previousDetail);
}
