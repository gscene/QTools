﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define TD_NOTE "td_note"
#define DEF_TOPIC QStringLiteral("默认")
#define MANUAL_INPUT QStringLiteral("-手动输入-")

#define ID_COL 0
#define TOP_COL 1
#define DET_COL 2
#define ADD_COL 3

#endif // M_FHS_H
