﻿#ifndef WIDGET_H
#define WIDGET_H

#include <common/baseeditor.h>
#include "additem.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void generateMenu();
    void updateMenu();
    void updateTopics();
    void updateView();
    void newItem();
    void removeItem();
    void saveItem();    //保存内容修改
    void do_dClickCopyTrigger();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_listView_doubleClicked(const QModelIndex &index);
    void on_tableView_clicked(const QModelIndex &index);
    void on_tableView_doubleClicked(const QModelIndex &);

private:
    Ui::Widget *ui;

    QStringList topics;
    bool dClickCopyFlag;  //双击复制

    QString selectTopic;
    int selectId;
    QString previousDetail;
    QStringListModel *listModel;
};
#endif // WIDGET_H
