QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = QNote
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
    ../../elfproj/support/sp_env.cpp \
    additem.cpp \
    main.cpp \
    widget.cpp

HEADERS += \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/support/sp_env.h \
    additem.h \
    g_ver.h \
    m_fhs.h \
    widget.h

FORMS += \
    additem.ui \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc

RC_ICONS = notepad.ico
