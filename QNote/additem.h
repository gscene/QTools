﻿#ifndef ADDITEM_H
#define ADDITEM_H

#include <head/g_pch.h>
#include <head/db_helper.h>
#include <head/m_message.h>
#include "m_fhs.h"

namespace Ui {
class AddItem;
}

class AddItem : public QDialog
{
    Q_OBJECT

public:
    explicit AddItem(QWidget *parent,const QStringList &topics);
    ~AddItem();

    bool addItem(const QString &topic,
                 const QString &detail);

private slots:
    void on_btn_submit_clicked();

    void on_topics_currentTextChanged(const QString &text);

private:
    Ui::AddItem *ui;

    QString table;
};

#endif // ADDITEM_H
