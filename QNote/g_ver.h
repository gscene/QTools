﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QNote"
#define SP_VER 1004
#define SP_UID "{fb93bf09-603c-44fa-9e50-ceb6ecf214c9}"
#define SP_TYPE "prop"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "QNote.ini"
#define SP_INFO "QNote.json"
#define SP_LINK "QNote.lnk"

/*
 * 1002 基本框架与功能
 * 1003 启用/禁用双击复制
 * 1004 保存和【保存】
*/

#endif // G_VER_H
