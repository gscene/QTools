﻿#include "mainwindow.h"
#include "m_viewer.h"

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        QApplication a(argc, argv);
        MainWindow w;
        w.show();
        return a.exec();
    }
    else
        return sp_loadModel(argv[1]);
}
