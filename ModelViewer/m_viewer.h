#ifndef M_VIEWER_H
#define M_VIEWER_H

#include <osg/GraphicsContext>
#include <osgDB/ReadFile>
#include <osgUtil/Optimizer>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include "m_config.h"

QRect rect=mv_loadRect();

osg::GraphicsContext* sp_getGC()
{
    //设置图形环境特性
    osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits();
    traits->x = rect.x();
    traits->y = rect.y();
    traits->width = rect.width();
    traits->height = rect.height();
    traits->windowDecoration = true;    //是否支持窗口扩展的功能
    traits->doubleBuffer = true;        //是否支持双缓存，默认不支持
    traits->sharedContext = nullptr;   //共享上下文

    //创建图形环境特性
    osg::GraphicsContext *gc = osg::GraphicsContext::createGraphicsContext(traits.get());
    return gc;
}

int sp_loadModel(const std::string &fileName)
{
    //创建Viewer对象
    osg::ref_ptr<osgViewer::Viewer> viewer = new osgViewer::Viewer;
    osg::ref_ptr<osg::Node> node=osgDB::readNodeFile(fileName);

    //设置视口
    viewer->getCamera()->setViewport(new osg::Viewport(0,0,
                                                       rect.width(),
                                                       rect.height()));
    //设置图形环境
    viewer->getCamera()->setGraphicsContext(sp_getGC());

    //优化场景数据
    osgUtil::Optimizer optimizer;
    optimizer.optimize(node.get());

    // mv_saveRect(rect);
    viewer->setSceneData(node.get());
    viewer->realize();
    return viewer->run();
}

#endif // M_VIEWER_H
