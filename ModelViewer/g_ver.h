#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "ModelViewer"
#define SP_VER 1003
#define SP_UID "{3e307c76-565e-4916-8613-1cbed5824bdd}"
#define SP_TYPE "console"

#define SP_CFG "ModelViewer.ini"
#define SP_INFO "ModelViewer.json"
#define SP_LINK "ModelViewer.lnk"

/*
 * 1001 osg base
 * 1002 add qt support
 * 1003 libs
 */

#endif // G_VER_H
