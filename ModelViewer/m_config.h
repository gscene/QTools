#ifndef M_CONFIG_H
#define M_CONFIG_H

#include "m_fhs.h"
#include <QString>
#include <QVariant>
#include <QFileInfo>
#include <QSettings>
#include <QRect>

QRect mv_loadRect()
{
    if(!QFileInfo::exists(M_CONFIG))
        return QRect(POS_X,POS_Y,POS_W,POS_H);
    else
    {
        QSettings cfg(M_CONFIG,QSettings::IniFormat);
        cfg.setIniCodec("UTF-8");
        return cfg.value("Main/rect").toRect();
    }
}

void mv_saveRect(const QRect &rect)
{
    QSettings cfg(M_CONFIG,QSettings::IniFormat);
    cfg.setIniCodec("UTF-8");
    cfg.setValue("Main/rect",rect);
}

#endif // M_CONFIG_H
