﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <head/g_functionbase.h>
#include <QApplication>
#include <QMainWindow>
#include <QPaintEvent>
#include <QMessageBox>
#include <QFileDialog>

#include <osgDB/ReadFile>
#include <osgDB/Options>
#include <osgGA/TrackballManipulator>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgUtil/Optimizer>
#include <osgQt/GraphicsWindowQt>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    osgQt::GraphicsWindowQt* createGraphicsWindow(int w, int h);
    void loadModelFile(const QString &fileName);

protected:
    void paintEvent(QPaintEvent* event);

private slots:
    void on_actionOpen_triggered();

private:
    Ui::MainWindow *ui;

    osgViewer::Viewer _viewer;
    QTimer _timer;
    osgUtil::Optimizer _optimizer;
};

#endif // MAINWINDOW_H
