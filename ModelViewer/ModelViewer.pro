#-------------------------------------------------
#
# Project created by QtCreator 2019-01-06T20:05:00
#
#-------------------------------------------------

QT       += core gui opengl network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ModelViewer
TEMPLATE = app

INCLUDEPATH += ../../elfproj
LIBS += -losg -losgDB -losgUtil -losgGA \
        -losgText -losgViewer -lOpenThreads \
        -losgWidget -losgQt5

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        g_ver.h \
        m_config.h \
        m_fhs.h \
        m_viewer.h \
        mainwindow.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc

RC_ICONS = chess.ico
