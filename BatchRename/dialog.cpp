﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    table="st_patent";
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_btn_pick_clicked()
{
    QString path=QFileDialog::getExistingDirectory(this);
    QDir::setCurrent(path);

    QSqlQuery query;
    QDir dir(path);
    auto list=dir.entryList(QDir::Files);
    foreach (QString item, list) {
        query.exec(QString("select * from %1 where location='%2'")
                   .arg(table)
                   .arg(item));
        if(query.next())
        {
            QString prefix;
            QString pub_id=query.value("pub_id").toString();
            QString label=query.value("label").toString();
            if(pub_id.startsWith("CN10"))
                prefix=QStringLiteral("中文-发明-");
            else if(pub_id.startsWith("CN20"))
                prefix=QStringLiteral("中文-实用-");
            else if(pub_id.startsWith("CN30"))
                prefix=QStringLiteral("中文-外观-");

            QString fileName=prefix + label + ".pdf";
            QFile::rename(item,fileName);
        }
    }
}
