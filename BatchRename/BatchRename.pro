#-------------------------------------------------
#
# Project created by QtCreator 2018-01-22T14:23:10
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BatchRename
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        dialog.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
        dialog.h \
    ../../elfproj/support/sp_env.h \
    g_ver.h

FORMS += \
        dialog.ui
