﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "BatchRename"
#define SP_VER 1003
#define SP_UID "{0d399bc8-b93e-4a2c-aa20-67856a01906a}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "BatchRename.ini"
#define SP_INFO "BatchRename.json"
#define SP_LINK "BatchRename.lnk"

#endif // G_VER_H
