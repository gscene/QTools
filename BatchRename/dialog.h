﻿#ifndef DIALOG_H
#define DIALOG_H

#include <head/g_pch.h>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_btn_pick_clicked();

private:
    Ui::Dialog *ui;

    QString table;
};

#endif // DIALOG_H
