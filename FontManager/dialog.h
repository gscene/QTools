﻿#ifndef DIALOG_H
#define DIALOG_H

#include <common/baseeditor.h>

#define LINKS QStringLiteral("<a href=\"%1\">%2</a>")

namespace Ui {
class Dialog;
}

class Dialog : public BaseEditor
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent=nullptr,
                    Qt::WindowFlags f=Qt::Dialog);
    ~Dialog();

    void generateMenu();
    QStringList getWS();

    void pickSystemFont();
    void pickUserFont();

private slots:
    void on_listView_doubleClicked(const QModelIndex &index);

    void on_comboBox_currentTextChanged(const QString &text);

private:
    Ui::Dialog *ui;

    QString fontLocation;
    QFont currentFont;
    QStringListModel *listModel;
    QMetaEnum metaEnum;
};

#endif // DIALOG_H
