﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent, Qt::WindowFlags f) :
    BaseEditor(parent,f),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    listModel=new QStringListModel(this);
    ui->listView->setModel(listModel);

    metaEnum=QMetaEnum::fromType<QFontDatabase::WritingSystem>();
    fontLocation=QStandardPaths::writableLocation(QStandardPaths::FontsLocation);
    ui->label->setText(QString(LINKS).arg(fontLocation).arg("Fonts"));

    ui->comboBox->addItems(getWS());
    ui->comboBox->setCurrentIndex(-1);

    createMenu();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::generateMenu()
{
    menu->addAction(QStringLiteral("选择系统字体..."),this,&Dialog::pickSystemFont);
    menu->addAction(QStringLiteral("选择用户字体..."),this,&Dialog::pickUserFont);
}

QStringList Dialog::getWS()
{
    QStringList detail;
    QList<QFontDatabase::WritingSystem> list=QFontDatabase().writingSystems();
    foreach (QFontDatabase::WritingSystem item, list) {
        detail.append(QString(metaEnum.valueToKey(item)));
    }
    return detail;
}

void Dialog::on_listView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        ui->display->clear();
        QString fontFamily=index.data().toString();
        QFont font(fontFamily);
        // defaultFamily 全是一样的
        // ui->display->append("defaultFamily: " + font.defaultFamily());
        ui->display->append("fontFamily: " + font.family());
        //  ui->display->append(font.styleName());  // windows不支持
        ui->display->append("description: " + font.toString());
        QString fontStyle=QFontDatabase().styles(fontFamily).join(",");
        ui->display->append(QString("fontStyle: ") + fontStyle);
    }
}

void Dialog::pickSystemFont()
{
    bool ok;
    QFont font=QFontDialog::getFont(&ok,this);
    if(ok)
    {
        ui->display->clear();
        QString fontFamily=font.family();
        ui->display->append("fontFamily: " + fontFamily);
        ui->display->append("description: " + font.toString());
        QString fontStyle=QFontDatabase().styles(fontFamily).join(",");
        ui->display->append(QString("fontStyle: ") + fontStyle);
    }
}

void Dialog::pickUserFont()
{
    QString fontPath=QFileDialog::getOpenFileName(this,QStringLiteral("选择字体文件"),
                                                  QString(),"Fonts (*.ttf *.ttc *.otf)");
    if(!fontPath.isEmpty())
    {
        ui->display->clear();
        int fontId=QFontDatabase::addApplicationFont(fontPath);
        QStringList fontFamilies=QFontDatabase::applicationFontFamilies(fontId);
        if(!fontFamilies.isEmpty())
        {
            foreach (QString fontFamily, fontFamilies) {
                QFont font(fontFamily);
                ui->display->append("fontFamily: " + fontFamily);
                ui->display->append("description: " + font.toString());
                QString fontStyle=QFontDatabase().styles(fontFamily).join(",");
                ui->display->append(QString("fontStyle: ") + fontStyle + "\n");
            }
        }
    }
}

void Dialog::on_comboBox_currentTextChanged(const QString &text)
{
    QStringList fonts;
    if(text.isEmpty())
        fonts=QFontDatabase().families(QFontDatabase::SimplifiedChinese);
    else
    {
        std::string _text=text.toStdString();
        QFontDatabase::WritingSystem ws=(QFontDatabase::WritingSystem)metaEnum.keyToValue(_text.c_str());
        fonts=QFontDatabase().families(ws);
    }

    listModel->setStringList(fonts);
}
