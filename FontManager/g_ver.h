﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "FontManager"
#define SP_VER 1004
#define SP_UID "{2ed19d45-d3d2-45ee-89bb-d8d37e9189b1}"
#define SP_TYPE "prop"

#define SP_CFG "FontManager.ini"
#define SP_INFO "FontManager.json"
#define SP_LINK "FontManager.lnk"

/*
 * 1002 重构
 * 1003 pickUserFont
 * 1004 暂停更新
 */

#endif // G_VER_H
