#-------------------------------------------------
#
# Project created by QtCreator 2017-11-09T16:44:18
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FontManager
TEMPLATE = app
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        dialog.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
        dialog.h \
    g_ver.h \
    ../../elfproj/common/baseeditor.h \
    m_fhs.h \
    ../../elfproj/support/sp_env.h

FORMS += \
        dialog.ui

RESOURCES += \
    res.qrc

RC_ICONS = fonts.ico
