#ifndef OS_HELPER_H
#define OS_HELPER_H

#include <windows.h>
#include <QString>
#include <QDebug>

static void OutputDebugView(const QString &content)
{
    qDebug() << content;
}

//提升权限
static bool EnableSpecificPrivilege(LPCTSTR lpPrivilegeName)
{

    HANDLE hToken = NULL;
    TOKEN_PRIVILEGES Token_Privilege;
    BOOL bRet = TRUE;

    do
    {
        if (0 == OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
        {
            OutputDebugView("OpenProcessToken Error");
            bRet = FALSE;
            break;
        }

        if (0 == LookupPrivilegeValue(NULL, lpPrivilegeName, &Token_Privilege.Privileges[0].Luid))
        {
            OutputDebugView("LookupPrivilegeValue Error");
            bRet = FALSE;
            break;
        }

        Token_Privilege.PrivilegeCount = 1;
        Token_Privilege.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
        //Token_Privilege.Privileges[0].Luid.LowPart=17;//SE_BACKUP_PRIVILEGE
        //Token_Privilege.Privileges[0].Luid.HighPart=0;


        if (0 == AdjustTokenPrivileges(hToken, FALSE, &Token_Privilege, sizeof(Token_Privilege), NULL,NULL))
        {
            OutputDebugView("AdjustTokenPrivileges Error");
            bRet = FALSE;
            break;
        }

    } while (false);

    if (NULL != hToken)
    {
        CloseHandle(hToken);
    }

    return bRet;
}

#endif // OS_HELPER_H
