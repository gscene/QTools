#ifndef DATE_HELPER_H
#define DATE_HELPER_H

#include <QDate>
#include <QTime>
#include <QFile>
#include <QSettings>
#include <QProcess>
#include "os_helper.h"

class DateTimeHelper
{
public:
    void setDate(const QString &date)
    {
        QProcess::startDetached(QString("cmd /c date %1").arg(date));
        auto date_=QDate::currentDate().toString("yyyy-MM-dd");
        qDebug() << QString("Date: %1").arg(date_);
    }

    void setTime(const QString &time)
    {
        QProcess::startDetached(QString("cmd /c time %1").arg(time));
        auto time_=QTime::currentTime().toString("hh:mm:ss");
        qDebug() << QString("Time: %1").arg(time_);
    }

    void generateConfig(const QString &configFile)
    {
        QSettings cfg(configFile,QSettings::IniFormat);
        cfg.setValue("Common/date",QDate::currentDate().toString("yyyy-MM-dd"));
        cfg.setValue("Common/time",QTime::currentTime().toString("hh:mm:ss"));
    }

    void setWindowsSystemTime(const QString &date,const QString &time)
    {
        auto dateTime=QString("%1 %2").arg(date).arg(time);
        qDebug() << dateTime;

        QDateTime dt=QDateTime::fromString(dateTime,"yyyy-MM-dd hh:mm:ss");
        SYSTEMTIME st;
        GetLocalTime(&st);
        st.wYear=dt.date().year();
        st.wMonth=dt.date().month();
        st.wDay=dt.date().day();
        st.wHour=dt.time().hour();
        st.wMinute=dt.time().minute();
        st.wSecond=dt.time().second();
        st.wMilliseconds=0;
        BOOL ret=SetLocalTime(&st);
        if(ret)
            qDebug() << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
        else
            qDebug() << ret;
    }

    void loadFromConfig(const QString &configFile)
    {
        if(!QFile::exists(configFile))
        {
            generateConfig(configFile);
            qDebug() << "Config File Generated";
            return;
        }

        QSettings cfg(configFile,QSettings::IniFormat);
        auto date=cfg.value("Common/date").toString();
        auto time=cfg.value("Common/time").toString();

        if(EnableSpecificPrivilege(SE_SYSTEMTIME_NAME))
            setWindowsSystemTime(date,time);
    }
};

#endif // DATE_HELPER_H
