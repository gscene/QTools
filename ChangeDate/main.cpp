#include <QCoreApplication>
#include "date_helper.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    auto file_name=a.applicationName();

    DateTimeHelper helper;
    auto cfg_file=file_name + ".ini";
    helper.loadFromConfig(cfg_file);

    return EXIT_SUCCESS;
}
