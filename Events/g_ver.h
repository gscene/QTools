﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Events"
#define SP_VER 1005
#define SP_UID "{65914b8b-b6f1-4391-b710-02d4bdb55fba}"
#define SP_TYPE "shell"

#define SP_CFG "Events.ini"
#define SP_INFO "Events.json"
#define SP_LINK "Events.lnk"

/*
 * 1001
 * 1002 baseeditor修正
 * 1003 表结构修正
 * 1004 列宽修正
 * 1005 bug修正
*/
#endif // G_VER_H
