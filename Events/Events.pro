#-------------------------------------------------
#
# Project created by QtCreator 2017-03-10T14:06:04
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Events
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

SOURCES += main.cpp\
        widget.cpp \
    ../common/bizeditor.cpp \
    ../../elfproj/support/sp_env.cpp \
    ../../elfproj/common/baseeditor.cpp \
    ../../elfproj/common/user.cpp

HEADERS  += widget.h \
    g_ver.h \
    ../common/bizeditor.h \
    ../../elfproj/support/sp_env.h \
    ../common/m_fhs.h \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/common/user.h

FORMS    += widget.ui

RESOURCES += \
    res.qrc

RC_ICONS = pin.ico
