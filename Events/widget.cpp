﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    BizEditor(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    table = TD_EVENTS;
    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);

    if(loadColumnWidth())
        loadColumnWidthFlag=true;

    updateView();
    createMenu();
}

Widget::~Widget()
{
    saveColumnWidth();
    delete ui;
}

void Widget::generateMenu()
{
    menu->addAction(QStringLiteral("未完成 事件"),this,&Widget::updateView);
    menu->addSeparator();
    menu->addAction(QStringLiteral("查找..."),this,&Widget::search);
    menu->addSeparator();
    menu->addAction(QStringLiteral("已完成 事件"),this,&Widget::updateViewFinish);
    menu->addAction(QStringLiteral("已取消 事件"),this,&Widget::updateViewCancel);
    menu->addSeparator();
    menu->addAction(QStringLiteral("进入编辑模式"),this,&Widget::edit_on);
}

void Widget::updateMenu()
{
    menu->clear();

    if(isEdit)
    {
        menu->addAction(QStringLiteral("【已完成】"),this,&Widget::en_finish);
        menu->addAction(QStringLiteral("【取消】"),this,&Widget::en_cancel);
        menu->addSeparator();
        menu->addAction(QStringLiteral("保存"),this,&Widget::save);
        menu->addAction(QStringLiteral("撤销"),this,&Widget::revert);
        menu->addSeparator();
        menu->addAction(QStringLiteral("退出编辑模式"),this,&Widget::edit_off);
    }
    else
        generateMenu();
}

bool Widget::loadColumnWidth()
{
    if(!QFile::exists(configFile))
        return false;
    else
    {
        QSettings set(configFile,QSettings::IniFormat);
        int width=set.value("Widget/width").toInt();
        int height=set.value("Widget/height").toInt();
        if(width >= 720 && height >= 480)
            this->resize(width,height);

        colWidth.insert(1,set.value("ColumnWidth/time").toInt());
        colWidth.insert(2,set.value("ColumnWidth/type").toInt());
        colWidth.insert(3,set.value("ColumnWidth/event").toInt());
        return true;
    }
}

void Widget::saveColumnWidth()
{
    QSettings set(configFile,QSettings::IniFormat);
    set.setValue("Widget/width",this->width());
    set.setValue("Widget/height",this->height());

    set.setValue("ColumnWidth/time",ui->tableView->columnWidth(1));
    set.setValue("ColumnWidth/type",ui->tableView->columnWidth(2));
    set.setValue("ColumnWidth/event",ui->tableView->columnWidth(3));
}

void Widget::setHeaderData()
{
    ui->tableView->hideColumn(0);       //id
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("时间"));      //time
    model->setHeaderData(2,Qt::Horizontal,QStringLiteral("类型"));        //type
    model->setHeaderData(3,Qt::Horizontal,QStringLiteral("事件"));        //event
    model->setHeaderData(4,Qt::Horizontal,QStringLiteral("状态"));        //status

    if(loadColumnWidthFlag)
    {
        QMap<int,int>::const_iterator itor;
        for(itor = colWidth.constBegin(); itor != colWidth.constEnd(); itor++ )
        {
            int width=itor.value();
            if(width <= 0)
                width=100;
            else if(width >= this->width())
                width =400;

            ui->tableView->setColumnWidth(itor.key(),width);
        }
    }
}

void Widget::search()
{
    showSearchBox("event");
}

void Widget::edit_on()
{
    isEdit=true;
    ui->tableView->setEditTriggers(QTableView::DoubleClicked);
    updateMenu();
}

void Widget::edit_off()
{
    isEdit=false;
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
    updateMenu();
}

void Widget::newItem()
{}

void Widget::en_finish()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QModelIndex mIndex=index;
        if(index.column() != 4)
            mIndex=index.sibling(index.row(),4);
        finishItem(mIndex);
    }
}

void Widget::en_cancel()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QModelIndex mIndex=index;
        if(index.column() != 4)
            mIndex=index.sibling(index.row(),4);
        cancelItem(mIndex);
    }
}

void Widget::on_btn_submit_clicked()
{
    QString type=ui->type->text().trimmed();
    if(type.isEmpty())
        return;

    QString event=ui->event->text().trimmed();
    if(event.isEmpty())
        return;

    QString time=QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    QSqlQuery query;
    QString sql=QString("insert into %1 (time,type,event,status) values (?,?,?,?)").arg(table);
    query.prepare(sql);
    query.addBindValue(time);
    query.addBindValue(type);
    query.addBindValue(event);
    query.addBindValue(ST_RUNNING);

    if(!query.exec())
    {
        qDebug() << query.lastError().text();
        QMessageBox::warning(this,QStringLiteral("异常情况"),QStringLiteral("无法提交数据！"));
    }
    else
    {
        ui->type->clear();
        ui->event->clear();
        updateView();
    }
}
