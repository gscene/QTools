﻿#ifndef WIDGET_H
#define WIDGET_H

#include "bizeditor.h"
#include "common/user.h"

#define TD_EVENTS "td_events"

namespace Ui {
class Widget;
}

class Widget : public BizEditor
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void generateMenu();
    void updateMenu();

    bool loadColumnWidth();
    void saveColumnWidth();
    void setHeaderData();

    void search();
    void newItem();
    void en_finish();
    void en_cancel();
    void edit_on();
    void edit_off();

private slots:
    void on_btn_submit_clicked();

private:
    Ui::Widget *ui;

    User user;
};

#endif // WIDGET_H
