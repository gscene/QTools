﻿#ifndef TEXTSCANER_H
#define TEXTSCANER_H

#include <QDialog>
#include <head/g_pch.h>
#include "m_fhs.h"

namespace Ui {
class TextScaner;
}

class TextScaner : public QDialog
{
    Q_OBJECT

public:
    explicit TextScaner(QWidget *parent = 0);
    ~TextScaner();

    bool addItem(const QString &label,
                 const QString &detail);

private slots:
    void on_btn_pick_clicked();

    void on_btn_scan_clicked();

private:
    Ui::TextScaner *ui;
    QString table;
    QString category;
};

#endif // TEXTSCANER_H
