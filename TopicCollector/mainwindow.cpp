﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    textScaner=nullptr;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionTextScan_triggered()
{
    if(textScaner==nullptr)
        textScaner=new TextScaner;

    if(centralWidget() != textScaner)
        setCentralWidget(textScaner);
}
