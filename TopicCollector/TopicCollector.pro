#-------------------------------------------------
#
# Project created by QtCreator 2017-10-20T08:58:57
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = TopicCollector
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    textscaner.cpp

HEADERS += \
        mainwindow.h \
    g_ver.h \
    textscaner.h \
    m_fhs.h

FORMS += \
        mainwindow.ui \
    textscaner.ui
