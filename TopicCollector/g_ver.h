﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "TopicCollector"
#define SP_VER 1002
#define SP_UID "{7285d64e-8d67-46b1-b6e2-5b21864aafef}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "TopicCollector.ini"
#define SP_INFO "TopicCollector.json"
#define SP_LINK "TopicCollector.lnk"

/*
 * 1002
 *
*/

#endif // G_VER_H
