QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS
INCLUDEPATH += ../../elfproj
unix:LIBS += -lrocksdb
win32:LIBS += -lrocksdb -lzstd -llz4 -lsnappy -lbz2 -lz -lshlwapi -lrpcrt4

SOURCES += \
    main.cpp \
    widget.cpp

HEADERS += \
    ../../elfproj/element/rdb.h \
    widget.h

FORMS += \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
