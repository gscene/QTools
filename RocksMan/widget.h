#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "element/rdb.h"
#include <QFileDialog>

#define LOG_GET QString("GET: %1, RETURN: %2")
#define LOG_PUT QString("PUT: %1, %2")
#define LOG_DEL QString("DELETE: %1")

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void log(const QString &message);

private slots:
    void on_btn_load_clicked();

    void on_btn_submit_clicked();

    void on_btn_remove_clicked();

    void on_btn_clear_clicked();

private:
    Ui::Widget *ui;
    RDB rdb;
    QString dbPath;
};
#endif // WIDGET_H
