#include "widget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("RocksMan");
    a.setApplicationVersion("0.0.2");
    Widget w;
    w.show();
    return a.exec();
}
