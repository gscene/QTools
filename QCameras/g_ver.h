﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QCameras"
#define SP_VER 1002
#define SP_UID "{3827d906-9307-401f-82df-8c600b56d368}"
#define SP_TYPE "shell"

#define SP_INFO "QCameras.json"
#define SP_LINK "QCameras.lnk"

#endif // G_VER_H
