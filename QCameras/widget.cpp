﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    currentResolution=RES_720P;
    startUp();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::startUp()
{
    auto items=QCameraInfo::availableCameras();
    int cameraCount=items.size();
    if(cameraCount > 0)
    {
        for(int i=0;i < items.size();i++)
        {
            QCamera *camera=new QCamera(items.at(i));
            QCameraViewfinder *viewer=new QCameraViewfinder;
            camera->setViewfinder((viewer));

            QCameraViewfinderSettings viewfinderSettings;
            viewfinderSettings.setResolution(currentResolution);
            camera->setViewfinderSettings(viewfinderSettings);

            ui->gridLayout->addWidget(viewer,0,i);

            camera->start();

            cameras.append(camera);
            cameraViewers.append(viewer);
        }
    }
    else
    {
        this->setWindowTitle(QStringLiteral("没有可用摄像头"));
    }
}
