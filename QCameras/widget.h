﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QCameraInfo>
#include <QCamera>
#include <QCameraViewfinder>
#include <QCameraViewfinderSettings>
#include <QCameraImageCapture>

#define RES_720P QSize(1280,720)
#define RES_1080P QSize(1920,1080)

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void startUp();

private:
    Ui::Widget *ui;

    QSize currentResolution;
    QList<QCamera*> cameras;
    QList<QCameraViewfinder*> cameraViewers;
};
#endif // WIDGET_H
