﻿#ifndef LISTER_H
#define LISTER_H

#include <head/g_pch.h>
#include <support/sp_env.h>

namespace Ui {
class Lister;
}

class Lister : public QDialog
{
    Q_OBJECT

public:
    explicit Lister(QWidget *parent = 0);
    ~Lister();

private slots:
    void on_btn_pick_clicked();
    void on_btn_process_clicked();
    void on_btn_clean_clicked();
    void on_btn_export_clicked();

private:
    Ui::Lister *ui;

    QStringList lines;
    QStringListModel *model;
};

#endif // LISTER_H
