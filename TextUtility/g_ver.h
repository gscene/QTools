﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "TextUtility"
#define SP_VER 1002
#define SP_UID "{UUID}"
#define SP_TYPE "prop"

#define SP_CFG "TextUtility.ini"
#define SP_INFO "TextUtility.json"
#define SP_LINK "TextUtility.lnk"

#endif // G_VER_H
