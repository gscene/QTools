#-------------------------------------------------
#
# Project created by QtCreator 2017-12-28T13:28:36
#
#-------------------------------------------------

QT       += core gui  sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TextUtility
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        widget.cpp \
    lister.cpp \
    ../../elfproj/support/sp_env.cpp \
    maker.cpp \
    scanner.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    lister.h \
    ../../elfproj/support/sp_env.h \
    maker.h \
    scanner.h

FORMS += \
        widget.ui \
    lister.ui \
    maker.ui \
    scanner.ui
