﻿#ifndef MAKER_H
#define MAKER_H

#include <head/g_pch.h>

#define LINKS "<a href=\"%1\">%2</a>"

namespace Ui {
class Maker;
}

class Maker : public QDialog
{
    Q_OBJECT

public:
    explicit Maker(QWidget *parent = 0);
    ~Maker();

    void do_makeDir(const QStringList &items);

private slots:
    void on_btn_pickDir_clicked();
    void on_btn_pick_clicked();
    void on_btn_process_clicked();

    void on_btn_clean_clicked();

private:
    Ui::Maker *ui;

    QString targetDir;
};

#endif // MAKER_H
