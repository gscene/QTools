﻿#ifndef WIDGET_H
#define WIDGET_H

#include "lister.h"
#include "maker.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_btn_lister_clicked();

    void on_btn_maker_clicked();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
