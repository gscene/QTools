﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btn_lister_clicked()
{
    Lister lister;
    lister.exec();
}

void Widget::on_btn_maker_clicked()
{
    Maker maker;
    maker.exec();
}
