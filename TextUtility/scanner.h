#ifndef SCANNER_H
#define SCANNER_H

#include <QDialog>

namespace Ui {
class Scanner;
}

class Scanner : public QDialog
{
    Q_OBJECT

public:
    explicit Scanner(QWidget *parent = 0);
    ~Scanner();

private:
    Ui::Scanner *ui;
};

#endif // SCANNER_H
