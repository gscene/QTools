﻿#ifndef WIDGET_H
#define WIDGET_H

#include "common/baseeditor.h"

namespace Ui {
class Widget;
}

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void updateDateList();
    void updateView(){}
    void generateMenu(){}
    void updateView(const QString &date);
    bool addItem(const QString &date,const QString &content);
    bool updateItem(const QString &content,const QString &date);

private slots:
    void on_btn_clean_clicked();

    void on_btn_save_clicked();

    void on_listView_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QString selectDate;
    QString lastContent;
    QStringListModel *listModel;
};

#endif // WIDGET_H
