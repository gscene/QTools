#-------------------------------------------------
#
# Project created by QtCreator 2018-02-21T08:47:12
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QDiary
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
        main.cpp \
        widget.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/support/sp_env.h

FORMS += \
        widget.ui
