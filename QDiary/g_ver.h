﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QDiary"
#define SP_VER 1002
#define SP_UID "{dc47316d-755a-49b7-9eaa-d1206af6d7e7}"
#define SP_TYPE "prop"

#define SP_CFG "QDiary.ini"
#define SP_INFO "QDiary.json"
#define SP_LINK "QDiary.lnk"

#endif // G_VER_H
