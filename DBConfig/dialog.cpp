﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    load();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::load()
{
    if(QFile::exists(CFG_FILE))
    {
        QSettings cfg(CFG_FILE,QSettings::IniFormat);
        ui->host->setText(cfg.value("Storage/host").toString());
        ui->port->setText(cfg.value("Storage/port").toString());
        ui->user->setText(cfg.value("Storage/user").toString());
        ui->passwd->setText(cfg.value("Storage/passwd").toString());
    }
    else
    {
        ui->host->setText("127.0.0.1");
        ui->port->setText("3306");
        ui->user->setText(QStringLiteral("无效帐号"));
        ui->passwd->setText(QStringLiteral("无效密码"));
    }
}

void Dialog::on_btn_save_clicked()
{
    QString host=ui->host->text().trimmed();
    bool ok;
    int port=ui->port->text().toInt(&ok);
    if(!ok)
        port=3306;
    QString user=ui->user->text().trimmed();
    QString passwd=ui->passwd->text().trimmed();

    QSettings cfg(CFG_FILE,QSettings::IniFormat);
    cfg.setValue("Storage/host",host);
    cfg.setValue("Storage/port",port);
    cfg.setValue("Storage/user",user);
    cfg.setValue("Storage/passwd",passwd);
}
