﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QString>
#include <QFile>
#include <QSettings>

#define CFG_FILE "../etc/storage.ini"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void load();

private slots:
    void on_btn_save_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
