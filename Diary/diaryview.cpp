﻿#include "diaryview.h"
#include "ui_diaryview.h"

DiaryView::DiaryView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DiaryView)
{
    ui->setupUi(this);
}

DiaryView::~DiaryView()
{
    delete ui;
}

void DiaryView::setContent(const QString &content)
{
    ui->contentView->setPlainText(content);
}
