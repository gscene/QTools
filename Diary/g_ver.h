﻿#ifndef G_VER
#define G_VER

#define SP_NAME "Diary"
#define SP_VER 1007
#define SP_UID "{9f99bec5-2fd5-45fd-bb19-3a1799cac645}"
#define SP_TYPE "shell"

#define SP_CFG "Diary.ini"
#define SP_INFO "Diary.json"
#define SP_LINK "Diary.lnk"

/*
 * 1001 项目开始，最基本的功能完成
 * 1002 增加自动打开当日日记功能
 * 1003 如果内容没有变化就不需要保存
 * 1004 保存后不再清除内容，增加了手工清空的功能
 * 1006 取消了标题，增加了插入当前时间的功能
 * 下一版本需要加入mysql的支持
*/
#endif // G_VER

