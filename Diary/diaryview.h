﻿#ifndef DIARYVIEW_H
#define DIARYVIEW_H

#include <QDialog>
#include <QString>

namespace Ui {
class DiaryView;
}

class DiaryView : public QDialog
{
    Q_OBJECT

public:
    explicit DiaryView(QWidget *parent = 0);
    ~DiaryView();

    void setContent(const QString &content);

private:
    Ui::DiaryView *ui;
};

#endif // DIARYVIEW_H
