﻿#ifndef SQLITE_HELPER_H
#define SQLITE_HELPER_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QString>
#include <QStandardPaths>
#include <QFile>
#include <QSqlError>
#include <QDebug>

static bool openDatabase(const QString &db_file)
{
    QSqlDatabase db=QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(db_file);
    if(!db.open())
    {
        qDebug() << db.lastError().text();
        return false;
    }
    else
        return true;
}

static bool createDatabase(const QString &db_file, const QString &sql)
{
    if(!openDatabase(db_file))
        return false;
    else
    {
        QSqlQuery query;
        if(!query.exec(sql))
        {
            qDebug() << query.lastError().text();
            return false;
        }
        else
            return true;
    }
}

static bool createDatabaseFromFile(const QString &db_file, const QString &sql_file)
{
    if(!openDatabase(db_file))
        return false;
    else
    {
        QFile file(sql_file);
        file.open(QFile::ReadWrite | QFile::Text);
        QSqlQuery query;
        QString sql=file.readAll();
        if(!query.exec(sql))
        {
            qDebug() << query.lastError().text();
            return false;
        }
        else
            return true;
    }
}

#endif // SQLITE_HELPER_H
