﻿#include "diary.h"
#include "ui_diary.h"

Diary::Diary(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Diary)
{
    ui->setupUi(this);

    newFlag=true;       //默认为新记录
    today=QDate::currentDate().toString("yyyy-M-d");

    model=new QSqlQueryModel(this);
    ui->diaryList->setModel(model);

    firstLoad();
    updateView();
    connect(ui->diaryList,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(showContentView(QModelIndex)));
}

void Diary::firstLoad()
{
    if(getType(today) == "diary")
    {
        //如果已经存在当日记录，就不再是新纪录
        newFlag=false;

        //设置选定日期为今天
        select_date=today;
        showContent(today);
    }
}

void Diary::showContent(const QString &date)
{
    QSqlQuery query;
    query.exec(QString("select content from diary where date='%1'").arg(date));
    if(query.next())
    {
        QString content=query.value(0).toString();
        ui->content->setPlainText(content);
        if(ui->btn_save->isEnabled())
            ui->btn_save->setEnabled(false);
    }
}

Diary::~Diary()
{
    delete ui;
}

void  Diary::updateView()
{
    model->setQuery("select date from diary order by date desc");
    model->setHeaderData(0,Qt::Horizontal,QStringLiteral("日期"));
}

bool Diary::addDiary(const QString &date, const QString &document)
{
    QSqlQuery query;
    query.prepare("insert into diary values (?,?,?)");
    query.addBindValue(date);
    query.addBindValue(document);
    query.addBindValue("diary");
    if(!query.exec())
    {
        qDebug() << query.lastError().text();
        return false;
    }
    else
    {
        newFlag = false;
        return true;
    }
}

bool Diary::updateDiary(const QString &date, const QString &document)
{
    QSqlQuery query;
    QString sql=QString("update diary set content='%1' where date='%2'").arg(document).arg(date);
    if(!query.exec(sql))
    {
        qDebug() << query.lastError().text();
        return false;
    }
    else
        return true;
}

QString Diary::getType(const QString &date)
{
    QSqlQuery query;
    query.exec(QString("select type from diary where date='%1'").arg(date));
    if(query.next())
        return query.value(0).toString();
    else
        return "error";
}

bool Diary::dateDetect()
{
    QDate another=QDate::fromString(select_date,"yyyy-M-d");
    if(another.daysTo(QDate::currentDate()) >=7)        //7天以上不再提供编辑功能
        return false;
    else
        return true;
}

void Diary::on_btn_save_clicked()
{
    QString content=ui->content->toPlainText();

    if(content.isEmpty())
    {
        QMessageBox::warning(this,QStringLiteral("不能为空"),QStringLiteral("内容不能为空！"));
        return;
    }

    if(newFlag)
    {
        if(addDiary(today,content))
            updateView();
    }
    else
    {
        if(!dateDetect())
        {
            QMessageBox::warning(this,QStringLiteral("无法编辑"),QStringLiteral("超过7天，不再提供编辑功能！"));
            return;
        }

        if(updateDiary(select_date,content))
            updateView();
    }

    if(ui->btn_save->isEnabled())
        ui->btn_save->setEnabled(false);
}

void Diary::showContentView(const QModelIndex &index)
{
    if(index.isValid())
    {
        QSqlRecord record=model->record(index.row());
        select_date=record.value("date").toString();
        showContent(select_date);
    }
}

void Diary::on_content_textChanged()
{
    if(!ui->btn_save->isEnabled())
        ui->btn_save->setEnabled(true);
}

void Diary::on_btn_clear_clicked()
{
    ui->content->clear();
}

void Diary::on_btn_time_clicked()
{
    ui->content->appendPlainText(QTime::currentTime().toString());
}
