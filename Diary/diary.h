﻿#ifndef DIARY_H
#define DIARY_H

#include <QDialog>
#include <QMessageBox>
#include <QDate>
#include <QTime>
#include <QUuid>

#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QSqlRecord>
#include <QModelIndex>
#include <QString>
#include <QDebug>

namespace Ui {
class Diary;
}

class Diary : public QDialog
{
    Q_OBJECT

public:
    explicit Diary(QWidget *parent = 0);
    ~Diary();

    void firstLoad();
    void showContent(const QString &date);
    void updateView();

    bool addDiary(const QString &date,const QString &document);
    bool updateDiary(const QString &date,const QString &document);
    QString getType(const QString &date);

    bool dateDetect();

private slots:
    void showContentView(const QModelIndex &index);
    void on_btn_save_clicked();

    void on_content_textChanged();

    void on_btn_clear_clicked();

    void on_btn_time_clicked();

private:
    Ui::Diary *ui;

    QString today;
    QString select_date;
    bool newFlag;
    QSqlQueryModel *model;
};

#endif // DIARY_H
