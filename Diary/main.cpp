﻿#include "diary.h"
#include <QtSingleApplication>
#include "boot/e_boot.h"
#include "sqlite_helper.h"

#define DB_FILE "/QVertex/diary.db"
#define SQL_FILE "../share/sql/create_diary.txt"

int main(int argc, char *argv[])
{
    SP_Single_Boot

    QString userLand=QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QString db_file=userLand + DB_FILE;

    if(!QFile::exists(db_file))
    {
        if(!createDatabaseFromFile(db_file, SQL_FILE))
        {
            QMessageBox::warning(0,QStringLiteral("致命错误"),QStringLiteral("无法创建数据库！"));
            return -1;
        }
    }
    else
    {
        if(!openDatabase(db_file))
        {
            QMessageBox::warning(0,QStringLiteral("致命错误"),QStringLiteral("无法打开数据库！"));
            return -1;
        }
    }

    Diary w;
    a.setActivationWindow(&w);
    w.show();

    return a.exec();
}
