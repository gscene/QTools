#-------------------------------------------------
#
# Project created by QtCreator 2016-02-24T20:33:45
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = Diary
TEMPLATE = app

INCLUDEPATH += ../../elfproj
include(../qtsingleapplication/src/qtsingleapplication.pri)


SOURCES += main.cpp\
        diary.cpp

HEADERS  += diary.h \
    g_ver.h \
    sqlite_helper.h

FORMS    += diary.ui

RESOURCES += \
    res.qrc

RC_ICONS = diary.ico
