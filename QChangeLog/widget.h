#ifndef WIDGET_H
#define WIDGET_H

#include "common/baseeditor.h"
#include "additem.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void generateMenu();
    void updateView();
    void setHeaderData();
    void updateEntry();
    void newItem();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_listView_doubleClicked(const QModelIndex &index);

    void on_listView_clicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QString selectEntry;
    QStringListModel *listModel;
};
#endif // WIDGET_H
