#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QChangeLog"
#define SP_VER 1002.3
#define SP_UID "{1450a9b8-901c-495d-9979-a216c72f7a6b}"
#define SP_TYPE "prop"

#define SP_CFG "QChangeLog.ini"
#define SP_INFO "QChangeLog.json"
#define SP_LINK "QChangeLog.lnk"

#endif // G_VER_H
