#ifndef ADDITEM_H
#define ADDITEM_H

#include <head/g_pch.h>
#include <head/db_helper.h>
#include <head/m_message.h>
#include "m_fhs.h"

namespace Ui {
class AddItem;
}

class AddItem : public QDialog
{
    Q_OBJECT

public:
    explicit AddItem(QWidget *parent = nullptr);
    ~AddItem();

    void updateEntry();
    void prepare(const QString &entry);
    bool addItem(const QString &entry,
                 double version,
                 const QString &detail);

private slots:
    void on_btn_submit_clicked();

    void on_entries_currentTextChanged(const QString &text);

private:
    Ui::AddItem *ui;

    QString table;
    QStringList entries;
};

#endif // ADDITEM_H
