#-------------------------------------------------
#
# Project created by QtCreator 2017-06-02T13:23:40
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TraitSearch
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
    ../../elfproj/support/sp_env.cpp \
    manager.cpp \
    additem.cpp \
    widget.cpp

HEADERS  += \
    ../../elfproj/head/g_pch.h \
    g_ver.h \
    ../../elfproj/support/sp_env.h \
    manager.h \
    m_fhs.h \
    additem.h \
    widget.h \
    ../../elfproj/common/baseeditor.h

FORMS    += \
    manager.ui \
    additem.ui \
    widget.ui

RC_ICONS = search.ico

RESOURCES += \
    res.qrc
