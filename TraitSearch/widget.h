﻿#ifndef WIDGET_H
#define WIDGET_H

#include "manager.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void updateCategories();
    void updateTraits();

    void loadPreference();
    void savePreference();

private slots:
    void on_categories_currentTextChanged(const QString &category);
    void on_traits_currentTextChanged(const QString &trait);
    void on_keyword_returnPressed();
    void on_btn_manager_clicked();

private:
    Ui::Widget *ui;

    Manager *manager;
    QString table;
    QString configFile;
    QString selectCategory;
    QString selectTrait;
};

#endif // WIDGET_H
