#ifndef MANAGER_H
#define MANAGER_H

#include <common/baseeditor.h>
#include <additem.h>

namespace Ui {
class Manager;
}

class Manager : public BaseEditor
{
    Q_OBJECT

public:
    explicit Manager(QWidget *parent = nullptr,
                     Qt::WindowFlags f=Qt::Dialog);
    ~Manager();

    void updateView();
    void generateMenu();
    void newItem();
    void removeItem();

private:
    Ui::Manager *ui;
};

#endif // MANAGER_H
