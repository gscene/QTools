﻿#include "additem.h"
#include "ui_additem.h"

AddItem::AddItem(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddItem)
{
    ui->setupUi(this);

    table=TD_PREFIX;
    updateCategory();
}

AddItem::~AddItem()
{
    delete ui;
}

void AddItem::updateCategory()
{
    if(ui->categories->count() != 0)
        ui->categories->clear();

    QStringList items=sp_getCategory(table);
    if(!items.isEmpty())
        ui->categories->addItems(items);

    ui->categories->addItem(MANUAL_INPUT);
    ui->categories->setCurrentIndex(-1);
}

bool AddItem::addItem(const QString &category,
                      const QString &label,
                      const QString &alias,
                      const QString &detail,
                      const QString &addition)
{
    QSqlQuery query;
    query.prepare(QString("insert into %1 (category,label,alias,detail,addition) values (?,?,?,?,?)")
                  .arg(table));
    query.addBindValue(category);
    query.addBindValue(label);
    query.addBindValue(alias);
    query.addBindValue(detail);
    query.addBindValue(addition);
    if(query.exec())
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void AddItem::on_categories_currentTextChanged(const QString &category)
{
    if(category == MANUAL_INPUT)
    {
        ui->category->clear();
        ui->category->setEnabled(true);
    }
    else
    {
        ui->category->setText(category);
        ui->category->setEnabled(false);
    }
}

void AddItem::on_btn_submit_clicked()
{
    QString category=ui->category->text().trimmed();
    if(category.isEmpty())
    {
        MESSAGE_CATEGORY_EMPTY
    }

    QString label=ui->label->text().trimmed();
    QString alias=ui->alias->text().trimmed();
    QString detail=ui->detail->text().trimmed();
    if(label.isEmpty() || detail.isEmpty())
    {
        MESSAGE_DETAIL_EMPTY
    }

    QString addition=ui->addition->text().trimmed();
    if(addItem(category,label,alias,detail,addition))
        accept();
    else
        MESSAGE_CANNOT_SUBMIT
}

void AddItem::on_btn_paste_clicked()
{
    QString detail=qApp->clipboard()->text();
    ui->detail->setText(detail);
}
