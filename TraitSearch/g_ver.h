﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "TraitSearch"
#define SP_VER 1010
#define SP_UID "{c891b102-424e-41f2-a11c-0c9ecf5a8ac2}"
#define SP_TYPE "prop"

#define SP_CFG "TraitSearch.ini"
#define SP_INFO "TraitSearch.json"
#define SP_LINK "TraitSearch.lnk"

/*
 * 1001
 * 1002 增加了位置记忆功能,last
 * 1003 mysql_helper
 * 1004 重新设计
 * 1005 Main/category
 * 1006 updateCategories
 * 1007 Manager,AddItem
 * 1008 quickConnectFromSHM
 * 1009 隐藏Manager的alias
 * 1010 addItem增加alias
*/
#endif // G_VER_H
