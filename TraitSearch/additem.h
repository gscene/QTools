﻿#ifndef ADDITEM_H
#define ADDITEM_H

#include <head/g_pch.h>
#include <head/db_helper.h>
#include <head/m_message.h>
#include "m_fhs.h"

#define MANUAL_INPUT QStringLiteral("-手动输入-")

namespace Ui {
class AddItem;
}

class AddItem : public QDialog
{
    Q_OBJECT

public:
    explicit AddItem(QWidget *parent = nullptr);
    ~AddItem();

    void updateCategory();

    bool addItem(const QString &category,
                 const QString &label,
                 const QString &alias,
                 const QString &detail,
                 const QString &addition);

private slots:
    void on_categories_currentTextChanged(const QString &category);

    void on_btn_submit_clicked();

    void on_btn_paste_clicked();

private:
    Ui::AddItem *ui;

    QString table;
};

#endif // ADDITEM_H
