﻿#include "manager.h"
#include "ui_manager.h"

Manager::Manager(QWidget *parent, Qt::WindowFlags f) :
    BaseEditor(parent,f),
    ui(new Ui::Manager)
{
    ui->setupUi(this);

    table=TD_PREFIX;
    isEdit=true;

    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);

    updateView();
    createMenu();
}

Manager::~Manager()
{
    delete ui;
}

void Manager::generateMenu()
{
    menu->addAction(QStringLiteral("添加... (F1)"),this,&BaseEditor::newItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("撤销 (Ctrl+Z)"),this,&BaseEditor::revert);
    menu->addAction(QStringLiteral("保存 (Ctrl+S)"),this,&BaseEditor::save);
    menu->addSeparator();
    menu->addAction(QStringLiteral("删除 (Delete)"),this,&BaseEditor::removeItem);
}

void Manager::updateView()
{
    model->setTable(table);
    model->select();

    ui->tableView->hideColumn(0);   //ID
    model->setHeaderData(COL_CAT,Qt::Horizontal,QStringLiteral("Category"));
    model->setHeaderData(COL_LAB,Qt::Horizontal,QStringLiteral("Label"));
    ui->tableView->setColumnWidth(COL_LAB,160);
    ui->tableView->hideColumn(COL_ALI);   //alias
    model->setHeaderData(COL_DET,Qt::Horizontal,QStringLiteral("Detail"));
    ui->tableView->setColumnWidth(COL_DET,480);
    model->setHeaderData(COL_ADD,Qt::Horizontal,QStringLiteral("Addition"));
}

void Manager::newItem()
{
    AddItem item;
    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Manager::removeItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
        model->removeRow(index.row());
}
