﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define COL_CAT 1
#define COL_LAB 2
#define COL_ALI 3
#define COL_DET 4
#define COL_ADD 5

#endif // M_FHS_H
