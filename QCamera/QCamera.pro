#-------------------------------------------------
#
# Project created by QtCreator 2017-01-23T22:02:30
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QCamera
TEMPLATE = app

INCLUDEPATH += ../../elfproj
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
        mainwindow.cpp \
    info.cpp \
    captureview.cpp

HEADERS  += mainwindow.h \
    info.h \
    captureview.h \
    g_ver.h

FORMS    += mainwindow.ui \
    info.ui

RESOURCES += \
    res.qrc

RC_ICONS = camera.ico
