﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStatusBar>

#include <QDateTime>

#include <QCameraInfo>
#include <QCamera>
#include <QCameraViewfinder>
#include <QCameraViewfinderSettings>
#include <QCameraImageCapture>
#include <QMediaRecorder>
#include <QVideoEncoderSettings>

#include <QSize>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QUrl>
#include <QDir>

#include <QKeyEvent>
#include <QDebug>

#include "info.h"
#include "captureview.h"

#define RES_720P QSize(1280,720)
#define RES_1080P QSize(1920,1080)

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QList<QVideoFrame::PixelFormat> getSupportedPixelFormats();
    QList<QSize> getSupportedResolutions();
    void setDefaultSetting();
    void setCurrentOutputFile();
    void stateChanged(QMediaRecorder::State state);

    void imageSaved(int id, const QString &fileName);
    void imageCaptured(int id,const QImage &image);

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_actionInfo_triggered();

    void on_actionCapture_triggered();

    void on_actionPicture_triggered();

    void on_action720P_triggered();

    void on_action1080P_triggered();

    //  void on_actionVideo_triggered();

    void on_actionRecord_triggered();

private:
    Ui::MainWindow *ui;

    QString pictureLocation;
    QString recordLocation;

    QString cameraDesc;
    QCamera *camera;
    QCameraViewfinder *viewer;
    QCameraImageCapture *imageCapture;
    QMediaRecorder *mediaRecorder;

    QSize currentResolution;
    CaptureView *preview;

    CameraInfoDialog *info;
    int captureId;
};

#endif // MAINWINDOW_H
