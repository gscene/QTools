﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QCamera"
#define SP_VER 1003
#define SP_UID "{5f0a2ff7-b4e2-4331-a284-045ca51cad4e}"
#define SP_TYPE "shell"

#define SP_INFO "QCamera.json"
#define SP_LINK "QCamera.lnk"

/*
 * 1001 项目开始
 * 1002 capture预览
 * 1003 修正
*/

#endif // G_VER_H
