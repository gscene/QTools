﻿#ifndef INFO_H
#define INFO_H

#include <QDialog>
#include <QString>
#include <QList>
#include <QSize>

#include <QCameraInfo>
#include <QCamera>
#include <QVideoFrame>

namespace Ui {
class CameraInfoDialog;
}


class CameraInfoDialog : public QDialog
{
    Q_OBJECT
public:
    explicit CameraInfoDialog(QWidget *parent = 0);
    ~CameraInfoDialog();

    void appendAvailableCameraInfo();
    void appendPixelFormatInfo(const QList<QVideoFrame::PixelFormat> &formats);
    void appendResolutionInfo(const QList<QSize> &sizes);

private:
    Ui::CameraInfoDialog *ui;
};

#endif // INFO_H
