﻿#include "info.h"
#include "ui_info.h"

CameraInfoDialog::CameraInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CameraInfoDialog)
{
    ui->setupUi(this);
}

CameraInfoDialog::~CameraInfoDialog()
{
    delete ui;
}

void CameraInfoDialog::appendAvailableCameraInfo()
{
    ui->detail->appendPlainText("AvailableCameras: ");
    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
    foreach (const QCameraInfo &cameraInfo, cameras) {
        ui->detail->appendPlainText("description: " + cameraInfo.description());
        ui->detail->appendPlainText("deviceName: " + cameraInfo.deviceName());
        ui->detail->appendPlainText("\n");
    }
}

void CameraInfoDialog::appendPixelFormatInfo(const QList<QVideoFrame::PixelFormat> &formats)
{
    ui->detail->appendPlainText("SupportedPixelFormats: ");
    foreach (QVideoFrame::PixelFormat format, formats) {
        ui->detail->appendPlainText(QString::number(format));
    }
    ui->detail->appendPlainText("\n");
}

void CameraInfoDialog::appendResolutionInfo(const QList<QSize> &sizes)
{
    ui->detail->appendPlainText("SupportedResolutions: ");
    foreach (QSize size, sizes) {
        ui->detail->appendPlainText(QString::number(size.width())
                                    + " x " + QString::number(size.height()));
    }
    ui->detail->appendPlainText("\n");
}
