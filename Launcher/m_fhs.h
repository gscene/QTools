﻿#ifndef M_FHS_H
#define M_FHS_H

#include "g_ver.h"
#include "head/g_fhs.h"

#define M_CONFIG QString(FHS_ETC) + SP_CFG
#define M_ENTRY "Common/entry"

#define M_KEY "Launcher/target"

#define CFG_UID "../etc/sp_uid.json"
#define CFG_NAME "../etc/sp_name.json"

#endif // M_FHS_H
