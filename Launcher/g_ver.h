﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Launcher"
#define SP_VER 1012
#define SP_UID "{5da8d639-db13-476d-90df-cdcbb7551f47}"
#define SP_TYPE "extra"

#define SP_CFG "Launcher.ini"
#define SP_INFO "Launcher.json"
#define SP_LINK "Launcher.lnk"

/*
 *  1005 eject 弹射函数
 *  1006 取消了propList
 *  1007 QMessageBox需要使用QApplication
 *  修正了设置程序名称的bug
 *  1008 可以为target传递参数
 *  1009 修正中文参数的问题
 *  1010 #13
 *  1011 #16
 * 1012 #20
 */

#endif // G_VER_H
