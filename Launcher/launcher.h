﻿#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QDir>
#include <QString>
#include <QStringList>
#include <QSettings>
#include <QProcess>

#include "m_fhs.h"

class Launcher
{
public:
    static void eject(const QString &item);
    static void ejectWithParam(const QString &item,const QString &param);
    static void launch();
};

#endif // LAUNCHER_H
