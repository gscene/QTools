#-------------------------------------------------
#
# Project created by QtCreator 2016-09-19T07:32:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = Launcher
TEMPLATE = app

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        launcher.cpp

HEADERS  += launcher.h \
    g_ver.h \
    m_fhs.h

RESOURCES += \
    res.qrc

RC_ICONS = rocket.ico
