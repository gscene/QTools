﻿#include "launcher.h"
#include <QApplication>

#define SP_DIRECT_FLAG

#include "head/e_boot.h"

int main(int argc, char *argv[])
{
    SP_Boot
            if(argc == 2)
    {
        QString item=QString::fromLocal8Bit(argv[1]);
        Launcher::eject(item);
        return EXIT_SUCCESS;
    }

    if(argc == 3)
    {
        QString item=QString::fromLocal8Bit(argv[1]);
        QString param=QString::fromLocal8Bit(argv[2]);
        Launcher::ejectWithParam(item,param);
        return EXIT_SUCCESS;
    }

    Launcher::launch();
    return EXIT_SUCCESS;
}
