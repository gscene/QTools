﻿#include "launcher.h"

void Launcher::launch()
{
    QSettings set(M_CONFIG,QSettings::IniFormat);
    if(!set.contains(M_KEY))
        return;

    QString item=set.value(M_KEY).toString();
    if(item.isEmpty())
        return;

    if(item.contains(","))
    {
        QStringList itemList=item.split(",");
        foreach (QString sep, itemList) {
            Launcher::eject(sep);
        }
    }
    else
        Launcher::eject(item);
}

void Launcher::eject(const QString &item)
{
    QStringList fileList=QDir::current().entryList(QStringList() << (item + "*"));
    if(!fileList.isEmpty())
        QProcess::startDetached(fileList.last());
    else
        QProcess::startDetached(item);
}

void Launcher::ejectWithParam(const QString &item,const QString &param)
{
    QStringList fileList=QDir::current().entryList(QStringList() << (item + "*"));
    if(!fileList.isEmpty())
        QProcess::startDetached(fileList.last() + " " + param);
    else
        QProcess::startDetached(item + " " + param);
}
