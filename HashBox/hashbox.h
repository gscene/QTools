﻿#ifndef HASHBOX_H
#define HASHBOX_H

#include <head/g_pch.h>
#include <element/verifyworker.h>

namespace Ui {
class HashBox;
}

class HashBox : public QDialog
{
    Q_OBJECT

public:
    explicit HashBox(QWidget *parent = 0);
    ~HashBox();

    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);

signals:
    void sendRequest(QCryptographicHash::Algorithm method,const QString &fileName);

private slots:
    void on_btn_open_clicked();
    void on_url_textChanged(const QString &url);
    void on_code_textChanged(const QString &code);
    void on_calc256_clicked();
    void on_calc384_clicked();
    void on_calc512_clicked();
    void receiveResult(QCryptographicHash::Algorithm method,const QByteArray &result);
    void on_btn_paste_clicked();

private:
    Ui::HashBox *ui;

    VerifyWorker *worker;
    QThread workThread;

    QString fileName;
    QStringList hashList;
};

#endif // HASHBOX_H
