#-------------------------------------------------
#
# Project created by QtCreator 2016-09-11T20:11:48
#
#-------------------------------------------------

QT       += core network gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HashBox
TEMPLATE = app

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        hashbox.cpp

HEADERS  += hashbox.h \
    g_ver.h \
    ../../elfproj/element/verifyworker.h

FORMS    += hashbox.ui

RESOURCES += \
    res.qrc

RC_ICONS = hash.ico
