﻿#include "hashbox.h"
#include "ui_hashbox.h"

HashBox::HashBox(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HashBox)
{
    ui->setupUi(this);
    ui->url->setAcceptDrops(true);

    qRegisterMetaType<QCryptographicHash::Algorithm>("QCryptographicHash::Algorithm");

    worker=new VerifyWorker;
    connect(&workThread,&QThread::finished,worker,&QObject::deleteLater);

    connect(this,&HashBox::sendRequest,worker,&VerifyWorker::verifyHash);
    connect(worker,&VerifyWorker::sendResult,this,&HashBox::receiveResult);

    worker->moveToThread(&workThread);
    workThread.start();
}

HashBox::~HashBox()
{
    workThread.quit();
    workThread.wait();

    delete ui;
}

void HashBox::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasFormat("text/uri-list"))
        event->acceptProposedAction();
}

void HashBox::dropEvent(QDropEvent *event)
{
    QList<QUrl> urls = event->mimeData()->urls();
    if(urls.isEmpty())
        return;

    QString fileName=urls.first().toLocalFile();
    ui->url->setText(fileName);
}

void HashBox::on_btn_open_clicked()
{
    QUrl url=QFileDialog::getOpenFileUrl(this,QStringLiteral("选择文件"));
    QString fileName = url.toLocalFile();
    if(fileName.isEmpty())
        return;

    ui->url->setText(fileName);
}

void HashBox::on_url_textChanged(const QString &url)
{
    if(!hashList.isEmpty())
        hashList.clear();

    fileName=url;

    ui->md5->setText(QStringLiteral("稍等，计算中..."));
    emit sendRequest(QCryptographicHash::Md5,fileName);

    ui->sha1->setText(QStringLiteral("稍等，计算中..."));
    emit sendRequest(QCryptographicHash::Sha1,fileName);
}

void HashBox::on_code_textChanged(const QString &code)
{
    if(hashList.contains(code))
        ui->status->setText("YES");
    else
        ui->status->setText("NO");
}

void HashBox::on_calc256_clicked()
{
    if(fileName.isEmpty())
        return;

    ui->sha256->setText(QStringLiteral("稍等，计算中..."));
    emit sendRequest(QCryptographicHash::Sha256,fileName);
}


void HashBox::on_calc384_clicked()
{
    if(fileName.isEmpty())
        return;

    ui->sha384->setText(QStringLiteral("稍等，计算中..."));
    emit sendRequest(QCryptographicHash::Sha384,fileName);
}

void HashBox::on_calc512_clicked()
{
    if(fileName.isEmpty())
        return;

    ui->sha512->setText(QStringLiteral("稍等，计算中..."));
    emit sendRequest(QCryptographicHash::Sha512,fileName);
}

void HashBox::receiveResult(QCryptographicHash::Algorithm method, const QByteArray &result)
{
    switch (method) {
    case QCryptographicHash::Md5:
        hashList.append(QString(result));
        ui->md5->setText(QString(result));
        break;
    case QCryptographicHash::Sha1:
        hashList.append(QString(result));
        ui->sha1->setText(QString(result));
        break;
    case QCryptographicHash::Sha256:
        hashList.append(QString(result));
        ui->sha256->setText(QString(result));
        break;
    case QCryptographicHash::Sha384:
        hashList.append(QString(result));
        ui->sha384->setText(QString(result));
        break;
    case QCryptographicHash::Sha512:
        hashList.append(QString(result));
        ui->sha512->setText(QString(result));
        break;
    }
}

void HashBox::on_btn_paste_clicked()
{
    QString hash=qApp->clipboard()->text().trimmed();
    if(!hash.isEmpty())
        ui->code->setText(hash);
}
