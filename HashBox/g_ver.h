﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "HashBox"
#define SP_VER 1004
#define SP_UID "{bb229408-1327-4d5a-995c-6d27892bc69a}"
#define SP_TYPE "shell"

#define SP_CFG "HashBox.ini"
#define SP_INFO "HashBox.json"
#define SP_LINK "HashBox.lnk"

/*
 * 1001 项目开始
 * 1002 使用子线程计算
 * 1003 修改了读取文件时的流程
 * 1004 #13
*/
#endif // G_VER_H
