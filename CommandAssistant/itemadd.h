﻿#ifndef ITEMADD_H
#define ITEMADD_H

#include "support/sp_env.h"
#include "m_fhs.h"

#define TD_CMD "ref_commands"

namespace Ui {
class ItemAdd;
}

class ItemAdd : public QDialog
{
    Q_OBJECT

public:
    explicit ItemAdd(QWidget *parent = 0);
    ~ItemAdd();

    bool addItem(const QString &label,
                 const QString &alias,
                 const QString &hexcode,
                 int code,
                 const QString &detail,
                 const QString &addition);

private slots:
    void on_btn_submit_clicked();

private:
    Ui::ItemAdd *ui;
};

#endif // ITEMADD_H
