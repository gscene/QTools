﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "CommandAssistant"
#define SP_VER 1002
#define SP_UID "{e1813e01-28ba-456d-872a-ee4702766811}"
#define SP_TYPE "shell"

#define SP_CFG "CommandAssistant.ini"
#define SP_INFO "CommandAssistant.json"
#define SP_LINK "CommandAssistant.lnk"

/*
 * 1001 开始
 * 1002 修正表结构
*/

#endif // G_VER_H
