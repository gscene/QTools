﻿#ifndef EDITOR_H
#define EDITOR_H

#include "common/baseeditordialog.h"
#include "itemadd.h"

namespace Ui {
class Editor;
}

class Editor : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit Editor(QWidget *parent = 0);
    ~Editor();

    void updateView();
    void search();

    void newItem();
    void removeItem();

    void edit_off();
    void edit_on();

private slots:
    void on_tableView_doubleClicked(const QModelIndex &);

private:
    Ui::Editor *ui;
};

#endif // EDITOR_H
