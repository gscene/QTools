﻿#include "editor.h"
#include "ui_editor.h"

Editor::Editor(QWidget *parent) :
    BaseEditorDialog(parent),
    ui(new Ui::Editor)
{
    ui->setupUi(this);

    table=TD_CMD;

    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
    ui->tableView->setModel(model);

    updateView();
    createMenu();
}

Editor::~Editor()
{
    delete ui;
}

void Editor::updateView()
{
    model->setTable(table);
    model->select();

    model->setHeaderData(0,Qt::Horizontal,QStringLiteral("命令"));    //label
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("别名"));    //alias
    model->setHeaderData(2,Qt::Horizontal,QStringLiteral("十六进制代码"));    //hexcode
    model->setHeaderData(3,Qt::Horizontal,QStringLiteral("十进制代码"));    //code
    model->setHeaderData(4,Qt::Horizontal,QStringLiteral("说明"));    //detail
    model->setHeaderData(5,Qt::Horizontal,QStringLiteral("附加说明"));    //addition
}

void Editor::edit_off()
{
    isEdit=false;
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
    updateMenu();
}

void Editor::edit_on()
{
    isEdit=true;
    ui->tableView->setEditTriggers(QTableView::DoubleClicked);
    updateMenu();
}

void Editor::newItem()
{
    ItemAdd item;
    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Editor::removeItem()
{
    if(ui->tableView->currentIndex().isValid())
    {
        int row=ui->tableView->currentIndex().row();
        model->removeRow(row);
    }
}

void Editor::search()
{
    showSearchBox("label");
}

void Editor::on_tableView_doubleClicked(const QModelIndex &)
{
    if(!isEdit)
        newItem();
}
