#-------------------------------------------------
#
# Project created by QtCreator 2016-12-14T20:01:44
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CommandAssistant
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)

INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

SOURCES += main.cpp\
    ../../elfproj/common/baseeditordialog.cpp \
        editor.cpp \
    itemadd.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += editor.h \
    ../../elfproj/common/baseeditordialog.h \
    g_ver.h \
    itemadd.h \
    ../common/m_fhs.h \
    ../../elfproj/support/sp_env.h

FORMS    += editor.ui \
    itemadd.ui
