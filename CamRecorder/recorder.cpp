﻿#include "recorder.h"
#include "ui_recorder.h"

Recorder::Recorder(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Recorder)
{
    ui->setupUi(this);

    this->setWindowTitle("Recorder");
    value=0;
    isRecording=false;
    baseTime=QTime::fromString(ZERO_TIME,"hh:mm:ss");
    ui->lcd->display(ZERO_TIME);

    skin_start;
}

Recorder::~Recorder()
{
    delete ui;
}

void Recorder::detectState()
{
    if(!isRecording)
        state_start();
    else
        state_stop();
}

void Recorder::state_start()
{
    isRecording=true;
    skin_stop;
    ui->status->setText(QStringLiteral("录制中……"));
    timerId=startTimer(1000);
    emit start();
}

void Recorder::state_stop()
{
    isRecording=false;
    skin_start;
    value=0;
    ui->lcd->display(ZERO_TIME);
    ui->status->setText(QStringLiteral("已停止"));
    killTimer(timerId);
    emit stop();
}

void Recorder::on_btn_control_clicked()
{
    detectState();
}

void Recorder::timerEvent(QTimerEvent *event)
{
    if(event->timerId() == timerId)
    {
        value +=1;
        QTime time=baseTime.addSecs(value);
        ui->lcd->display(time.toString("hh:mm:ss"));
    }
}

void Recorder::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Space:
        detectState();
        break;
    case Qt::Key_Escape:
        if(isRecording)
            state_stop();
        else
            close();
        break;
    }
}
