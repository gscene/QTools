﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStatusBar>
#include <QLabel>
#include <QDateTime>
#include <QSize>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QUrl>
#include <QDir>
#include <QTimer>
#include <QFileDialog>

#include "opencv2/opencv.hpp"
#include <string>
#include "recorder.h"

#include <QDebug>

#define MAX_CAMERA_COUNT 2
#define DEFAULT_FPS 30
#define RES_720P QSize(1280,720)
#define RES_1080P QSize(1920,1080)
#define CV_RES_720P Size(1280,720)
#define CV_RES_1080P Size(1920,1080)

using namespace std;
using namespace cv;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    //int getCameraCount();
    QImage convertMat2Image(const Mat &cvImg);
    void showVideoInfo();
    void openCamera();
    QString generateRecordFile() const;
    void nextFrame();

    void startRecord();
    void stopRecord();

private slots:
    void on_actionOpen_triggered();
    void on_actionCamera_triggered();
    void on_actionRecord_triggered();
    void on_actionVideoLocation_triggered();

    void on_actionRecordLocation_triggered();

private:
    Ui::MainWindow *ui;
    Recorder *recorder;

    QLabel *display;
    QString videoLocation;
    QString recordLocation;
    QTimer *timer;

    VideoCapture capture;
    VideoWriter writer;
    Mat frame;
    QImage image;
    double rate;    //FPS
    bool isRecording;
};

#endif // MAINWINDOW_H
