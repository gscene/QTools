﻿#ifndef RECORDER_H
#define RECORDER_H

#include <QDialog>
#include <QTime>
#include <QTimerEvent>
#include <QKeyEvent>
#include <QDateTime>

#define ZERO_TIME "00:00:00"
#define skin_start ui->btn_control->setStyleSheet("border-image: url(:/res/start.png);")
#define skin_stop ui->btn_control->setStyleSheet("border-image: url(:/res/stop.png);")

namespace Ui {
class Recorder;
}

class Recorder : public QDialog
{
    Q_OBJECT

public:
    explicit Recorder(QWidget *parent = 0);
    ~Recorder();

    void detectState();
    void state_start();
    void state_stop();

    void timerEvent(QTimerEvent *event);
    void keyPressEvent(QKeyEvent *event);

signals:
    void start();
    void stop();

private slots:
    void on_btn_control_clicked();

private:
    Ui::Recorder *ui;

    QTime baseTime;
    int timerId;
    int value;
    bool isRecording;
};

#endif // RECORDER_H
