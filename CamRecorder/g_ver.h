﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "CamRecorder"
#define SP_VER 1004
#define SP_UID "{a38d2356-690e-4b35-a213-ddea04cd305d}"
#define SP_TYPE "shell"

#define SP_INFO "CamRecorder.json"
#define SP_LINK "CamRecorder.lnk"

/*
 * 1001 项目开始
 * 1002
 * 1003 修正同时预览与录制
 * 1004 精简关闭过程
*/

/*
 * CV_CAP_PROP_FPS 帧率
 * CV_CAP_PROP_FRAME_WIDTH
 * CV_CAP_PROP_FRAME_HEIGHT
 * CV_CAP_PROP_FOURCC
 * CV_CAP_PROP_FRAME_COUNT
 *
*/
#endif // G_VER_H
