#-------------------------------------------------
#
# Project created by QtCreator 2017-02-26T08:58:22
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CamRecorder
TEMPLATE = app

include(../../elfproj/include/opencv_prefix.pri)

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
        mainwindow.cpp \
    recorder.cpp

HEADERS  += mainwindow.h \
    g_ver.h \
    recorder.h

FORMS    += mainwindow.ui \
    recorder.ui

RESOURCES += \
    res.qrc

RC_ICONS = recorder.ico
