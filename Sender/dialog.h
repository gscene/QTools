#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QMessageBox>
#include "head/g_functionbase.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_addressType_currentTextChanged(const QString &text);
    void on_btn_sendCmd_clicked();
    void on_btn_sendMsg_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
