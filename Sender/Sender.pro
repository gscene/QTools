#-------------------------------------------------
#
# Project created by QtCreator 2017-02-11T14:51:44
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Sender
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        dialog.cpp

HEADERS  += dialog.h \
    m_fhs.h

FORMS    += dialog.ui

RESOURCES += \
    res.qrc

RC_ICONS = sender.ico
