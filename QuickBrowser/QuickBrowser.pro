#-------------------------------------------------
#
# Project created by QtCreator 2017-03-22T15:38:02
#
#-------------------------------------------------

QT       += core gui network sql webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QuickBrowser
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += main.cpp \
    mainwindow.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += \
    mainwindow.h \
    g_ver.h \
    m_fhs.h \
    ../../elfproj/support/sp_env.h

FORMS += \
    mainwindow.ui

RESOURCES += \
    res.qrc

RC_ICONS = browser.ico
