﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QuickBrowser"
#define SP_VER 1006
#define SP_UID "{be57f278-ec72-480a-b33e-62f30e1ea475}"
#define SP_TYPE "prop"

#define SP_CFG "QuickBrowser.ini"
#define SP_INFO "QuickBrowser.json"
#define SP_LINK "QuickBrowser.lnk"

/*
 * 1001
 * 1002 bookmarks
 * 1003 p_site,td_prefix
 * 1004 p_site_trace
 * 1005 首页功能
 * 1006 简化
*/

#endif // G_VER_H
