﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <head/g_pch.h>
#include "support/sp_env.h"
#include "m_fhs.h"
#include <QWebEngineView>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void createNavbar();
    void createAppBar();
    void updateFavoriteBar();
    void updateMainBar();
    void loadHomePage();

    bool tickUp(const QString &label,const QString &detail);

    void load(const QString &url);
    void loadUrl(const QString &url);

    void onLoadStart();
    void onLoadFinished(bool flag);
    //   void onUrlChanged(const QUrl &url);
    void onTitleChanged(const QString &title);
    void onPageLinkHovered(const QString &url);

    void do_openFile();
    void do_openUrl();
    void do_updateNavbar();
    void do_setHomePage();

    void processFavoriteAction(QAction *action);
    void processMainAction(QAction *action);

protected:
    void mouseReleaseEvent(QMouseEvent *event);

private:
    Ui::MainWindow *ui;

    QString configFile;
    QString targetUrl;
    QWebEngineView *view;
    QMap<QString,QString> items;
    QMap<QString,QString> favItems;
};

#endif // MAINWINDOW_H
