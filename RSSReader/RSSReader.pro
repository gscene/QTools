#-------------------------------------------------
#
# Project created by QtCreator 2019-04-15T21:55:48
#
#-------------------------------------------------

QT       += core gui sql network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RSSReader
TEMPLATE = app

CONFIG += C++11
DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
        main.cpp \
        widget.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    ../../elfproj/element/basehttpreceiver.h \
    m_fhs.h

FORMS += \
        widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
