#ifndef WIDGET_H
#define WIDGET_H

#include <head/g_pch.h>
#include <head/db_helper.h>
#include <element/basehttpreceiver.h>
#include <support/g_xml.h>
#include "m_fhs.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

    void updateRepos();
    void startReceiver();
    void stopReceiver();
    void onReceived(const QByteArray &data);

signals:
    void get(const QString &url);

private slots:
    void on_btn_paste_clicked();
    void on_btn_submit_clicked();
    void on_listView_doubleClicked(const QModelIndex &index);
    void on_reposView_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    BaseHttpReceiver *receiver;
    QThread recvThread;

    QString table;
    QString url;
    QMenu *menu;
    QStringListModel *repoModel;
    QStringListModel *itemModel;
    QMap<QString,QString> itemMap;
};

#endif // WIDGET_H
