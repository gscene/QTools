#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "RSSReader"
#define SP_VER 1004
#define SP_UID "{9a49576c-1781-49b0-bf4e-306ca3107697}"
#define SP_TYPE "prop"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "RSSReader.ini"
#define SP_INFO "RSSReader.json"
#define SP_LINK "RSSReader.lnk"

#endif // G_VER_H
