QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
    ../../elfproj/support/sp_env.cpp \
    main.cpp \
    p_additem.cpp \
    widget.cpp

HEADERS += \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/support/sp_env.h \
    g_ver.h \
    m_fhs.h \
    p_additem.h \
    widget.h

FORMS += \
    p_additem.ui \
    widget.ui

RESOURCES += \
    res.qrc

RC_ICONS = key.ico
