﻿#include "p_additem.h"
#include "ui_p_additem.h"

P_AddItem::P_AddItem(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::P_AddItem)
{
    ui->setupUi(this);

    id=0;
    table=KIM_PASSWD;
    ui->types->addItems(QStringList()
                        << T_URL << T_RDP
                        << T_SSH << T_NULL);
    ui->types->setCurrentIndex(3);
    updateCategory();
}

P_AddItem::~P_AddItem()
{
    delete ui;
}

void P_AddItem::updateCategory()
{
    if(ui->categories->count() != 0)
        ui->categories->clear();

    auto items=sp_getCategory(table);
    if(!items.isEmpty())
        ui->categories->addItems(items);
    ui->categories->addItem(MANUAL_INPUT);
}

void P_AddItem::setId(int id_)
{
    id=id_;
    QSqlQuery query;
    query.exec(QString("select category,label,source,account,passwd from %1 where id=%2")
               .arg(table)
               .arg(id));
    if(query.next())
    {
        ui->categories->setCurrentText(query.value(0).toString());
        ui->label->setText(query.value(1).toString());
        ui->source->setText(query.value(2).toString());
        ui->account->setText(query.value(3).toString());
        ui->passwd->setText(query.value(4).toString());
        ui->btn_submit->setText("保存");
    }
}

void P_AddItem::fetchById(int id_)
{
    QSqlQuery query;
    query.exec(QString("select category,label,source from %1 where id=%2")
               .arg(table)
               .arg(id_));
    if(query.next())
    {
        ui->categories->setCurrentText(query.value(0).toString());
        ui->label->setText(query.value(1).toString());
        ui->source->setText(query.value(2).toString());
    }
}

void P_AddItem::on_categories_currentTextChanged(const QString &text)
{
    if(text == MANUAL_INPUT)
    {
        ui->category->clear();
        ui->category->setEnabled(true);
    }
    else
    {
        ui->category->setText(text);
        ui->category->setEnabled(false);
    }
}

bool P_AddItem::addItem(const QString &category,
                        const QString &label,
                        const QString &source,
                        const QString &account,
                        const QString &passwd)
{
    QSqlQuery query;
    query.prepare(QString("insert into %1 (category,label,source,account,passwd) values (?,?,?,?,?)")
                  .arg(table));
    query.addBindValue(category);
    query.addBindValue(label);
    query.addBindValue(source);
    query.addBindValue(account);
    query.addBindValue(passwd);
    if(query.exec())
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

bool P_AddItem::updateItem(const QString &category,
                           const QString &label,
                           const QString &source,
                           const QString &account,
                           const QString &passwd)
{
    QSqlQuery query;
    query.prepare(QString("update %1 set category=?,label=?,source=?,account=?,passwd=? where id=%2")
                  .arg(table)
                  .arg(id));
    query.addBindValue(category);
    query.addBindValue(label);
    query.addBindValue(source);
    query.addBindValue(account);
    query.addBindValue(passwd);
    if(query.exec())
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void P_AddItem::on_btn_submit_clicked()
{
    QString category=ui->category->text().trimmed();
    if(category.isEmpty())
        category="默认";
    QString label=ui->label->text().trimmed();
    QString source=ui->source->text().trimmed();
    if(label.isEmpty() || source.isEmpty())
    {
        MESSAGE_LABEL_EMPTY
    }

    QString account=ui->account->text().trimmed();
    QString passwd=ui->passwd->text().trimmed();
    if(account.isEmpty() || passwd.isEmpty())
    {
        MESSAGE_DETAIL_EMPTY
    }

    if(id != 0)
    {
        if(updateItem(category,label,source,account,passwd))
            accept();
        else
            MESSAGE_CANNOT_SAVE
    }
    else
    {
        if(addItem(category,label,source,account,passwd))
            accept();
        else
            MESSAGE_CANNOT_SUBMIT
    }
}

void P_AddItem::on_types_currentIndexChanged(int index)
{
    QString source=ui->source->text().trimmed();
    if(source.isEmpty())
        return;

    QString host=source;
    if(source.contains("://"))
        host=source.split("://").last();

    switch (index) {
    case 0:
        if(!source.startsWith(PREFIX_HTTP))
            source=PREFIX_HTTP + host;
        break;
    case 1:
        if(!source.startsWith(PREFIX_RDP))
            source=PREFIX_RDP + host;
        break;
    case 2:
        if(!source.startsWith(PREFIX_SSH))
            source=PREFIX_SSH + host;
        break;
    case 3:
        source=host;
        break;
    }

    ui->source->setText(source);
}

void P_AddItem::on_source_editingFinished()
{
    QString source=ui->source->text().trimmed();
    if(source.isEmpty())
        return;

    if(source.startsWith("http"))
        ui->types->setCurrentIndex(0);
    else if (source.startsWith(PREFIX_RDP))
        ui->types->setCurrentIndex(1);
    else if(source.startsWith(PREFIX_SSH))
        ui->types->setCurrentIndex(2);
    else
        ui->types->setCurrentIndex(3);
}
