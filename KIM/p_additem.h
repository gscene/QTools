#ifndef P_ADDITEM_H
#define P_ADDITEM_H

#if _MSC_VER >= 1600
﻿﻿﻿﻿﻿﻿#pragma execution_character_set("utf-8")
#endif

#include <head/g_pch.h>
#include <head/db_helper.h>
#include <head/m_message.h>
#include "m_fhs.h"

namespace Ui {
    class P_AddItem;
}

class P_AddItem : public QDialog
{
    Q_OBJECT

public:
    explicit P_AddItem(QWidget *parent = nullptr);
    ~P_AddItem();

    void updateCategory();
    bool addItem(const QString &category,
                 const QString &label,
                 const QString &source,
                 const QString &account,
                 const QString &passwd);

    bool updateItem(const QString &category,
                    const QString &label,
                    const QString &source,
                    const QString &account,
                    const QString &passwd);

    void setId(int id_);
    void fetchById(int id_);

private slots:
    void on_categories_currentTextChanged(const QString &text);
    void on_btn_submit_clicked();
    void on_types_currentIndexChanged(int index);
    void on_source_editingFinished();

private:
    Ui::P_AddItem *ui;

    int id;
    QString table;
};

#endif // P_ADDITEM_H
