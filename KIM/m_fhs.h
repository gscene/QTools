﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define KIM_PASSWD "kim_passwd"
#define KIM_CONTACT "kim_contact"

#define MANUAL_INPUT QStringLiteral("-手动输入-")

#define T_URL "URL"
#define T_RDP "RDP"
#define T_SSH "SSH"
#define T_NULL "NULL"

#define PREFIX_HTTP "http://"
#define PREFIX_RDP "rdp://"
#define PREFIX_SSH "ssh://"

#define RDP_OPEN "mstsc /v:%1"

#define ID_COL 0
#define CAT_COL 1
#define LAB_COL 2
#define SRC_COL 3   //source
#define ACC_COL 4   //account
#define PWD_COL 5   //passwd

#endif // M_FHS_H
