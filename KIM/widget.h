﻿#ifndef WIDGET_H
#define WIDGET_H

#include "common/baseeditor.h"
#include "p_additem.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void generateMenu();
    void updateCategory();
    void updateListView();
    void updateView();
    void newItem();
    void copyNewItem();
    void editItem();
    void removeItem();
    void openSource();
    void copyAccount();
    void copyPasswd();

    void saveState();
    void loadState();   //加载状态

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_listView_doubleClicked(const QModelIndex &index);
 //   void on_tableView_doubleClicked(const QModelIndex &index);
    void on_kw_editingFinished();
    void on_categories_currentTextChanged(const QString &category);

private:
    Ui::Widget *ui;

    QString selectCategory;
    QString selectLabel;
    QStringListModel *listModel;
};
#endif // WIDGET_H
