﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "KIM"
#define SP_VER 1008
#define SP_UID "{56099799-2794-4195-881c-73de9edb0e1c}"
#define SP_TYPE "prop"

#define SP_CFG "KIM.ini"
#define SP_INFO "KIM.json"
#define SP_LINK "KIM.lnk"

/*
 * 1002
 * 1003 编辑与保存
 * 1004 openSource
 * 1005 source type
 * 1006 copyAccount,copyPasswd,取消双击
*/

#endif // G_VER_H
