#-------------------------------------------------
#
# Project created by QtCreator 2016-08-13T18:52:06
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = property_editor
TEMPLATE = app

INCLUDEPATH += ../../elfproj/support

SOURCES += main.cpp\
        dialog.cpp \
    ../../elfproj/support/piev.cpp \
    ../../elfproj/support/pie.cpp

HEADERS  += dialog.h \
    ../../elfproj/support/piev.h \
    ../../elfproj/support/pie.h

FORMS    += dialog.ui
