﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QInputDialog>
#include <QStringListModel>
#include <QModelIndex>

#include <QMessageBox>
#include <QStandardPaths>

#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

#include "piev.h"

#define LABEL_T "label"
#define DETAIL_T "properties"
#define T_UID "{e12cdb0b-d776-411f-af7e-8e30ac59ecbe}"

#define CFG_FILE "../etc/remote.json"
#include <QDebug>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    bool load(const QString &file=CFG_FILE);
    void replyFinished(QNetworkReply *);

private slots:
    void on_btn_load_clicked();

    void on_btn_add_clicked();

    void on_btn_del_clicked();

    void on_btn_save_clicked();

    void on_btn_online_clicked();

    void un_reach();

    void on_categoryList_doubleClicked(const QModelIndex &index);

    void on_btn_view_clicked();

private:
    Ui::Dialog *ui;

    bool offline;
    QString desktopPath;
    QStringListModel *model;
    QStringListModel *categoryModel;
    QString fileName;

    QString base_url;
    QString current_item;
    QString current_category;
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
    QNetworkRequest request;
};

#endif // DIALOG_H
