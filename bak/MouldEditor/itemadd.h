﻿#ifndef ITEMADD_H
#define ITEMADD_H

#include "m_fhs.h"

namespace Ui {
class ItemAdd;
}

class ItemAdd : public QDialog
{
    Q_OBJECT

public:
    explicit ItemAdd(QWidget *parent = 0);
    ~ItemAdd();

    bool addItem(const QString &type,
                 const QString &entry,
                 const QString &title,
                 const QString &category,
                 const QString &label,
                 const QString &detail,
                 const QString &addition,
                 const QString &table="");

private slots:
    void on_btn_submit_clicked();
    void on_type_currentTextChanged(const QString &text);
    void on_externalTable_stateChanged(int state);

private:
    Ui::ItemAdd *ui;
};

#endif // ITEMADD_H
