﻿#ifndef DIALOG_H
#define DIALOG_H

#include "common/baseeditor.h"
#include "itemadd.h"

namespace Ui {
class Dialog;
}

class Dialog : public BaseEditor
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void updateView();
    void generateMenu();
    void newItem();
    void removeItem();
    void invokeItem();
    void do_invoke(const QString &type,
                   const QStringList &params);

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
