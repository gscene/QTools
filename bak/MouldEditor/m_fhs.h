﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define TD_META "meta_mould"
#define TD_META_CONFIG "meta_mould_config"

#define T_CONE "cone"
#define T_CUBE "cube"
#define T_ITEM "item"
#define T_BIGCONE "bigcone"
#define T_BIGCUBE "bigcube"
#define T_BIGITEM "bigitem"

#ifdef Q_OS_WIN32
#define P_CONE "Cone.exe"
#define P_CUBE "Cube.exe"
#define P_ITEM "Item.exe"
#define P_BIGCONE "BigCone.exe"
#define P_BIGCUBE "BigCube.exe"
#define P_BIGITEM "BigItem.exe"
#else
#define P_CONE "Cone"
#define P_CUBE "Cube"
#define P_ITEM "Item"
#define P_BIGCONE "BigCone"
#define P_BIGCUBE "BigCube"
#define P_BIGITEM "BigItem"
#endif

#endif // M_FHS_H
