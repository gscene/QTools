﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "MouldEditor"
#define SP_VER 1008
#define SP_UID "{c54a9bb2-59ba-4b73-8090-d947db1c2c78}"
#define SP_TYPE "shell"

#define SP_NAME_CN QStringLiteral("模板编辑器")
#define SP_TITLE QString::number(SP_VER) + " - " + SP_NAME_CN

#define SP_CFG "MouldEditor.ini"
#define SP_INFO "MouldEditor.json"
#define SP_LINK "MouldEditor.lnk"

/*
 * 1001 start
 * 1002 项目重建
 * 1004 除了cone、cube，其他detail都不能为空
 * 1005 SP_TITLE
 * 1006 item/bigitem
 * 1007 外部表
 */
#endif // G_VER_H
