﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    BaseEditor(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    table=TD_META;
    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);
    ui->tableView->setEditTriggers(QTableView::DoubleClicked);

    updateView();
    createMenu();

    setWindowTitle(SP_TITLE);
    loadLocation();
}

Dialog::~Dialog()
{
    saveLocation();
    delete ui;
}

void Dialog::generateMenu()
{
    menu->addAction(QStringLiteral("添加..."),this,&Dialog::newItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("启动"),this,&Dialog::invokeItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新"),this,&Dialog::updateView);
    menu->addAction(QStringLiteral("撤销"),this,&Dialog::revert);
    menu->addAction(QStringLiteral("保存"),this,&Dialog::save);
    menu->addSeparator();
    menu->addAction(QStringLiteral("删除"),this,&Dialog::removeItem);
}

void Dialog::updateView()
{
    model->setTable(table);
    model->select();

    ui->tableView->hideColumn(0);       //id
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("应用类型"));    //type
    model->setHeaderData(2,Qt::Horizontal,QStringLiteral("启动参数"));    //entry
    model->setHeaderData(3,Qt::Horizontal,QStringLiteral("应用标题"));    //title
    model->setHeaderData(4,Qt::Horizontal,QStringLiteral("类别列名称"));    //category
    model->setHeaderData(5,Qt::Horizontal,QStringLiteral("标签列名称"));    //label
    model->setHeaderData(6,Qt::Horizontal,QStringLiteral("内容列名称"));    //detail
    model->setHeaderData(7,Qt::Horizontal,QStringLiteral("附加列名称"));    //addition
    model->setHeaderData(8,Qt::Horizontal,QStringLiteral("外部表"));    //table
}

void Dialog::newItem()
{
    ItemAdd itemAdd;
    if(itemAdd.exec() == QDialog::Accepted)
        updateView();
}

void Dialog::removeItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        int row=index.row();
        model->removeRow(row);
    }
}

void Dialog::invokeItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QString type=sp_fetchString(index,1);
        QString entry=sp_fetchString(index,2);
        if(type.isEmpty() || entry.isEmpty())
            return;

        QStringList params;
        params.append(entry);
        QSqlQuery query;
        query.exec(QString("select config from %1 where entry='%2'")
                   .arg(TD_META_CONFIG)
                   .arg(entry));
        if(query.next())
            params.append(query.value("config").toString());

        do_invoke(type,params);
    }
}

void Dialog::do_invoke(const QString &type, const QStringList &params)
{
    QString app;
    if(type == T_CONE)
        app=P_CONE;
    else if(type == T_CUBE)
        app=P_CUBE;
    else if(type == T_ITEM)
        app=P_ITEM;
    else if(type == T_BIGCONE)
        app=P_BIGCONE;
    else if(type == T_BIGCUBE)
        app=P_BIGCUBE;
    else if(type == T_BIGITEM)
        app=P_BIGITEM;
    else
        return;

    if(!QFile::exists(app))
        return;

    QProcess::startDetached(app,params);
}
