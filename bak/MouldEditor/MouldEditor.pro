#-------------------------------------------------
#
# Project created by QtCreator 2017-01-25T20:45:42
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = MouldEditor
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        dialog.cpp \
    itemadd.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += dialog.h \
    g_ver.h \
    ../../elfproj/common/baseeditor.h \
    itemadd.h \
    ../common/m_fhs.h \
    ../../elfproj/support/sp_env.h \
    m_fhs.h

FORMS    += dialog.ui \
    itemadd.ui

RC_ICONS = board.ico

RESOURCES += \
    res.qrc
