﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QMessageBox>
#include <QString>
#include <QSettings>
#include <QFile>
#include <QDir>
#include <QMap>

#include <QDate>
#include <QStandardPaths>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include <QUuid>
#include <QByteArray>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <QMultimedia>
#include <QAudioRecorder>
#include <QAudioEncoderSettings>

#include <AVPlayer.h>

#include "m_fhs.h"
#include "support/sp_env.h"

#include <QDebug>

using namespace QtAV;

namespace Ui {
class Dialog;
}

enum R_Request {
    R_Get,
    R_Post
};

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void replyFinished(QNetworkReply *reply);
    bool loadSession();
    bool saveSession();

    void initAudioRecorder();
    void setCurrentOutputFile();
    void detectState();
    void stateChanged(QMediaRecorder::State state);

    void getToken(const QString &token_file=TOKEN_FILE);

    void httpGet(const QString &url);
    void httpPost(const QString &url,const QByteArray &data);

    void ping();
    void play(const QString &path);

protected slots:
    void un_reach();

private slots:
    void on_btn_submit_clicked();

    void on_btn_submit2_clicked();

    void on_btn_record_clicked();

private:
    Ui::Dialog *ui;

    QString access_token;
    QString mac;
    QNetworkAccessManager *manager;
    QNetworkReply *pingReply;
    QMap<QNetworkReply *, R_Request> replyMap;

    AVPlayer *player;

    QString today;
    QString local;
    QString sessionFile;

    QString tmp;
    QString recordFile;
};

#endif // DIALOG_H
