#-------------------------------------------------
#
# Project created by QtCreator 2017-02-28T20:33:59
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = Whisper
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        dialog.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += dialog.h \
    g_ver.h \
    m_fhs.h \
    ../../elfproj/support/sp_env.h

FORMS    += dialog.ui
