﻿#ifndef M_FHS_H
#define M_FHS_H

#define TOKEN_FILE "../etc/token.ini"
#define TOKEN_URL "http://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id=%1&client_secret=%2&"

#define TTS_BASE_URL "http://tsn.baidu.com/text2audio"
#define TTS_URL "http://tsn.baidu.com/text2audio?tex=%1&lan=zh&cuid=%2&ctp=1&tok=%3"
#define TTS2_URL "http://tsn.baidu.com/text2audio?tex=%1&lan=zh&cuid=%2&ctp=1&tok=%3&per=4"

//per 	选填 	发音人选择, 0为女声，1为男声，3为情感合成-度逍遥，4为情感合成-度丫丫，默认为普通女声

#define VOP_URL "http://vop.baidu.com/server_api"

//Access Token 有效期为一个月
#define SESSION_FILE "/session.dat"

#endif // M_FHS_H
