﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "BigCone"
#define SP_VER 1007
#define SP_UID "{8c4263da-abec-49c4-b40f-e576aa0fa14e}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "BigCone.ini"
#define SP_INFO "BigCone.json"
#define SP_LINK "BigCone.lnk"

/*
 * 1001 项目开始
 * 1002 updateItem
 * 1004 close & return
 * 1006 addition,Qt::Dialog
 * 1007 增加了可以指定配置文件的功能
*/

#endif // G_VER_H
