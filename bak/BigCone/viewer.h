﻿#ifndef VIEWER_H
#define VIEWER_H

#include "common/baseeditor.h"
#include "itemadd.h"
#include "itemedit.h"

#define TD_META "meta_mould"
#define TD_CONE "bigcone_data"
#define T_CONE "bigcone"

namespace Ui {
class Viewer;
}

class Viewer : public BaseEditor
{
    Q_OBJECT

public:
    explicit Viewer(QWidget *parent, const QString &entry);
    ~Viewer();

    bool initialize();
    void updateView();
    void generateMenu();

    void newItem();
    void editItem();
    void showSearchBox(const QString &condition);
    void removeItem();
    void clear();

private slots:
    void on_tableView_clicked(const QModelIndex &index);

private:
    Ui::Viewer *ui;

    QString meta_table;
    QString _entry;

    QString title;
    QString label_t,detail_t,addition_t;
};

#endif // VIEWER_H
