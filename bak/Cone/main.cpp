﻿#include "viewer.h"
#include <QtSingleApplication>
#include "boot/e_boot.h"
#include "support/mysql_helper.h"

int main(int argc, char *argv[])
{
    SP_Single_Boot

            if(argc < 2)
    {
        QMessageBox::critical(0,QStringLiteral("错误"),QStringLiteral("缺少启动参数，程序无法启动！"));
        return EXIT_FAILURE;
    }

    if(!createConnection(DB_CONFIG))
    {
        QMessageBox::critical(0,QStringLiteral("致命错误"),QStringLiteral("无法连接到数据库！"));
        return EXIT_FAILURE;
    }

    QString entry= QString::fromLocal8Bit(argv[1]);

    Viewer w(0,entry);
    a.setActivationWindow(&w);

    if(w.initialize())
        w.show();
    else
    {
        QMessageBox::critical(0,QStringLiteral("格式错误"),QStringLiteral("无法读取数据！"));
        return EXIT_FAILURE;
    }

    return a.exec();
}
