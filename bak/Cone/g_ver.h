﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Cone"
#define SP_VER 1007
#define SP_UID "{3e23e6e0-b0fa-44ad-97ff-3d13ca46968f}"
#define SP_TYPE "extra"

#define SP_CFG "Cone.ini"
#define SP_INFO "Cone.json"
#define SP_LINK "Cone.lnk"

/*
 * 1001 项目开始
 * 1002 项目重建
 * 1004 统一meta表
 * 1007 全局快捷键，_table与entry判定
*/

#endif // G_VER_H
