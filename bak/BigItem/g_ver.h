﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "BigItem"
#define SP_VER 1005
#define SP_UID "{79a88e83-e258-4425-9dad-a456fcf07840}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "BigItem.ini"
#define SP_INFO "BigItem.json"
#define SP_LINK "BigItem.lnk"

/*
 * 1001 项目开始
 * 1002 功能完善
 * 1003 entry bug
 * 1004 no entry
 * 1005 增加了可以指定配置文件的功能
*/

#endif // G_VER_H
