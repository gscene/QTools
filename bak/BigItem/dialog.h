﻿#ifndef DIALOG_H
#define DIALOG_H

#include "itembaseeditor.h"
#include "itemadd.h"
#include "itemedit.h"

#define TD_META "meta_mould"
#define TD_ITEM "bigitem_data"
#define T_ITEM "bigitem"

namespace Ui {
class Dialog;
}

class Dialog : public ItemBaseEditor
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent, const QString &entry);
    ~Dialog();

    bool initialize();
    void updateView();
    void generateMenu();
    void newItem();
    void editItem();
    void removeItem();
    void collapseAll();
    void clear();

private slots:
    void on_treeView_clicked(const QModelIndex &index);

private:
    Ui::Dialog *ui;

    QString meta_table;
    QString _entry;

    QString title;
    QString category_t;
    QString label_t,detail_t,addition_t;
};

#endif // DIALOG_H
