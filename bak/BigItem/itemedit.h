﻿#ifndef ITEMEDIT_H
#define ITEMEDIT_H

#include <QDialog>
#include <QMessageBox>
#include <QString>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

namespace Ui {
class ItemEdit;
}

class ItemEdit : public QDialog
{
    Q_OBJECT

public:
    explicit ItemEdit(QWidget *parent,int id,const QString &table);
    ~ItemEdit();

    void initView();
    bool updateItem(const QString &category, const QString &label,
                    const QString &detail, const QString &addition);

private slots:
    void on_btn_save_clicked();

private:
    Ui::ItemEdit *ui;

    int _id;
    QString _table;
    QString _category,_label,_detail,_addition;
};

#endif // ITEMEDIT_H
