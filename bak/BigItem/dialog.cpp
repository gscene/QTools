﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent, const QString &entry) :
    ItemBaseEditor(parent),_entry(entry),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    // setWindowFlags(Qt::Dialog);
    meta_table=TD_META;
    itemModel=new QStandardItemModel(this);
    ui->treeView->setModel(itemModel);
    ui->treeView->setEditTriggers(QTreeView::NoEditTriggers);

    loadLocation();
}

Dialog::~Dialog()
{
    saveLocation();
    delete ui;
}

bool Dialog::initialize()
{
    QSqlQuery query;
    query.exec(QString("select title,category_t,label_t,detail_t,addition_t,_table from %1 where entry = '%2' AND type='%3'")
               .arg(meta_table)
               .arg(_entry)
               .arg(T_ITEM));

    if(query.next())
    {
        category_t=query.value("category_t").toString();
        label_t=query.value("label_t").toString();
        detail_t=query.value("detail_t").toString();
        if(category_t.isEmpty() || label_t.isEmpty() || detail_t.isEmpty())
            return false;

        title=query.value("title").toString();
        if(title.isEmpty())
            title=_entry;

        table=query.value("_table").toString();
        if(table.isEmpty())
            table=TD_ITEM;
        else
            _entry.clear();

        if(addition_t.isEmpty())
            ui->addition->setVisible(false);

        this->setWindowTitle(QString::number(SP_VER) + " - " + title);

        updateView();
        createMenu();

        return true;
    }
    else
        return false;
}

void Dialog::updateView()
{
    if(addition_t.isEmpty())
        updateView_One(category_t,_entry);
    else
        updateView_Two(category_t,addition_t,_entry);

    clear();
}

void Dialog::generateMenu()
{
    menu->addAction(QStringLiteral("添加..."),this,&Dialog::newItem);
    menu->addAction(QStringLiteral("编辑..."),this,&Dialog::editItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新"),this,&Dialog::updateView);
    menu->addAction(QStringLiteral("全部折叠"),this,&Dialog::collapseAll);
}

void Dialog::collapseAll()
{
    ui->treeView->collapseAll();
}

void Dialog::newItem()
{
    ItemAdd itemAdd(this,table,_entry);
    itemAdd.setWindowTitle(title + QStringLiteral(" - 新条目"));

    QModelIndex index=ui->treeView->currentIndex();
    if(getCategoryByIndex(index))
        itemAdd.setCategory(category);
    else
        itemAdd.setCategoryPlaceholderText(category_t);

    itemAdd.setLabelPlaceholderText(label_t);
    itemAdd.setDetailPlaceholderText(detail_t);
    if(addition_t.isEmpty())
        itemAdd.setAdditionDisable();
    else
        itemAdd.setAdditionPlaceholderText(addition_t);

    if(itemAdd.exec() == QDialog::Accepted)
        updateView();
}

void Dialog::editItem()
{
    QModelIndex index=ui->treeView->currentIndex();
    if(!index.isValid())
        return;

    QSqlQuery query;
    QString sql=QString("select id from %1 where category='%2' and label='%3'")
            .arg(table)
            .arg(category)
            .arg(label);

    query.exec(sql);
    if(query.next())
    {
        int id=query.value("id").toInt();
        ItemEdit itemEdit(this,id,table);
        if(itemEdit.exec() == QDialog::Accepted)
            updateView();
    }
}

void Dialog::removeItem()
{
    QModelIndex index=ui->treeView->currentIndex();
    if(!getItemByIndex(index))
        return;

    QMessageBox::StandardButton btn=QMessageBox::warning(this,QStringLiteral("操作确认"),QStringLiteral("确定删除？无法撤销！"),
                                                         QMessageBox::Cancel | QMessageBox::Ok,
                                                         QMessageBox::Cancel );
    if(btn == QMessageBox::Ok)
    {
        QSqlQuery query;
        QString sql=QString("delete from %1 where category='%2' and label='%3'")
                .arg(table)
                .arg(category)
                .arg(label);
        if(query.exec(sql))
            updateView();
        else
            QMessageBox::warning(this,QStringLiteral("异常情况"),QStringLiteral("异常情况，删除失败！"));
    }

}

void Dialog::on_treeView_clicked(const QModelIndex &index)
{
    if(!getItemByIndex(index))
        return;

    if(!getDetail())
        return;

    ui->label->setText(label);
    ui->detail->setPlainText(detail);
    if(ui->addition->isVisible())
        ui->addition->setText(addition);
}

void Dialog::clear()
{
    ui->label->clear();
    ui->detail->clear();
    if(ui->addition->isVisible())
        ui->addition->clear();
}
