#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <head/g_functionbase.h>
#include <QMainWindow>
#include <QStatusBar>
#include <QLabel>
#include <QMessageBox>
#include <QFileDialog>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QSize>
#include <QUrl>
#include <QDir>
#include <QTimer>

#include "convert_mat.h"

#define DEFAULT_FPS 30
#define MIN_SIZE QSize(640,480)
#define RES_720P QSize(1280,720)
#define RES_1080P QSize(1920,1080)
#define CV_RES_720P Size(1280,720)
#define CV_RES_1080P Size(1920,1080)

using namespace cv;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void showVideoInfo();
    void nextFrame();

private slots:
    void on_actionOpen_triggered();

private:
    Ui::MainWindow *ui;

    QLabel *display;
    QString videoLocation;
    VideoCapture capture;
    QTimer _timer;
};

#endif // MAINWINDOW_H
