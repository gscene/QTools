#-------------------------------------------------
#
# Project created by QtCreator 2019-01-07T17:43:44
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VideoViewer
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += ../../elfproj
INCLUDEPATH += C:/App/msys64/mingw64/include/opencv4

LIBS += -lopencv_highgui -lopencv_videoio \
-lopencv_imgcodecs -lopencv_imgproc \
-lopencv_core

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h \
    convert_mat.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
