﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent, const QString &entry) :
    ItemBaseEditorDialog(parent),_entry(entry),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    meta_table=TD_META;
    itemModel=new QStandardItemModel(this);
    ui->treeView->setModel(itemModel);
    ui->treeView->setEditTriggers(QTreeView::NoEditTriggers);

    player=nullptr;
    loadLocation();
}

Dialog::~Dialog()
{
    saveLocation();
    delete ui;
}

bool Dialog::initialize()
{
    QSqlQuery query;
    query.exec(QString("select title,category_t,label_t,detail_t,addition_t,_table from %1 where entry = '%2' AND type='%3'")
               .arg(meta_table)
               .arg(_entry)
               .arg(T_ITEM));

    if(query.next())
    {
        title=query.value("title").toString();
        category_t=query.value("category_t").toString();
        label_t=query.value("label_t").toString();
        detail_t=query.value("detail_t").toString();
        addition_t=query.value("addition_t").toString();
        table=query.value("_table").toString();

        if(title.isEmpty())
            title=_entry;

        if(table.isEmpty())
            table=TD_ITEM;

        if(category_t.isEmpty() || label_t.isEmpty() || detail_t.isEmpty())
            return false;

        if(!addition_t.isEmpty())
            this->resize(420,540);

        this->setWindowTitle(QString::number(SP_VER) + " - " + title);

        updateView();
        createMenu();

        return true;
    }
    else
        return false;
}

void Dialog::updateView()
{
    if(addition_t.isEmpty())
        updateView_One(_entry,category_t);
    else
        updateView_Two(_entry,category_t,addition_t);

    ui->treeView->setColumnWidth(0,200);
}

void Dialog::generateMenu()
{
    menu->addAction(QStringLiteral("添加..."),this,&Dialog::newItem);
    menu->addAction(QStringLiteral("编辑..."),this,&Dialog::editItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新"),this,&Dialog::updateView);
    menu->addAction(QStringLiteral("全部折叠"),this,&Dialog::collapseAll);
}

void Dialog::collapseAll()
{
    ui->treeView->collapseAll();
}

void Dialog::newItem()
{
    ItemAdd itemAdd(this,table,_entry);
    itemAdd.setWindowTitle(title + QStringLiteral(" - 新条目"));

    QModelIndex index=ui->treeView->currentIndex();
    if(getCategoryByIndex(index))
        itemAdd.setCategory(category);
    else
        itemAdd.setCategoryPlaceholderText(category_t);

    itemAdd.setLabelPlaceholderText(label_t);
    itemAdd.setDetailPlaceholderText(detail_t);
    if(addition_t.isEmpty())
        itemAdd.setAdditionDisable();
    else
        itemAdd.setAdditionPlaceholderText(addition_t);

    itemAdd.move(this->x() + this->width() + 20,this->y());
    if(itemAdd.exec() == QDialog::Accepted)
        updateView();
}

void Dialog::editItem()
{
    ItemEdit itemEdit(this,table,_entry);
    itemEdit.setWindowTitle(title + QStringLiteral(" - 编辑器"));

    if(addition_t.isEmpty())
        itemEdit.setItemString(category_t,label_t,detail_t);
    else
        itemEdit.setItemString(category_t,label_t,detail_t,addition_t);

    itemEdit.setHeaderData();
    if(itemEdit.exec() == QDialog::Accepted)
        updateView();
}

void Dialog::play(const QString &path)
{
    if(player == nullptr)
        player=new AVPlayer(this);

    player->setFile(path);
    player->play();
}

void Dialog::on_treeView_doubleClicked(const QModelIndex &index)
{
    if(!getItemByIndex(index))
        return;
    if(!getDetail())
        return;
    if(detail.isEmpty())
        return;

    if(detail.startsWith("#SND#"))
    {
        detail=detail.remove("#SND#");
        detail=P_R_SND + detail;
        if(!QFile::exists(detail))
            return;
    }
    play(detail);
}

void Dialog::on_treeView_clicked(const QModelIndex &index)
{
    if(!getItemByIndex(index))
        return;

    if(!getDetail())
        return;

    if(detail.isEmpty())
        return;

    ui->detail->setText(detail);
}
