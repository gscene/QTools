﻿#ifndef DIALOG_H
#define DIALOG_H

#include "itembaseeditordialog.h"
#include <AVPlayer.h>

#include "itemadd.h"
#include "itemedit.h"

#define TD_META "meta_mould"
#define TD_ITEM "item_data"
#define T_ITEM "item"

#define P_R_SND "../share/Music/"

using namespace QtAV;

namespace Ui {
class Dialog;
}

class Dialog : public ItemBaseEditorDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent, const QString &entry);
    ~Dialog();

    bool initialize();
    void updateView();
    void generateMenu();
    void newItem();
    void editItem();
    void collapseAll();

    void play(const QString &path);

private slots:
    void on_treeView_clicked(const QModelIndex &index);
    void on_treeView_doubleClicked(const QModelIndex &index);

private:
    Ui::Dialog *ui;

    QString meta_table;
    QString _entry;

    QString title;
    QString category_t;
    QString label_t,detail_t,addition_t;

    AVPlayer *player;
};

#endif // DIALOG_H
