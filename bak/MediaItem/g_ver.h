﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "MediaItem"
#define SP_VER 1002
#define SP_UID "{4f01501f-970d-48de-b2da-2664dc7120f7}"
#define SP_TYPE "extra"

#define SP_CFG "MediaItem.ini"
#define SP_INFO "MediaItem.json"
#define SP_LINK "MediaItem.lnk"

/*
 * 1001 项目开始
 * 1002 P_FHS_ETC
*/

#endif // G_VER_H
