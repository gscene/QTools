#-------------------------------------------------
#
# Project created by QtCreator 2017-01-23T19:33:16
#
#-------------------------------------------------

QT       += core gui sql network av

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = MediaItem
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

SOURCES += main.cpp\
    ../../elfproj/common/baseeditordialog.cpp \
        dialog.cpp \
    itemadd.cpp \
    itemedit.cpp \
    ../common/itembaseeditordialog.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += dialog.h \
    ../../elfproj/common/baseeditordialog.h \
    g_ver.h \
    itemadd.h \
    itemedit.h \
    ../common/itembaseeditordialog.h \
    ../../elfproj/support/sp_env.h

FORMS    += dialog.ui \
    itemadd.ui \
    itemedit.ui

RC_ICONS = item.ico

RESOURCES += \
    res.qrc
