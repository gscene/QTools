﻿#ifndef DIALOG_H
#define DIALOG_H

#include "itembaseeditor.h"
#include "itemadd.h"
#include "itemedit.h"

#define TD_META "meta_mould"
#define TD_ITEM "item_data"
#define T_ITEM "item"

#define MODE_PASTE 1
#define MODE_OPEN 2

namespace Ui {
class Dialog;
}

class Dialog : public ItemBaseEditor
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent, const QString &entry);
    ~Dialog();

    bool initialize();
    void updateView();
    void generateMenu();
    void newItem();
    void editItem();
    void collapseAll();

    void showSearchView(const QString &keyword,const QString &column);

    void triggerPasteMode();
    void triggerOpenMode();

private slots:
    void on_treeView_clicked(const QModelIndex &index);
    void on_treeView_doubleClicked(const QModelIndex &index);

private:
    Ui::Dialog *ui;

    int mode;
    QString meta_table;
    QString _entry;
    QString type;

    QString title;
    QString category_t;
    QString label_t,detail_t,addition_t;
};

#endif // DIALOG_H
