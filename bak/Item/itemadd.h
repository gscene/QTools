﻿#ifndef ITEMADD_H
#define ITEMADD_H

#include "head/g_pch.h"

namespace Ui {
class ItemAdd;
}

class ItemAdd : public QDialog
{
    Q_OBJECT

public:
    explicit ItemAdd(QWidget *parent,
                     const QString &table,
                     const QString &entry);
    ~ItemAdd();

    bool addItem(const QString &category,
                 const QString &label,
                 const QString &detail,
                 const QString &addition);

    void setCategory(const QString &category);
    void setCategoryPlaceholderText(const QString &category);
    void setLabelPlaceholderText(const QString &label);
    void setDetailPlaceholderText(const QString &detail);
    void setAdditionPlaceholderText(const QString &addition);
    void setAdditionDisable();

private slots:
    void on_btn_submit_clicked();

private:
    Ui::ItemAdd *ui;

    QString _table;
    QString _entry;
};

#endif // ITEMADD_H
