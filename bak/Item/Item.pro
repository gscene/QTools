#-------------------------------------------------
#
# Project created by QtCreator 2017-01-23T19:33:16
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = Item
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

SOURCES += main.cpp\
        dialog.cpp \
    itemadd.cpp \
    itemedit.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += dialog.h \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/common/baseeditor_dialog.h \
    g_ver.h \
    itemadd.h \
    itemedit.h \
    ../common/itembaseeditor.h \
    ../../elfproj/support/sp_env.h

FORMS    += dialog.ui \
    itemadd.ui \
    itemedit.ui

RC_ICONS = item.ico

RESOURCES += \
    res.qrc
