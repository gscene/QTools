﻿#ifndef ITEMEDIT_H
#define ITEMEDIT_H

#include "common/baseeditor_dialog.h"

namespace Ui {
class ItemEdit;
}

class ItemEdit : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit ItemEdit(QWidget *parent ,
                      const QString &table,
                      const QString &entry);
    ~ItemEdit();

    void save();
    void setTitle(const QString &title);
    void setItemString(const QString &category,
                       const QString &label,
                       const QString &detail,
                       const QString &additon=QString());

    void updateView();
    void generateMenu();
    void removeItem();
    void setHeaderData();

private:
    Ui::ItemEdit *ui;

    bool isChanged;

    QString _table;
    QString _entry;
    QString _category,_label,_detail,_addition;
};

#endif // ITEMEDIT_H
