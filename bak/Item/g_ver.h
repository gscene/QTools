﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Item"
#define SP_VER 1009
#define SP_UID "{f6fb7102-328e-486b-8db6-2bd969da24a4}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "Item.ini"
#define SP_INFO "Item.json"
#define SP_LINK "Item.lnk"

/*
 * 1001 项目开始
 * 1002 功能完善
 * 1003 项目重建
 * 1004 统一meta表
 * 1005 mode
 * 1006 默认paste模式
 * 1007 updateView_Two(category_t,addition_t,_entry);
 * 1008 在使用外部表时，不再使用entry
 * 1009 增加了可以指定配置文件的功能
*/

#endif // G_VER_H
