﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent, const QString &entry) :
    ItemBaseEditor(parent),_entry(entry),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog);

    mode=MODE_PASTE;
    meta_table=TD_META;
    type=T_ITEM;

    itemModel=new QStandardItemModel(this);
    ui->treeView->setModel(itemModel);
    ui->treeView->setEditTriggers(QTreeView::NoEditTriggers);

    loadLocation();
}

Dialog::~Dialog()
{
    saveLocation();
    delete ui;
}

bool Dialog::initialize()
{
    QSqlQuery query;
    query.exec(QString("select title,category_t,label_t,detail_t,addition_t,_table from %1 where entry = '%2' AND type='%3'")
               .arg(meta_table)
               .arg(_entry)
               .arg(type));

    if(query.next())
    {
        category_t=query.value("category_t").toString();
        label_t=query.value("label_t").toString();
        detail_t=query.value("detail_t").toString();
        if(category_t.isEmpty() || label_t.isEmpty() || detail_t.isEmpty())
            return false;

        title=query.value("title").toString();
        if(title.isEmpty())
            title=_entry;

        table=query.value("_table").toString();
        if(table.isEmpty())
            table=TD_ITEM;
        else
            _entry.clear();

        addition_t=query.value("addition_t").toString();
        if(!addition_t.isEmpty())
            this->resize(420,540);

        this->setWindowTitle(QString::number(SP_VER) + " - " + title);

        updateView();
        createMenu();

        return true;
    }
    else
        return false;
}

void Dialog::updateView()
{
    if(addition_t.isEmpty())
        updateView_One(category_t,_entry);
    else
        updateView_Two(category_t,addition_t,_entry);

    ui->treeView->setColumnWidth(0,200);
}

void Dialog::generateMenu()
{
    menu->addAction(QStringLiteral("添加..."),this,&Dialog::newItem);
    menu->addAction(QStringLiteral("编辑..."),this,&Dialog::editItem);
    menu->addSeparator();
    QMenu *m_mode=menu->addMenu(QStringLiteral("模式"));
    m_mode->addAction(QStringLiteral("复制模式"),this,&Dialog::triggerPasteMode);
    m_mode->addAction(QStringLiteral("打开模式"),this,&Dialog::triggerOpenMode);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新"),this,&Dialog::updateView);
    menu->addAction(QStringLiteral("全部折叠"),this,&Dialog::collapseAll);
}

void Dialog::triggerPasteMode()
{
    mode=MODE_PASTE;
}

void Dialog::triggerOpenMode()
{
    mode=MODE_OPEN;
}

void Dialog::collapseAll()
{
    ui->treeView->collapseAll();
}

void Dialog::newItem()
{
    ItemAdd itemAdd(this,table,_entry);
    itemAdd.setWindowTitle(title + QStringLiteral(" - 新条目"));

    QModelIndex index=ui->treeView->currentIndex();
    if(getCategoryByIndex(index))
        itemAdd.setCategory(category);
    else
        itemAdd.setCategoryPlaceholderText(category_t);

    itemAdd.setLabelPlaceholderText(label_t);
    itemAdd.setDetailPlaceholderText(detail_t);
    if(addition_t.isEmpty())
        itemAdd.setAdditionDisable();
    else
        itemAdd.setAdditionPlaceholderText(addition_t);

    itemAdd.move(this->x() + this->width() + 20,this->y());
    if(itemAdd.exec() == QDialog::Accepted)
        updateView();
}

void Dialog::editItem()
{
    ItemEdit itemEdit(this,table,_entry);
    itemEdit.setWindowTitle(title + QStringLiteral(" - 编辑器"));

    if(addition_t.isEmpty())
        itemEdit.setItemString(category_t,label_t,detail_t);
    else
        itemEdit.setItemString(category_t,label_t,detail_t,addition_t);

    itemEdit.setHeaderData();
    QDialog *item=qobject_cast<QDialog*>(&itemEdit);
    if(item != 0)
    {
        if(item->exec() == QDialog::Accepted)
            updateView();
    }
}

void Dialog::on_treeView_doubleClicked(const QModelIndex &index)
{
    switch (mode) {
    case MODE_PASTE:
        if(!getItemByIndex(index))
            return;
        if(!getDetail())
            return;
        if(detail.isEmpty())
            return;
        qApp->clipboard()->setText(detail);
        break;
    case MODE_OPEN:
        if(!getItemByIndex(index))
            return;
        if(!getDetail())
            return;
        if(detail.isEmpty())
            return;
        QDesktopServices::openUrl(QUrl(detail));
        break;
    }
}

void Dialog::on_treeView_clicked(const QModelIndex &index)
{
    if(!getItemByIndex(index))
        return;
    if(!getDetail())
        return;
    if(detail.isEmpty())
        return;
    ui->detail->setText(detail);
}

void Dialog::showSearchView(const QString &keyword,const QString &column)
{
    QSqlQuery query;
    QString sql;
    if(_entry.isEmpty())
        sql=QString("select category,label from %1 where %2 like '%%3%'").arg(table)
                .arg(column).arg(keyword);
    else
        sql=QString("select category,label from %1 where entry='%2' AND %3 like '%%4%'")
                .arg(table).arg(_entry)
                .arg(column).arg(keyword);

    query.exec(sql);
    while(query.next())
    {
        QString category=query.value("category").toString();
        QString label=query.value("label").toString();
        QList<QStandardItem *> items;
        items.append(new QStandardItem(category));
        items.append(new QStandardItem(label));
        itemModel->appendRow(items);
    }
}
