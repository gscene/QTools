﻿#ifndef VIEWER_H
#define VIEWER_H

#include "common/baseeditor.h"
#include "itemadd.h"

#define TD_META "meta_mould"
#define TD_CUBE "cube_data"
#define T_CUBE "cube"

namespace Ui {
class Viewer;
}

class Viewer : public BaseEditor
{
    Q_OBJECT

public:
    explicit Viewer(QWidget *parent, const QString &entry);
    ~Viewer();

    bool initialize();
    void updateView();

    void newItem();
    void removeItem();
    void showSearchBox(const QString &column="label");

    void edit_on();
    void edit_off();

private:
    Ui::Viewer *ui;

    QString meta_table;
    QString type;
    QString _entry;

    QString title;
    QString label_t,detail_t,addition_t;
};

#endif // VIEWER_H
