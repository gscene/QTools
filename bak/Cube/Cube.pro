#-------------------------------------------------
#
# Project created by QtCreator 2016-09-28T21:47:31
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = Cube
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

SOURCES += main.cpp\
        viewer.cpp \
    itemadd.cpp \
    ../../elfproj/support/sp_env.cpp


HEADERS  += viewer.h \
    g_ver.h \
    ../../elfproj/common/baseeditor.h \
    itemadd.h \
    ../../elfproj/support/sp_env.h \
    ../common/m_fhs.h

FORMS    += viewer.ui \
    itemadd.ui

RESOURCES += \
    res.qrc

RC_ICONS = cube.ico
