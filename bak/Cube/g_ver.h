﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Cube"
#define SP_VER 1007
#define SP_UID "{3231ee49-4e37-4255-9932-6d41b9333c15}"
#define SP_TYPE "extra"

#define SP_CFG "Cube.ini"
#define SP_INFO "Cube.json"
#define SP_LINK "Cube.lnk"

/*
 * 1001 项目开始
 * 1002 项目重建
 * 1003 统一meta表
 * 1007 全局快捷键，_table与entry判定
*/

#endif // G_VER_H
