﻿#include "viewer.h"
#include "ui_viewer.h"

Viewer::Viewer(QWidget *parent, const QString &entry) :
    BaseEditor(parent),_entry(entry),
    ui(new Ui::Viewer)
{
    ui->setupUi(this);
    meta_table=TD_META;
    type=T_CUBE;

    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);
    // ui->tableView->setEditTriggers(QTableView::NoEditTriggers);

    loadLocation();
}

Viewer::~Viewer()
{
    saveLocation();
    delete ui;
}

bool Viewer::initialize()
{
    QSqlQuery query;
    query.exec(QString("select title,label_t,detail_t,addition_t,_table from %1 where entry = '%2' AND type='%3'")
               .arg(meta_table)
               .arg(_entry)
               .arg(type));

    if(query.next())
    {
        label_t=query.value("label_t").toString();
        if(label_t.isEmpty())
            return false;

        title=query.value("title").toString();
        if(title.isEmpty())
            title=_entry;

        detail_t=query.value("detail_t").toString();
        addition_t=query.value("addition_t").toString();
        table=query.value("_table").toString();
        if(table.isEmpty())
            table=TD_CUBE;
        else
            _entry.clear();

        this->setWindowTitle(QString::number(SP_VER) + " - " + title);

        updateView();
        createMenu();

        return true;
    }
    else
        return false;
}

void Viewer::updateView()
{
    model->setTable(table);
    if(!_entry.isEmpty())
        model->setFilter(QString("entry = '%1'").arg(_entry));
    model->select();

    ui->tableView->hideColumn(0);   //id
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("日期"));    //label

    model->setHeaderData(2,Qt::Horizontal,label_t);    //label
    // ui->tableView->resizeColumnToContents(2);

    if(!detail_t.isEmpty())
        model->setHeaderData(3,Qt::Horizontal,detail_t);    //detail
    else
        ui->tableView->hideColumn(3);   //detail

    if(!addition_t.isEmpty())
        model->setHeaderData(4,Qt::Horizontal,addition_t);    //addition
    else
        ui->tableView->hideColumn(4);   //addition

    if(!_entry.isEmpty())
        ui->tableView->hideColumn(5);     //entry
}

void Viewer::newItem()
{
    ItemAdd item(this,table,_entry);
    item.setWindowTitle(title + QStringLiteral(" - 新条目"));

    item.setLabelPlaceholderText(label_t);
    if(!detail_t.isEmpty())
        item.setDetailPlaceholderText(detail_t);
    else
        item.setDetailDisable();

    if(!addition_t.isEmpty())
        item.setAdditionPlaceholderText(addition_t);
    else
        item.setAdditionDisable();

    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Viewer::removeItem()
{
    if(ui->tableView->currentIndex().isValid())
    {
        int curRow = ui->tableView->currentIndex().row();
        model->removeRow(curRow);
    }
}

void Viewer::edit_on()
{
    isEdit=true;
    ui->tableView->setEditTriggers(QTableView::DoubleClicked);
    updateMenu();
}

void Viewer::edit_off()
{
    isEdit=false;
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
    updateMenu();
}

void Viewer::showSearchBox(const QString &condition)
{
    QString input=QInputDialog::getText(this,QStringLiteral("输入关键词"),QStringLiteral("关键词"));
    if(!input.isEmpty())
    {
        if(!_entry.isEmpty())
            model->setFilter(QString("entry='%1' AND %2 like '%%3%'").arg(_entry)
                             .arg(condition).arg(input));
        else
            model->setFilter(QString("%1 like '%%2%'").arg(condition).arg(input));

        model->select();
    }
}
