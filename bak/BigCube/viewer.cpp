﻿#include "viewer.h"
#include "ui_viewer.h"

Viewer::Viewer(QWidget *parent, const QString &entry) :
    BaseEditor(parent),_entry(entry),
    ui(new Ui::Viewer)
{
    ui->setupUi(this);
    // setWindowFlags(Qt::Dialog);
    meta_table=TD_META;
    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);

    loadLocation();
}

Viewer::~Viewer()
{
    saveLocation();
    delete ui;
}

bool Viewer::initialize()
{
    QSqlQuery query;
    query.exec(QString("select title,label_t,detail_t,addition_t,_table from %1 where entry = '%2' AND type='%3'")
               .arg(meta_table)
               .arg(_entry)
               .arg(T_CUBE));

    if(query.next())
    {
        label_t=query.value("label_t").toString();
        detail_t=query.value("detail_t").toString();
        if(label_t.isEmpty() || detail_t.isEmpty())
            return false;

        title=query.value("title").toString();
        if(title.isEmpty())
            title=_entry;

        table=query.value("_table").toString();
        if(table.isEmpty())
            table=TD_CUBE;
        else
            _entry.clear();

        addition_t=query.value("addition_t").toString();
        if(addition_t.isEmpty())
            ui->addition->setVisible(false);

        this->setWindowTitle(QString::number(SP_VER) + " - " + title);

        updateView();
        createMenu();

        return true;
    }
    else
        return false;
}

void Viewer::generateMenu()
{
    menu->addAction(QStringLiteral("添加..."),this,&Viewer::newItem);
    menu->addAction(QStringLiteral("编辑..."),this,&Viewer::editItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("查找..."),this,&Viewer::search);
    menu->addAction(QStringLiteral("更新"),this,&Viewer::updateView);
    menu->addSeparator();
    menu->addAction(QStringLiteral("【删除】"),this,&Viewer::removeItem);
}

void Viewer::updateView()
{
    model->setTable(table);
    if(!_entry.isEmpty())
        model->setFilter(QString("entry = '%1'").arg(_entry));
    model->select();

    ui->tableView->hideColumn(0);   //id
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("日期"));    //date
    model->setHeaderData(2,Qt::Horizontal,label_t);    //label
    ui->tableView->hideColumn(3);   //detail
    ui->tableView->hideColumn(4);   //addition

    if(!_entry.isEmpty())
        ui->tableView->hideColumn(5);   //entry

    clear();
}

void Viewer::newItem()
{
    ItemAdd item(this,table,_entry);
    item.setWindowTitle(title + QStringLiteral(" - 新条目"));
    item.setLabelPlaceholderText(label_t);
    item.setDetailPlaceholderText(detail_t);

    if(!addition_t.isEmpty())
        item.setAdditionPlaceholderText(addition_t);
    else
        item.setAdditionDisable();

    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Viewer::editItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(!index.isValid())
        return;

    QModelIndex id_index=index.sibling(index.row(),0);  //id
    bool ok;
    int id=model->data(id_index).toInt(&ok);
    if(ok)
    {
        ItemEdit itemEdit(this,id,table);
        itemEdit.setWindowTitle(title + QStringLiteral(" - 编辑器"));
        if(itemEdit.exec() == QDialog::Accepted)
            updateView();
    }
}

void Viewer::removeItem()
{
    if(ui->tableView->currentIndex().isValid())
    {
        QMessageBox::StandardButton btn=QMessageBox::warning(this,QStringLiteral("操作确认"),QStringLiteral("确定删除？无法撤销！"),
                                                             QMessageBox::Cancel | QMessageBox::Ok,
                                                             QMessageBox::Cancel );
        if(btn == QMessageBox::Ok)
        {
            int curRow = ui->tableView->currentIndex().row();
            model->removeRow(curRow);
            model->submitAll();
            clear();
        }
    }
}

void Viewer::clear()
{
    ui->label->clear();
    ui->detail->clear();
    if(ui->addition->isVisible())
        ui->addition->clear();
}

void Viewer::showSearchBox(const QString &condition)
{
    QString input=QInputDialog::getText(this,QStringLiteral("输入关键词"),QStringLiteral("关键词"));
    if(!input.isEmpty())
    {
        model->setFilter(QString("entry='%1' AND %2 like '%%3%'")
                         .arg(_entry)
                         .arg(condition)
                         .arg(input));
        model->select();
    }
}

void Viewer::on_tableView_clicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        //   QModelIndex label_index=index.sibling(index.row(),2);   //label
        //   QString label=model->data(label_index).toString();
        QString label=model->data(index).toString();
        ui->label->setText(label);

        QModelIndex detail_index=index.sibling(index.row(),3);  //detail
        QString detail=model->data(detail_index).toString();
        ui->detail->setPlainText(detail);

        if(ui->addition->isVisible())
        {
            QModelIndex addition_index=index.sibling(index.row(),4);   //addition
            QString addition=model->data(addition_index).toString();
            ui->addition->setText(addition);
        }
    }
}
