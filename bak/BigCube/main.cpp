﻿#include "viewer.h"
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QtSingleApplication>
#include "boot/e_boot.h"
#include "support/mysql_helper.h"

int main(int argc, char *argv[])
{
    SP_Single_Boot

    a.setApplicationName(SP_NAME);
    a.setApplicationVersion(SP_VERSION);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("entry","entry name");
    parser.addPositionalArgument("config","config file");
    parser.process(a);

    const QStringList args=parser.positionalArguments();
    if(args.isEmpty())
    {
        QMessageBox::critical(0,QStringLiteral("错误"),QStringLiteral("缺少启动参数，程序无法启动！"));
        return EXIT_FAILURE;
    }
    else if(args.size() == 1)
    {
        if(!createConnection(DB_CONFIG))
        {
            QMessageBox::critical(0,QStringLiteral("致命错误"),QStringLiteral("无法连接到数据库！"));
            return EXIT_FAILURE;
        }
    }
    else if(args.size() == 2)
    {
        if(!createConnection(args.last()))
        {
            QMessageBox::critical(0,QStringLiteral("致命错误"),QStringLiteral("无法连接到数据库！"));
            return EXIT_FAILURE;
        }
    }

    QString entry=args.first();

    Viewer w(0,entry);
    a.setActivationWindow(&w);

    if(w.initialize())
        w.show();
    else
    {
        QMessageBox::critical(0,QStringLiteral("格式错误"),QStringLiteral("无法读取数据！"));
        return EXIT_FAILURE;
    }

    return a.exec();
}
