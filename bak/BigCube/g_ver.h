﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "BigCube"
#define SP_VER 1006
#define SP_UID "{682e7992-57b4-4de0-9b97-0c6c5cdfe713}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")


#define SP_CFG "BigCube.ini"
#define SP_INFO "BigCube.json"
#define SP_LINK "BigCube.lnk"

/*
 * 1001 项目开始
 * 1002 clear
 * 1005 addition
 * 1006 增加了可以指定配置文件的功能
*/

#endif // G_VER_H
