﻿#ifndef BIZEDITOR_H
#define BIZEDITOR_H

#include <common/baseeditor.h>

class BizEditor : public BaseEditor
{
    Q_OBJECT
public:
    explicit BizEditor(QWidget *parent = 0)
        : BaseEditor(parent)
    {
        loadColumnWidthFlag=false;
    }

    void keyPressEvent(QKeyEvent *event)
    {
        if(event->key() == Qt::Key_Escape)
            this->close();
    }

    virtual void updateView(const QString &status=ST_RUNNING)
    {
        model->setTable(table);
        model->setFilter(QString("status = '%1'").arg(status));
        model->select();

        setHeaderData();
    }

    virtual void updateViewCancel()
    {
        updateView(ST_CANCEL);
    }

    virtual void updateViewFinish()
    {
        updateView(ST_FINISH);
    }

    virtual bool loadColumnWidth() = 0;
    virtual void saveColumnWidth() = 0;
    virtual void setHeaderData() = 0;

    virtual void cancelItem(const QModelIndex &index)
    {
        if(model->data(index).toString() != ST_RUNNING)
        {
            QMessageBox::warning(this,QStringLiteral("状态无效"),QStringLiteral("只有【处理中】才可以取消！"));
            return;
        }
        model->setData(index,ST_CANCEL);
    }

    virtual void finishItem(const QModelIndex &index)
    {
        if(model->data(index).toString() != ST_RUNNING)
        {
            QMessageBox::warning(this,QStringLiteral("状态无效"),QStringLiteral("只有【处理中】才可以完成！"));
            return;
        }
        model->setData(index,ST_FINISH);
    }

protected:
    bool loadColumnWidthFlag;
    QMap<int,int> colWidth;
};

#endif // BIZEDITOR_H
