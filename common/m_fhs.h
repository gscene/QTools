#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"
#include "template/m_sql.h"

#define ST_RUNNING QStringLiteral("【处理中】")
#define ST_FINISH QStringLiteral("【已完成】")
#define ST_CANCEL QStringLiteral("【已取消】")

#define TD_PRODUCT "td_product"
#define TD_PRODUCT_IMG "td_product_img"

#define P_DIR_IMG "../var/images/"

#define PRE_IMG "#IMG#"
#define PRE_CDN "#CDN#"

#define M_OFFLINE_KEY "Mode/offline"
#define PL_FAV "/favorite.json"

#endif // M_FHS_H
