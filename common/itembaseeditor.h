﻿#ifndef ITEMBASEEDITOR_H
#define ITEMBASEEDITOR_H

#include "common/baseeditor.h"

class ItemBaseEditor : public BaseEditor
{
public:
    explicit ItemBaseEditor(QWidget *parent = Q_NULLPTR)
        : BaseEditor(parent)
    {
        itemModel = nullptr;
        form = WF_NORMAL;
    }

    void showSearchBox(const QString &column="label")
    {
        QString keyword=QInputDialog::getText(this,QStringLiteral("输入关键词"),QStringLiteral("关键词"));
        if(keyword.isEmpty())
            return;

        form=WF_SEARCH;
        itemModel->clear();
        itemModel->setHorizontalHeaderLabels(QStringList() << QStringLiteral("类别") << keyword);

        showSearchView(keyword,column);
    }

    virtual void showSearchView(const QString &keyword,const QString &column="label")
    {
        QSqlQuery query;
        QString sql=QString("select category,label from %1 where %2 like '%%3%'")
                .arg(table)
                .arg(column)
                .arg(keyword);
        query.exec(sql);

        while(query.next())
        {
            QString category=query.value("category").toString();
            QString label=query.value("label").toString();
            QList<QStandardItem *> items;
            items.append(new QStandardItem(category));
            items.append(new QStandardItem(label));
            itemModel->appendRow(items);
        }
    }

    virtual bool getItemByIndex(const QModelIndex &index)
    {
        if(!index.isValid())
            return false;

        category.clear();
        label.clear();

        if(form == WF_NORMAL)
        {
            QModelIndex p_index=index.parent();
            if(p_index != QModelIndex())
            {
                category=p_index.data().toString();
                label=index.data().toString();
                return true;
            }
            else
                return false;
        }
        else
        {
            category=index.sibling(index.row(),0).data().toString();
            label=index.sibling(index.row(),1).data().toString();
            return true;
        }
    }

    virtual bool getCategoryByIndex(const QModelIndex &index)
    {
        if(!index.isValid())
            return false;

        category.clear();

        if(form == WF_NORMAL)
        {
            QModelIndex p_index=index.parent();
            if(p_index == QModelIndex())
                category=index.data().toString();
            else
                category=p_index.data().toString();
        }
        else
            category=index.sibling(index.row(),0).data().toString();

        return true;
    }

    virtual bool getDetail()
    {
        detail.clear();
        addition.clear();

        QSqlQuery query;
        QString sql=QString("select detail,addition from %1 where category='%2' AND label ='%3'")
                .arg(table)
                .arg(category)
                .arg(label);
        query.exec(sql);

        if(query.next())
        {
            detail = query.value("detail").toString();
            addition=query.value("addition").toString();
            return true;
        }
        else
            return false;
    }

    virtual void updateView_One(const QString &category_t=QStringLiteral("类别"),const QString &entry=QString())
    {
        form=WF_NORMAL;
        itemModel->clear();
        itemModel->setHorizontalHeaderLabels(QStringList() << category_t);

        QSqlQuery query;
        QString sql;
        if(!entry.isEmpty())
            sql=QString("select category from %1 where entry='%2' GROUP BY category").arg(table).arg(entry);
        else
            sql=QString("select category from %1 GROUP BY category").arg(table);

        query.exec(sql);
        while(query.next())
        {
            QString category=query.value("category").toString();
            QStandardItem *p_item=new QStandardItem(category);
            p_item->appendRows(getLabelItems(category));
            itemModel->appendRow(p_item);
        }
    }

    virtual void updateView_Two(const QString &category_t,const QString &addition_t,const QString &entry=QString())
    {
        form=WF_NORMAL;
        itemModel->clear();
        itemModel->setHorizontalHeaderLabels(QStringList() << category_t << addition_t );

        QSqlQuery query;
        QString sql;
        if(!entry.isEmpty())
            sql=QString("select category from %1 where entry='%2' GROUP BY category").arg(table).arg(entry);
        else
            sql=QString("select category from %1 GROUP BY category").arg(table);

        query.exec(sql);
        while(query.next())
        {
            QString category=query.value("category").toString();
            QStandardItem *p_item=new QStandardItem(category);

            QSqlQuery query;
            QString sql=QString("select label,addition from %1 where category = '%2'").arg(table).arg(category);
            query.exec(sql);
            while(query.next())
            {
                QString label=query.value("label").toString();
                QString addition=query.value("addition").toString();
                int row=p_item->rowCount();
                p_item->setChild(row,0,new QStandardItem(label));
                p_item->setChild(row,1,new QStandardItem(addition));
            }
            itemModel->appendRow(p_item);
        }
    }

    virtual QList<QStandardItem *> getLabelItems(const QString &category) const
    {
        QList<QStandardItem *> items;
        QSqlQuery query;
        QString sql=QString("select label from %1 where category = '%2'").arg(table).arg(category);
        query.exec(sql);
        while(query.next())
        {
            QString label=query.value("label").toString();
            items.append(new QStandardItem(label));
        }

        return items;
    }

protected:
    WorkForm form;
    QStandardItemModel *itemModel;
    QString category;
    QString label;
    QString detail;
    QString addition;
};

#endif // ITEMBASEEDITOR_H
