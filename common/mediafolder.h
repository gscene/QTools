#ifndef MEDIAFOLDER_H
#define MEDIAFOLDER_H

#include "head/g_functionbase.h"
#include "support/sp_env.h"
#include "support/mysql_helper.h"
#include "m_fhs.h"

//#define LINKS QStringLiteral("<a href=\"%1\">%2</a>")

class MediaFolder : public QWidget
{
    Q_OBJECT
public:
    enum MediaType
    {
        MT_Picture,
        MT_Music,
        MT_Video
    };

    explicit MediaFolder(QWidget *parent,const MediaType &type):
        QWidget(parent)
    {
        model=new QFileSystemModel(this);
        configFile=Location::document + APP_ROOT + QString("/") + SP_CFG;

        switch (type) {
        case MT_Picture:
            defaultLocation=Location::picture;
            traceTable=TD_PICTURE_TRACE;
            markTable=TD_PICTURE_MARK;
            category_=MT_PICTURE;
            break;
        case MT_Music:
            defaultLocation=Location::music;
            traceTable=TD_MUSIC_TRACE;
            markTable=TD_MUSIC_MARK;
            category_=MT_MUSIC;
            break;
        case MT_Video:
            defaultLocation=Location::video;
            traceTable=TD_VIDEO_TRACE;
            markTable=TD_VIDEO_MARK;
            category_=MT_VIDEO;
            break;
        }

        offline_=false;
        if(QFile::exists(M_CONFIG))
        {
            QSettings cfg(M_CONFIG,QSettings::IniFormat);
            if(cfg.contains(M_OFFLINE_KEY))
                offline_=true;
        }

        if(!offline_)        //在线模式
        {
            if(!createConnection(CFG_LOCAL_FILE))
                offline_=true;
            else
                offline_=false;
        }
    }

    bool isOffline() const
    {
        return offline_;
    }

    QFileInfo getLast() const
    {
        QSqlQuery query;
        query.exec(QString("select detail from %1 ORDER BY id DESC").arg(traceTable));
        if(query.next())
            return QFileInfo(query.value(0).toString());
        else
            return QFileInfo();
    }

    virtual void loadLocation()
    {
        if(QFile::exists(configFile))
        {
            QSettings cfg(configFile,QSettings::IniFormat);
            cfg.setIniCodec("UTF-8");
            int x=cfg.value("Location/x").toInt();
            int y=cfg.value("Location/y").toInt();
            this->move(x,y);

            currentLocation=cfg.value("Spoor/last").toString();
            if(currentLocation.isEmpty() || !QDir(currentLocation).exists())
                currentLocation=defaultLocation;
        }
        else
            currentLocation=defaultLocation;
    }

    virtual void saveLocation()
    {
        QSettings cfg(configFile,QSettings::IniFormat);
        cfg.setIniCodec("UTF-8");
        cfg.setValue("Location/x",this->x());
        cfg.setValue("Location/y",this->y());
        cfg.setValue("Spoor/last",currentLocation);
    }

    virtual void createMenu()
    {
        menu=new QMenu(this);
        menu->addAction(QStringLiteral("打开媒体文件夹"),this,
                        &MediaFolder::openMediaLocation);
        menu->addAction(QStringLiteral("设置媒体文件夹"),this,
                        &MediaFolder::setMediaLocation);
        menu->addSeparator();
        menu->addAction(QStringLiteral("加入收藏夹"),this,
                        &MediaFolder::addToFavorite);
        menu->addAction(QStringLiteral("添加到播放列表"),this,
                        &MediaFolder::addToPlaylist);
        menu->addSeparator();
        menu->addAction(QStringLiteral("重置媒体文件夹"),this,
                        &MediaFolder::resetMediaLocation);
    }

    virtual void play(const QFileInfo &fileInfo) = 0;
    virtual void updateView() = 0;
    virtual void addToFavorite(){}
    virtual void addToPlaylist(){}

    void openMediaLocation()
    {
        QDesktopServices::openUrl(QUrl::fromLocalFile(currentLocation));
    }

    void setMediaLocation()
    {
        currentLocation=QFileDialog::getExistingDirectory(0,QStringLiteral("选择媒体文件夹"));
        if(!currentLocation.isEmpty())
        {
            updateView();
        }
    }

    void resetMediaLocation()
    {
        currentLocation=defaultLocation;
        updateView();
    }

    bool trace(const QFileInfo &fileInfo)
    {
        QSqlQuery query;
        query.prepare(QString("insert into %1 (time,label,detail) values (?,?,?)").arg(traceTable));
        query.addBindValue(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
        query.addBindValue(fileInfo.fileName());
        query.addBindValue(fileInfo.filePath());
        return query.exec();
    }

    bool mark(const QString &label,const QString &detail)
    {
        QSqlQuery query;
        query.prepare(QString("insert into %1 (label,detail) values (?,?)").arg(markTable));
        query.addBindValue(label);
        query.addBindValue(detail);
        return query.exec();
    }

    bool _addToFavorite(const QFileInfo &fileInfo)
    {
        QSqlQuery query;
        query.prepare(QString("insert into %1 (category,label,detail) values (?,?,?)").arg(TD_FAVORITE));
        query.addBindValue(category_);
        query.addBindValue(fileInfo.fileName());
        query.addBindValue(fileInfo.filePath());
        return query.exec();
    }

    bool _addToPlaylist(const QFileInfo &fileInfo)
    {
        QSqlQuery query;
        query.prepare(QString("insert into %1 (category,label,detail) values (?,?,?)").arg(TD_PLAYLIST));
        query.addBindValue(category_);
        query.addBindValue(fileInfo.fileName());
        query.addBindValue(fileInfo.filePath());
        return query.exec();
    }

    void cleanTrace()
    {
        QSqlQuery query;
        query.exec(QString("TRUNCATE TABLE %1").arg(traceTable));
    }

    void cleanMark()
    {
        QSqlQuery query;
        query.exec(QString("TRUNCATE TABLE %1").arg(markTable));
    }

protected:
    void contextMenuEvent(QContextMenuEvent *)
    {
        menu->exec(QCursor::pos());
    }

    QFileSystemModel *model;
    QMenu *menu;

    bool offline_;
    QString configFile;
    QString defaultLocation;
    QString currentLocation;

    QString category_;
    QString traceTable;
    QString markTable;
    QStringList nameFilter;
};

#endif // MEDIAFOLDER_H
