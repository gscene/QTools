#-------------------------------------------------
#
# Project created by QtCreator 2016-05-12T19:45:44
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UUIDGenerator
TEMPLATE = app

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        dialog.cpp

HEADERS  += dialog.h \
    g_ver.h

FORMS    += dialog.ui

RESOURCES += \
    res.qrc

RC_ICONS = build.ico
