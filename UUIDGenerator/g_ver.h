#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "UUIDGenerator"
#define SP_VER 1005
#define SP_UID "{ac84970d-17e5-404f-bd19-88f97786e812}"
#define SP_TYPE "shell"

#define SP_CFG "UUIDGenerator.ini"
#define SP_INFO "UUIDGenerator.json"
#define SP_LINK "UUIDGenerator.lnk"

#endif // G_VER_H
