#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QClipboard>
#include <QString>
#include <QStringList>
#include <QStringListModel>
#include <QUuid>
#include <QStandardPaths>
#include <QDateTime>
#include <QFile>
#include <QByteArray>
#include <QMessageBox>

#define PRE_NAME QString("/uuid-")

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void updateView();

private slots:
    void on_btn_gen_clicked();
    void on_btn_save_clicked();
    void on_listView_doubleClicked(const QModelIndex &index);

private:
    Ui::Dialog *ui;

    QStringList items;
    QStringListModel *listModel;
};

#endif // DIALOG_H
