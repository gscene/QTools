﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    listModel=new QStringListModel(this);
    ui->listView->setModel(listModel);

    updateView();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::updateView()
{
    QString uuid=QUuid::createUuid().toString();
    listModel->setStringList(QStringList() << uuid);
}

void Dialog::on_btn_gen_clicked()
{
    int count = ui->in_num->value();

    if(!items.isEmpty())
        items.clear();

    for(int i=0;i < count;i++)
        items.append(QUuid::createUuid().toString());

    listModel->setStringList(items);
}

void Dialog::on_btn_save_clicked()
{
    if(items.isEmpty())
        return;

    QString date=QDateTime::currentDateTime().toString("MMddmmss");
    QString desktopPath=QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    QString fileName=desktopPath + PRE_NAME + date + QString(".txt");

    QFile file(fileName);
    if(file.open(QFile::Append | QFile::Text))
    {
        file.write(items.join("\n").toUtf8());
        file.close();

        QString word=QStringLiteral("文件已保存为：\n") + fileName;
        QMessageBox::information(this,QStringLiteral("操作已完成"),
                                 word,QMessageBox::Ok);
    }
}

void Dialog::on_listView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QString row=index.data().toString();
        if(!row.isEmpty())
        {
            qApp->clipboard()->setText(row);
            QMessageBox::information(this,QStringLiteral("操作已完成"),
                                     row + QStringLiteral(" 已复制到剪贴板"),
                                     QMessageBox::Ok);
        }
    }
}
