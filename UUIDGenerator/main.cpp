﻿#include "dialog.h"
#include <QApplication>
#include "boot/g_bootfunction.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if(sp_parseArgument(argc,argv) == EXIT_SUCCESS)
        return EXIT_SUCCESS;

    Dialog w;
    w.show();

    return a.exec();
}
