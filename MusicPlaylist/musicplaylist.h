﻿#ifndef MUSICPLAYLIST_H
#define MUSICPLAYLIST_H

#include <QWidget>
#include <QSettings>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QFileDialog>
#include <QHash>
#include <QListView>
#include <QStringListModel>
#include <QStandardPaths>
#include <QMediaPlaylist>
#include <QMediaContent>
#include <QKeyEvent>
#include <QContextMenuEvent>
#include <QMenu>
#include <QAction>
#include <QCursor>

#include "playlistplayer.h"

#define SELECT_COUNT "SELECT name,path,COUNT(name) as count from music_log GROUP BY name ORDER BY count DESC LIMIT 10"

namespace Ui {
class MusicPlaylist;
}

class MusicPlaylist : public QWidget
{
    Q_OBJECT

public:
    explicit MusicPlaylist(QWidget *parent = 0);
    ~MusicPlaylist();

    void keyPressEvent(QKeyEvent *event);
    void contextMenuEvent(QContextMenuEvent *);

    void createMenu();
    void updateView();

    void loadDefaultFile();
    void genDefaultList();

    void addMediaFile();
    void removeMediaFile();

    void playNext();
    void playPrevious();

    void setPlayMode_TrackPlayOnce();
    void setPlayMode_TrackPlayLoop();
    void setPlayMode_ListPlaySequence();
    void setPlayMode_ListPlayLoop();

private slots:
    void stateChanged(QMediaPlayer::State state);
    void mediaError(QMediaPlayer::Error error);
    void currentName(const QString &name);
    void currentMediaChanged(const QMediaContent &media);

    void on_listView_doubleClicked(const QModelIndex &index);

private:
    Ui::MusicPlaylist *ui;

    QString playListFile;
    QString musicLocation;
    bool offline;

    QString fileName;
    QStringList mediaFileList;
    QStringListModel *model;
    QMediaPlaylist *playList;
    PlaylistPlayer *player;

    QMenu *menu;
    QMenu *m_playMode;

    QAction *addMediaAction;
    QAction *removeMediaAction;

    QAction *playMode_TrackPlayOnce;
    QAction *playMode_TrackPlayLoop;
    QAction *playMode_ListPlaySequence;
    QAction *playMode_ListPlayLoop;

    QHash<QString,QString> playListItems;
};

#endif // MUSICPLAYLIST_H
