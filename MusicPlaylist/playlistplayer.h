#ifndef PLAYLISTPLAYER_H
#define PLAYLISTPLAYER_H

#include <QMediaPlayer>
#include <QMediaPlaylist>

class PlaylistPlayer : public MusicPlayer
{
    Q_OBJECT

public:
    enum PlayMode
    {
        TrackPlayOnce,    //单曲播放一次停止
        TrackPlayLoop,    //单曲循环播放
        ListPlaySequence,    //列表顺序播放，默认模式
        ListPlayLoop       //列表循环播放
    };

    explicit PlaylistPlayer(QObject *parent = 0, bool offlineFlag=false);

    void setPlayMode(PlayMode mode);
    void setNameList(const QStringList &list);
    void setPlaylistItems(const QHash<QString,QString> &items);
    void playNext();

private slots:
    void processMediaStatus(QMediaPlayer::MediaStatus status);

private:
    PlayMode playMode;

    QStringList nameList;
    QHash<QString,QString> playListItems;
};

#endif // PLAYLISTPLAYER_H
