﻿#ifndef M_FHS_H
#define M_FHS_H

#include "g_ver.h"
#include "head/g_fhs.h"

#define M_CONFIG QString(FHS_ETC) + SP_CFG
#define M_KEY "Mode/offline"

#define DB_CONFIG "../etc/storage.ini"
#define DEF_PLAYLIST "/QVertex/playlist.m3u"

#endif // M_FHS_H
