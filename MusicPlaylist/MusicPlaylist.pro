#-------------------------------------------------
#
# Project created by QtCreator 2016-09-09T18:51:19
#
#-------------------------------------------------

QT       += core gui multimedia sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = MusicPlaylist
TEMPLATE = app

INCLUDEPATH += ../../elfproj
include(../qtsingleapplication/src/qtsingleapplication.pri)

SOURCES += main.cpp\
        musicplaylist.cpp \
    ../../elfproj/support/musicplayer.cpp \
    playlistplayer.cpp

HEADERS  += musicplaylist.h \
    ../../elfproj/support/musicplayer.h \
    g_ver.h \
    playlistplayer.h \
    ../../elfproj/support/mysql_helper.h \
    m_fhs.h

FORMS    += musicplaylist.ui

RESOURCES += \
    res.qrc

RC_ICONS = playlist.ico
