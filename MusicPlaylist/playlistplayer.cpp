﻿#include "playlistplayer.h"

PlaylistPlayer::PlaylistPlayer(QObject *parent, bool offlineFlag) :
    MusicPlayer(parent, offlineFlag)
{
    playMode=PlaylistPlayer::ListPlaySequence;

    if(!offline)
    {
        connect(this,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),
                this,SLOT(processMediaStatus(QMediaPlayer::MediaStatus)));
    }
}

void PlaylistPlayer::setPlayMode(PlaylistPlayer::PlayMode mode)
{
    playMode=mode;
}

void PlaylistPlayer::processMediaStatus(QMediaPlayer::MediaStatus status)
{
    if(status == QMediaPlayer::EndOfMedia)
        playNext();
}

void PlaylistPlayer::setNameList(const QStringList &list)
{
    nameList=list;
}

void PlaylistPlayer::setPlaylistItems(const QHash<QString, QString> &items)
{
    playListItems=items;
}

void PlaylistPlayer::playNext()
{
    QString fileName=_fileInfo.completeBaseName();
    int index=nameList.indexOf(fileName);

    switch (playMode) {
    case PlaylistPlayer::TrackPlayOnce:      //啥也不做
        break;
    case PlaylistPlayer::TrackPlayLoop:      //反复播放同一首
        this->playMusic(_fileInfo);
        break;
    case PlaylistPlayer::ListPlaySequence:
        if(index < nameList.size() -1)  //只有大于一首歌才有效
        {
            _fileInfo=QFileInfo(nameList.at(index + 1));
            this->playMusic(_fileInfo);
        }
        break;
    case PlaylistPlayer::ListPlayLoop:
        if(index < nameList.size() -1 )
            _fileInfo=QFileInfo(nameList.at(index + 1));
        else if(index == nameList.size() -1)
            _fileInfo=QFileInfo(nameList.at(0));

        this->playMusic(_fileInfo);
        break;
    }
}
