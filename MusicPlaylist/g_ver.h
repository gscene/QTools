﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "MusicPlaylist"
#define SP_VER 1006
#define SP_UID "{89d90f1d-c5dd-4db7-84c1-6bdafa3e2567}"
#define SP_TYPE "shell"

#define SP_CFG "MusicPlaylist.ini"
#define SP_INFO "MusicPlaylist.json"
#define SP_LINK "MusicPlaylist.lnk"

/*
 * 1001 项目开始
 * 1002 可以设置在线或是离线模式
 * 1005 分离出DB配置文件
 * 1006 #13
*/

#endif // G_VER_H
