﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_btn_date_clicked()
{
    QDate src=ui->date_src->date();
    QDate dst=ui->date_dst->date();
    qint64 offset=src.daysTo(dst);
    if(offset < 0)
        offset=qAbs(offset);
    ui->lbl_day->setText(QString::number(offset));
}

void Dialog::on_btn_time_clicked()
{
    QTime src=ui->time_src->time();
    QTime dst=ui->time_dst->time();
    qint64 offset=src.secsTo(dst);
    if(offset < 0)
        offset=qAbs(offset);
    ui->lbl_sec->setText(QString::number(offset));
}
