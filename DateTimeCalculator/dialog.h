﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QString>
#include <QDate>
#include <QTime>
#include <QDateTime>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_btn_date_clicked();

    void on_btn_time_clicked();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
