﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define P_UPDATE "p_update"
#define DEF_CATE "default"

#define ID_COL 0
#define CAT_COL 1
#define LAB_COL 2
#define DAT_COL 3
#define VER_COL 4
#define LOC_COL 5
#define REM_COL 6

#endif // M_FHS_H
