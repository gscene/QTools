#-------------------------------------------------
#
# Project created by QtCreator 2018-06-06T11:13:45
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QUpdater
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
        main.cpp \
        widget.cpp \
    ../../elfproj/support/sp_env.cpp \
    additem.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/common/baseeditor.h \
    additem.h

FORMS += \
        widget.ui \
    additem.ui

RESOURCES += \
    res.qrc

RC_ICONS = update.ico
