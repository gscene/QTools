﻿#include "additem.h"
#include "ui_additem.h"

Additem::Additem(QWidget *parent, int id) :
    QDialog(parent),id_(id),
    ui(new Ui::Additem)
{
    ui->setupUi(this);

    table=P_UPDATE;
    if(id_ != -1)
    {
        setWindowTitle(QStringLiteral("更新条目"));
        ui->btn_submit->setText(QStringLiteral("保存"));
    }

    updateCategory();
}

Additem::~Additem()
{
    delete ui;
}

void Additem::updateCategory()
{
    if(ui->categories->count() != 0)
        ui->categories->clear();

    QStringList items=sp_getCategory(table);
    if(!items.isEmpty())
        ui->categories->addItems(items);

    ui->categories->addItem(MANUAL_INPUT);
    ui->categories->setCurrentIndex(-1);
}

bool Additem::addItem(const QString &category, const QString &label, const QString &version, const QString &local, const QString &remote)
{
    QSqlQuery query;
    query.prepare(QString("insert into %1 (category,label,date_,version,local,remote) values (?,?,?,?,?,?)")
                  .arg(table));
    query.addBindValue(category);
    query.addBindValue(label);
    query.addBindValue(Today);
    query.addBindValue(version);
    query.addBindValue(local);
    query.addBindValue(remote);
    if(query.exec())
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

bool Additem::updateItem(const QString &category,
                         const QString &label,
                         const QString &local,
                         const QString &remote)
{
    QSqlQuery query;
    QString sql=QString("update %1 set category='%2',label='%3',local='%4',remote='%5' where id=%6")
            .arg(table)
            .arg(category)
            .arg(label)
            .arg(local)
            .arg(remote)
            .arg(id_);
    if(query.exec(sql))
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void Additem::setCategory(const QString &category)
{
    ui->categories->setCurrentText(category);
}

void Additem::setItem(const QString &category,
                      const QString &label,
                      const QString &local,
                      const QString &remote)
{
    category_=category;
    label_=label;
    local_=local;
    remote_=remote;

    setCategory(category);
    ui->label->setText(label);
    ui->version->setEnabled(false);
    ui->local->setText(local);
    ui->remote->setText(remote);
}

void Additem::on_btn_paste_clicked()
{
    QString url=qApp->clipboard()->text();
    if(!url.isEmpty())
        ui->remote->setText(url);
}

void Additem::on_btn_pick_clicked()
{
    QString previous=ui->local->text().trimmed();
    QString local;
    if(!previous.isEmpty() && QDir(previous).exists())
        local=QFileDialog::getExistingDirectory(this,QStringLiteral("选择位置"),previous);
    else
        local=QFileDialog::getExistingDirectory(this,QStringLiteral("选择位置"));

    if(!local.isEmpty())
        ui->local->setText(local);
}

void Additem::on_btn_submit_clicked()
{
    if(id_ != -1)
    {
        QString category=ui->category->text().trimmed();
        QString label=ui->label->text().trimmed();
        QString local=ui->local->text().trimmed();
        QString remote=ui->remote->text().trimmed();
        if(category_ == category && label_ == label && local_ == local && remote_ == remote)
        {
            MESSAGE_NOT_CHANGE
        }
        else
        {
            if(updateItem(category,label,local,remote))
                accept();
            else
                MESSAGE_CANNOT_SUBMIT
        }
    }
    else
    {
        QString category=ui->category->text().trimmed();
        if(category.isEmpty())
            category=DEF_CATE;

        QString label=ui->label->text().trimmed();
        QString version=ui->version->text().trimmed();
        if(label.isEmpty() || version.isEmpty())
            return;

        QString local=ui->local->text().trimmed();
        QString remote=ui->remote->text().trimmed();
        if(addItem(category,label,version,local,remote))
            accept();
        else
            MESSAGE_CANNOT_SUBMIT
    }
}

void Additem::on_categories_currentTextChanged(const QString &text)
{

    if(text == MANUAL_INPUT)
    {
        ui->category->clear();
        ui->category->setEnabled(true);
    }
    else
    {
        ui->category->setText(text);
        ui->category->setEnabled(false);
    }
}
