﻿#ifndef WIDGET_H
#define WIDGET_H

#include "common/baseeditor.h"
#include "additem.h"
#include <head/g_functionbase.h>

namespace Ui {
class Widget;
}

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void generateMenu();
    void updateCategory();
    void updateView();
    void setHeaderData();
    void do_openUrl();
    void do_addItem();
    void do_updateItem();
    void do_removeItem();
    void do_updateVersion();
    void do_openLocalLocation();
    bool updateVersion(const QString &version, int id);

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_listView_doubleClicked(const QModelIndex &index);

    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QString selectCate;
    QStringListModel *cateModel;
};

#endif // WIDGET_H
