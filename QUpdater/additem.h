﻿#ifndef ADDITEM_H
#define ADDITEM_H

#include <head/g_pch.h>
#include <head/db_helper.h>
#include "m_fhs.h"
#include <head/m_message.h>

#define MANUAL_INPUT QStringLiteral("-手动输入-")

namespace Ui {
class Additem;
}

class Additem : public QDialog
{
    Q_OBJECT

public:
    explicit Additem(QWidget *parent, int id=-1);
    ~Additem();

    void updateCategory();

    bool addItem(const QString &category,
                 const QString &label,
                 const QString &version,
                 const QString &local,
                 const QString &remote);

    // 不在这里更新版本号
    bool updateItem(const QString &category,
                    const QString &label,
                    const QString &local,
                    const QString &remote);

    void setCategory(const QString &category);

    void setItem(const QString &category,
                 const QString &label,
                 const QString &local,
                 const QString &remote);

private slots:
    void on_btn_paste_clicked();
    void on_btn_pick_clicked();
    void on_btn_submit_clicked();
    void on_categories_currentTextChanged(const QString &text);

private:
    Ui::Additem *ui;

    QString table;
    int id_;
    QString category_,label_,local_,remote_;
};

#endif // ADDITEM_H
