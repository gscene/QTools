﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QUpdater"
#define SP_VER 1005
#define SP_UID "{a0f384c9-3463-431b-8cdb-751f418c907e}"
#define SP_TYPE "prop"

#define SP_CFG "QUpdater.ini"
#define SP_INFO "QUpdater.json"
#define SP_LINK "QUpdater.lnk"

/*
 * 1002
 * 1003 列宏，菜单折叠，快键键
 * 1004 updateVersion
 * 1005 quickConnectFromSHM
*/

#endif // G_VER_H
