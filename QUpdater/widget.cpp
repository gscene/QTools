﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    BaseEditor(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    table=P_UPDATE;

    cateModel=new QStringListModel(this);
    ui->listView->setModel(cateModel);

    model=new QSqlTableModel(this);
    ui->tableView->setModel(model);

    updateCategory();
    createMenu();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::AltModifier && event->key()==Qt::Key_W)
        do_openUrl();
    else if(event->modifiers() == Qt::ControlModifier && event->key()==Qt::Key_L)
        do_openLocalLocation();
    else
    {
        switch (event->key()) {
        case Qt::Key_F1:
            do_addItem();
            break;
        case Qt::Key_F2:
            do_updateVersion();
            break;
        case Qt::Key_F3:
            do_updateItem();
            break;
        case Qt::Key_F4:
            updateCategory();
            break;
        case Qt::Key_F5:
            updateView();
            break;
        case Qt::Key_Delete:
            do_removeItem();
            break;
        }
    }
}

void Widget::updateCategory()
{
    if(cateModel->rowCount() != 0)
        cateModel->setStringList(QStringList());

    if(!selectCate.isEmpty())
        selectCate.clear();

    QStringList items=sp_getCategory(table);
    if(!items.isEmpty())
        cateModel->setStringList(items);

    updateView();
}

void Widget::updateView()
{
    if(model->rowCount() != 0)
        model->clear();

    if(selectCate.isEmpty())
        return;

    model->setTable(table);
    model->setFilter(QString("category='%1'").arg(selectCate));
    model->select();

    setHeaderData();
}

void Widget::setHeaderData()
{
    ui->tableView->hideColumn(ID_COL);   //ID
    ui->tableView->hideColumn(CAT_COL);   //CATE
    model->setHeaderData(LAB_COL,Qt::Horizontal,QStringLiteral("名称")); //label
    ui->tableView->setColumnWidth(LAB_COL,160);
    model->setHeaderData(DAT_COL,Qt::Horizontal,QStringLiteral("更新日期")); //date
    model->setHeaderData(VER_COL,Qt::Horizontal,QStringLiteral("版本号")); //version
    model->setHeaderData(LOC_COL,Qt::Horizontal,QStringLiteral("本地位置")); //local
    ui->tableView->hideColumn(REM_COL);   //remote
}

void Widget::generateMenu()
{
    menu->addAction(QStringLiteral("打开链接 (Alt+W)"),this,&Widget::do_openUrl);
    menu->addSeparator();
    menu->addAction(QStringLiteral("添加... (F1)"),this,&Widget::do_addItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新类别 (F4)"),this,&Widget::updateCategory);
    menu->addAction(QStringLiteral("更新视图 (F5)"),this,&Widget::updateView);
    menu->addSeparator();

    QMenu *editMenu=menu->addMenu(QStringLiteral("编辑"));
    editMenu->addAction(QStringLiteral("更新版本 (F2)"),this,&Widget::do_updateVersion);
    editMenu->addAction(QStringLiteral("编辑条目 (F3)"),this,&Widget::do_updateItem);

    menu->addSeparator();
    menu->addAction(QStringLiteral("打开位置 (Ctrl+L)"),this,&Widget::do_openLocalLocation);
}

void Widget::do_openLocalLocation()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QString item=sp_fetchString(index,LOC_COL);
        if(!item.isEmpty())
            QDesktopServices::openUrl(QUrl::fromLocalFile(item));
    }
}

bool Widget::updateVersion(const QString &version, int id)
{
    QSqlQuery query;
    QString sql=QString("update %1 set date_='%2',version='%3' where id=%4")
            .arg(table)
            .arg(Today)
            .arg(version)
            .arg(id);
    if(query.exec(sql))
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void Widget::do_updateVersion()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        int id=sp_fetchId(index);
        QString version=sp_fetchString(index,VER_COL);
        QString newVersion=QInputDialog::getText(this,QStringLiteral("输入新版本"),
                                                 QStringLiteral("版本号："),
                                                 QLineEdit::Normal,
                                                 version);

        if(newVersion.isEmpty() || version == newVersion)
            return;

        if(updateVersion(newVersion,id))
            updateView();
    }
}

void Widget::on_listView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        selectCate=index.data().toString();
        updateView();
    }
}

void Widget::do_openUrl()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QString url=sp_fetchString(index,REM_COL);
        if(!url.isEmpty())
            QDesktopServices::openUrl(QUrl(url));
    }
}

void Widget::do_addItem()
{
    Additem item(this);
    if(!selectCate.isEmpty())
        item.setCategory(selectCate);

    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Widget::do_updateItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        int id=sp_fetchId(index);
        QString cate=sp_fetchString(index,CAT_COL);
        QString label=sp_fetchString(index,LAB_COL);
        QString local=sp_fetchString(index,LOC_COL);
        QString remote=sp_fetchString(index,REM_COL);

        Additem item(this,id);
        item.setItem(cate,label,local,remote);
        if(item.exec() == QDialog::Accepted)
            updateView();
    }
}

void Widget::do_removeItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QMessageBox::StandardButton result=MESSAGE_DELETE_CONFIRM
                if(result == QMessageBox::Yes)
        {
                if(sp_removeModelIndex(table,index))
                updateView();
                else
                MESSAGE_CANNOT_DELETE
    }
    }
}

void Widget::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QString url=sp_fetchString(index,REM_COL);
        if(!url.isEmpty())
            QDesktopServices::openUrl(QUrl(url));
    }
}
