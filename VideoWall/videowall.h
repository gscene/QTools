﻿#ifndef VIDEOWALL_H
#define VIDEOWALL_H

#include <QWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <QRect>

#include <QtAV>
#include <QtAVWidgets>
#include <QMessageBox>
#include <QFileDialog>

#include <QStandardPaths>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QContextMenuEvent>
#include <QMenu>
#include <QAction>
#include <QCursor>

#include <QList>
#include <QGridLayout>
#include <QTimerEvent>

#define WIDTH_MIN 640
#define HEIGHT_MIN 360
#define INTERVAL 1000

class VideoWall : public QWidget
{
    Q_OBJECT

public:
    explicit VideoWall(QWidget *parent = 0);
    ~VideoWall();

    void contextMenuEvent(QContextMenuEvent *);
    void keyPressEvent(QKeyEvent *event);
    void timerEvent(QTimerEvent *event);
    void createMenu();

    void initializeWall();
    void play(const QString &file);
    void detectState();
    void stop();
    void open();

    void changeClockType();

private:
    int timer_id;
    QtAV::AVClock *clock;
    QList<QtAV::AVPlayer*> players;

    QMenu *menu;
    QString videoLocation;

    int rows,cols;
};

#endif // VIDEOWALL_H
