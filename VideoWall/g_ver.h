﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "VideoWall"
#define SP_VER 1002
#define SP_UID "{a3fbb987-0152-4822-a3a7-d7cc6d698880}"
#define SP_TYPE "shell"

#define SP_INFO "VideoWall.json"
#define SP_LINK "VideoWall.lnk"

/*
 * 1001
 * 1002 快捷键与同步
*/

#endif // G_VER_H
