#-------------------------------------------------
#
# Project created by QtCreator 2017-02-24T19:42:49
#
#-------------------------------------------------

QT       += core gui av avwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VideoWall
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        videowall.cpp

HEADERS  += videowall.h \
    g_ver.h

RESOURCES += \
    res.qrc

RC_ICONS = wall.ico
