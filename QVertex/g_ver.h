﻿#ifndef G_VER
#define G_VER

#define SP_NAME "QVertex"
#define SP_VER 1036
#define SP_UID "{e7979301-8ba0-4361-bb10-3ac19ded8982}"
#define SP_TYPE "shell"

#define SP_CFG "QVertex.ini"
#define SP_INFO "QVertex.json"
#define SP_LINK "QVertex.lnk"

/*
 * 1002 使用注册表取代json作为信息文件，使用ini取代json作为配置文件
 * 1003 收藏夹和菜单现在共用一个数据库
 * 1005 增加ThroughPanel
 * 1007 修正无法调用外部程序的bug
 * 1008 使用ini作为本地配置文件
 * 1009 增加了实用快捷键功能
 * 1010 分离出BaseFacade,取消了Sprite
 * 1011 修改了头文件结构，增加了Servant辅助线程
 * 1012 增加了信息选项
 * 1013 集成了mysql，增加了线程池
 * 1014 取消mysql，由storagetray取代，取消注册表
 * 1015 修正了百度云、磁链的bug
 * 1016 baseFacade修正
 * 1017 菜单功能简化，组件菜单改为不可编辑型
 * 1018 菜单功能继续简化
 * 1019 #15
 * 1020 #21
 * 1021 #25
 * 1023 工具箱
 * 1024 自动刷新工具箱
 * 1025 Monitor截图功能
 * 1026 检测是否是激活窗口
 * 1027 高级模式
 * 1028 reg_winver 硬件信息
 * 1029 可配置的自动隐藏与截屏时间
 * 1030 截屏位置修正,命令功能
 * 1031 项目重制
 * 1032 重制
 * 1033 优化
 * 1034 结构改善
 * 1035 rebuildMenu
 * 1036 ActionCommander,暂时无法使用多线程操作数据库
*/

#endif // G_VER

