#-------------------------------------------------
#
# Project created by QtCreator 2015-05-21T20:45:11
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = QVertex
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)

INCLUDEPATH += $$PWD
INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
    facade.cpp \
    module/popup.cpp \
    module/t_dialog.cpp \
    module/torus.cpp \
    module/note.cpp \
    module/note2.cpp \
    module/searchbox.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += \
    module/actioncommander.h \
    feature/feature.h \
    feature/smartmenu.h \
    feature/verifyworker.h \
    module/popup.h \
    module/t_dialog.h \
    module/torus.h \
    module/note.h \
    module/note2.h \
    g_ver.h \
    m_fhs.h \
    module/searchbox.h \
    facade.h \
    basefacade.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/common/t_facade.h \
    ../../elfproj/element/baseudpreceiver.h \
    ../../elfproj/element/baserestreceiver.h \
    ../../elfproj/element/commandstore.h \
    ../../elfproj/element/simpleconfigurator.h

FORMS    += facade.ui \
    module/popup.ui \
    module/torus.ui \
    module/note.ui \
    module/note2.ui \
    module/searchbox.ui

RESOURCES += \
    res.qrc

RC_ICONS = app.ico
