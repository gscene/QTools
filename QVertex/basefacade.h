﻿#ifndef BASEFACADE_H
#define BASEFACADE_H

#include "common/t_facade.h"
#include "m_fhs.h"

class BaseFacade : public TransFacade
{
    Q_OBJECT

public:
    explicit BaseFacade(QWidget *parent = 0)
        : TransFacade(parent)
    {
        userLand = Location::document + APP_ROOT;

        QDir dir(userLand);
        if(!dir.exists())
            dir.mkpath(userLand);

        if(getUid().isEmpty())
            putUid(QUuid::createUuid().toString());
    }

    QString getUid() const
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        return reg.value("uid").toString();
    }
    void putUid(const QString &uid)\
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        reg.setValue("uid",uid);
    }

    int getSkin() const
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        return reg.value("skin").toInt();
    }
    void putSkin(int skin)
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        reg.setValue("skin",skin);
    }

protected:
    QString userLand;
};

#endif // BASEFACADE_H
