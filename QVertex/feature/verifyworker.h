﻿#ifndef VERIFYWORKER_H
#define VERIFYWORKER_H

#include <head/g_functionbase.h>

class VerifyWorker : public QObject
{
    Q_OBJECT
public:
    explicit VerifyWorker(QObject *parent = nullptr): QObject(parent)
    {}

    void verifyHash(QCryptographicHash::Algorithm method,const QString &fileName)
    {
        QByteArray data=sp_verifyFile(method,fileName);
        emit sendResult(method,data);
    }

signals:
    void sendResult(QCryptographicHash::Algorithm method,const QByteArray &result);
};

#endif // VERIFYWORKER_H
