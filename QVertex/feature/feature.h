﻿#ifndef FEATURE_H
#define FEATURE_H

#include "m_fhs.h"
#include <head/g_pch.h>
#include <element/simpleconfigurator.h>
#include "verifyworker.h"

class Feature : public QObject
{
    Q_OBJECT
public:
    explicit Feature(QObject *parent = nullptr): QObject(parent)
    {
        form = Form_None;
        isWorking=false;
        QString date=QDate::currentDate().toString("yyyyMMdd");
        shotLocation=Location::genericDataLocation + PRE_SCRSHOTS + date;
    }

    ~Feature()
    {
        workerThread.quit();
        workerThread.wait();
    }

    bool isWorking;
    SP_Form form;
    QString entry;  // 双击入口点
    QString shotLocation;
    SimpleConfigurator config;

    void startUp()
    {
        startVerifyWorker();
        if(config.isMonitor)
            monitorId=startTimer(MONITOR_INTERVAL);
    }

    void startVerifyWorker()
    {
        qRegisterMetaType<QCryptographicHash::Algorithm>("QCryptographicHash::Algorithm");

        worker=new VerifyWorker;
        connect(&workerThread,&QThread::finished,worker,&QObject::deleteLater);
        connect(this,&Feature::sendRequest,worker,&VerifyWorker::verifyHash);
        connect(worker,&VerifyWorker::sendResult,this,&Feature::receiveResult);

        worker->moveToThread(&workerThread);
        workerThread.start();
    }

    void receiveResult(QCryptographicHash::Algorithm method, const QByteArray &result)
    {
        switch (method) {
        case QCryptographicHash::Md5:
            qApp->clipboard()->setText(QString(result));
            isWorking=false;
            emit say("MD5: " + QString(result) + QStringLiteral(" 已复制到剪贴板"));
            break;
        case QCryptographicHash::Sha1:
            qApp->clipboard()->setText(QString(result));
            isWorking=false;
            emit say("SHA1: " + QString(result) + QStringLiteral(" 已复制到剪贴板"));
            break;
        }
    }

    void util_mouseDoubleClickEvent()
    {
        if(form == Form_None)
        {
            QUrl url(config.entry);
            if(url.isValid())
                QDesktopServices::openUrl(url);
            return;
        }

        QString word=qApp->clipboard()->text();
        switch (form) {
        case Form_Magnet:
            if(word.startsWith("magnet:"))
            {
                QDesktopServices::openUrl(QUrl(word));
                return;
            }
            word.remove(QRegExp("\\s"));
            word = MAG_URL + word;
            qApp->clipboard()->setText(word);
            QDesktopServices::openUrl(QUrl(word));
            break;
        case Form_Pan:
            if(word.startsWith("http://"))
            {
                QDesktopServices::openUrl(QUrl(word));
                return;
            }
            word.remove(QRegExp("\\s"));
            word = PAN_URL + word;
            qApp->clipboard()->setText(word);
            QDesktopServices::openUrl(QUrl(word));
            break;
        case Form_StrLen:
            emit say(QStringLiteral("当前字符串长度为： ") + QString::number(word.length()));
            break;
        default:
            if(word.startsWith("http"))
                QDesktopServices::openUrl(QUrl(word));
            break;
        }
    }

    void dropFile(const QString &fileName)
    {
        QFileInfo fileInfo(fileName);
        int multiple;
        if(fileInfo.size() >= BASE_SIZE)
            multiple=fileInfo.size() / BASE_SIZE;
        else
            multiple=4;

        if(form == Form_MD5)
        {
            emit say(QStringLiteral("计算中..."),multiple);
            isWorking=true;
            emit sendRequest(QCryptographicHash::Md5,fileName);

        }
        else if(form == Form_SHA1)
        {
            emit say(QStringLiteral("计算中..."),multiple);
            isWorking=true;
            emit sendRequest(QCryptographicHash::Sha1,fileName);
        }
    }

    void util_reset()
    {
        if(!config.isAdvanced)
            return;

        form=Form_None;
        emit say(QStringLiteral("模式已重置"));
    }
    void util_generateUuid()
    {
        if(!config.isAdvanced)
            return;

        QString uuid=QUuid::createUuid().toString();
        qApp->clipboard()->setText(uuid);
        emit say(QStringLiteral("生成的UUID已经复制到剪贴板"));
    }

    void util_verifyMD5()
    {
        if(!config.isAdvanced)
            return;

        if(isWorking)
        {
            emit say(QStringLiteral("稍等，上个任务还在计算中"));
            return;
        }

        form=Form_MD5;
        emit say(QStringLiteral("已进入MD5计算模式，请把文件拖放到我身上"));
    }

    void util_verifySHA1()
    {
        if(!config.isAdvanced)
            return;

        if(isWorking)
        {
            emit say(QStringLiteral("稍等，上个任务还在计算中"));
            return;
        }

        form=Form_SHA1;
        emit say(QStringLiteral("已进入SHA1计算模式，请把文件拖放到我身上"));
    }

    void util_webThrough()
    {
        if(!config.isAdvanced)
            return;

        form=Form_Web;
        emit say(QStringLiteral("已进入网址直达模式，请先复制网址然后双击我"));
    }
    void util_magnet()
    {
        if(!config.isAdvanced)
            return;

        form=Form_Magnet;
        emit say(QStringLiteral("已进入磁链模式，请先复制代码然后双击我"));
    }
    void util_baiduPan()
    {
        if(!config.isAdvanced)
            return;

        form=Form_Pan;
        emit say(QStringLiteral("已进入百度云模式，请先复制提取码然后双击我"));
    }
    void util_stringLength()
    {
        if(!config.isAdvanced)
            return;

        form=Form_StrLen;
        emit say(QStringLiteral("已进入字符串长度计算模式，请先复制内容然后双击我"));
    }
    void util_grubScreen()
    {
        if(!config.isAdvanced)
            return;

        QPixmap pix=qApp->primaryScreen()->grabWindow(0);
        if(pix.isNull())
            return;

        // 文件名不能使用冒号！
        QString time=QDateTime::currentDateTime().toString("yyyy-MM-dd hhmmss");
        QString fileName=time + ".png";
        QString filePath=Location::picture + "/" + fileName;

        QFile file(filePath);
        file.open(QFile::WriteOnly);
        if(pix.save(&file,"PNG"))
            emit say(QStringLiteral("截屏已保存到图片文件夹"));
    }

    void util_shotScreen()
    {
        QPixmap pix=qApp->primaryScreen()->grabWindow(0);
        if(pix.isNull())
            return;

        QDir dir(shotLocation);
        if(!dir.exists())
            dir.mkpath(shotLocation);

        QString time=QDateTime::currentDateTime().toString("hhmmss");
        QString fileName=time + ".png";
        QString filePath=shotLocation + "/" + fileName;

        QFile file(filePath);
        file.open(QFile::WriteOnly);
        pix.save(&file,"PNG");
    }

signals:
    void say(const QString &message,unsigned sec=4);
    void sendRequest(QCryptographicHash::Algorithm method,const QString &fileName);

protected:
    void timerEvent(QTimerEvent *event)
    {
        int id=event->timerId();
        if(id == monitorId)
            util_shotScreen();
    }

private:
    int monitorId;
    VerifyWorker *worker;
    QThread workerThread;
};

#endif // FEATURE_H
