﻿#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"

#define PRE_SCRSHOTS "/Screenshots/"
#define e_note "/note.txt"
#define e_note21 "/note21.txt"
#define e_note22 "/note22.txt"

#define README "../share/readme.html"
#define Calc "calc"

#define ID_NOTE 10
#define ID_NOTE2 11
#define ID_SEARCHBOX 12
#define ID_CALC 13

#define sk_gre 0x0f00
#define sk_blu 0x0f01   //0x0f01=3841
#define sk_pik 0x0f02
#define sk_def 0x0f03

#define SCHED_ONCE "sched_once"
#define SCHED_PERIOD "sched_period"

#define P_LAUNCHER "p_launcher"
#define P_SITE "p_site"

#define T_LOCATION "location"
#define T_LOCATION_LOW "location_low"
#define T_TOOL "tool"
#define T_COMMAND "command"
#define T_PROPS "props"
#define T_SYSTOOL "system"
#define T_APP "app"
#define T_URL "url"

#define MENU_GET_VARS "select label,detail from td_menu_preset where category='%1'"
#define SELECT_VARS "select label,detail from %1 where category='%2'"

#define MAG_URL "magnet:?xt=urn:btih:"
#define PAN_URL "http://pan.baidu.com/s/"

enum SP_Form
{
    Form_None,      //Ctrl+Alt+Backspace
    Form_MD5,       //J
    Form_SHA1,      //L
    Form_Web,       //W
    Form_Magnet,    //M
    Form_Pan,        //P
    Form_StrLen,
    Form_PrintScr
};

#endif // M_FHS_H
