﻿#ifndef FACADE_H
#define FACADE_H

#include "basefacade.h"
#include "module/actioncommander.h"
#include "module/searchbox.h"
#include "module/torus.h"
#include "module/popup.h"
#include "module/note.h"
#include "module/note2.h"

#include "feature/feature.h"
#include "feature/smartmenu.h"

#include "element/baseudpreceiver.h"
#include "element/baserestreceiver.h"
#include "element/commandstore.h"

#define skin_blu ui->body->setStyleSheet("image: url(:/res/Mario_Blue.png)")
#define skin_gre ui->body->setStyleSheet("image: url(:/res/Mario_Green.png)")
#define skin_pik ui->body->setStyleSheet("image: url(:/res/Mario_Pink.png)")

namespace Ui {
class Facade;
}

class Facade : public BaseFacade
{
    Q_OBJECT

public:
    explicit Facade(QWidget *parent = 0);
    ~Facade();

    void startUp();
    void startReceiver();
    void endReceiver();
    void receivedMessage(const QByteArray &data);
    void receivedObject(const QJsonObject &json);
    void parseCommand(const QString &cmd);
    void executeCommand(int code,QStringList params);

    void setSkin();

    void showNote();
    void showNote2();
    void invokeCalc();
    void showSearchBox();

    void pop(const QString &, const QString &);
    void say(const QString &, unsigned sec=4);

    void createMenu();
    void rebuildMenu();
    void invokeProp(int propId);
    void showMessage(const QString &title,const QString &message);
    void changeSkin(int skinId);

    void onTrayStateChanged();

signals:
    void execute(int code,QStringList params);

protected:
    void keyPressEvent(QKeyEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void timerEvent(QTimerEvent *event);
    void contextMenuEvent(QContextMenuEvent *);

private:
    Ui::Facade *ui;

    int autoHideId;

    CommandStore cmdStore;
    ActionCommander commander;
    Feature *feature;
    SmartMenu *menu;

    BaseUdpReceiver *udpReceiver;
    QThread udpRecvThread;

    BaseRestReceiver *restReceiver;
    QThread restRecvThread;

    Popup *popup;
    Note *note;
    Note2 *note2;
    SearchBox *searchBox;
};

#endif // FACADE_H
