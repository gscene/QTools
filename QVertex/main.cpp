﻿#include "facade.h"
#include <QtSingleApplication>
#include "boot/e_boot.h"

int main(int argc, char *argv[])
{
    SP_Single_Boot
            if(!quickConnectFromSHM(SHM_KEY))
    {
        MESSAGE_CRITICAL_ERROR
    }

    a.setQuitOnLastWindowClosed(false);
    Facade w;
    a.setActivationWindow(&w);
    w.show();

    return a.exec();
}
