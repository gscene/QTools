﻿#include "searchbox.h"
#include "ui_searchbox.h"

SearchBox::SearchBox(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SearchBox)
{
    ui->setupUi(this);
}

SearchBox::~SearchBox()
{
    delete ui;
}

void SearchBox::on_btn_go_clicked()
{
    QString searchStr=ui->in_cmd->text();
    if(searchStr.isEmpty())
        return;

    if(searchStr.startsWith("#"))
    {
        searchStr = searchStr.remove("#");
        emit sendCmd(searchStr);
    } // 指令模式
    else
    {
        searchStr=SEARCH + searchStr;
        QDesktopServices::openUrl(QUrl(searchStr));
    } // 搜索模式
    ui->in_cmd->clear();
    close();
}
