﻿#include "popup.h"
#include "ui_popup.h"

Popup::Popup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Popup)
{
    ui->setupUi(this);

    // setAttribute(Qt::WA_ShowWithoutActivating);

    setWindowFlags(
                Qt::FramelessWindowHint | // No window border
                //           Qt::WindowDoesNotAcceptFocus | // No focus
                Qt::WindowStaysOnTopHint |// Always on top
                Qt::Tool
                );
}

Popup::~Popup()
{
    delete ui;
}

void Popup::showMe(const QString &title, const QString &content)
{
    ui->title->setText(QString("<b>%1</b>").arg(title));
    ui->content->setText(content);

    setGeometry(QStyle::alignedRect(
                    Qt::RightToLeft,
                    Qt::AlignBottom,
                    size(),
                    qApp->primaryScreen()->availableGeometry()));
    //qApp->desktop()->availableGeometry()));
    show();
}

void Popup::mouseDoubleClickEvent(QMouseEvent *)
{
    close();
}
