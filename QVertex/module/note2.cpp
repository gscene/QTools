﻿#include "note2.h"
#include "ui_note2.h"

Note2::Note2(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Note2)
{
    ui->setupUi(this);
}

Note2::~Note2()
{
    delete ui;
}

void Note2::setUserLand(const QString &location)
{
    userLand=location;
}

void Note2::closeEvent(QCloseEvent *)
{
    save();
    accept();
}

void Note2::firstLoad()
{
    QFile file1(userLand + e_note21);
    if(file1.exists())
    {
        QTextStream text1(&file1);
        file1.open(QFile::ReadWrite | QFile::Text);
        QString content1=text1.readAll();
        ui->textEdit_1->setPlainText(content1);
        file1.close();
    }

    QFile file2(userLand + e_note22);
    if(file2.exists())
    {
        QTextStream text2(&file2);
        file2.open(QFile::ReadWrite | QFile::Text);
        QString content2=text2.readAll();
        ui->textEdit_2->setPlainText(content2);
        file2.close();
    }
}

void Note2::save()
{
    QFile file1(userLand + e_note21);
    file1.open(QFile::WriteOnly | QFile::Text);
    QTextStream text1(&file1);
    text1 << ui->textEdit_1 ->toPlainText();
    file1.close();

    QFile file2(userLand + e_note22);
    file2.open(QFile::WriteOnly | QFile::Text);
    QTextStream text2(&file2);
    text2 << ui->textEdit_2 ->toPlainText();
    file2.close();
}
