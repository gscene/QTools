﻿#ifndef SEARCHBOX_H
#define SEARCHBOX_H

#include <QDialog>
#include <QString>
#include <QUrl>
#include <QDesktopServices>

#define SEARCH "https://www.baidu.com/s?wd="

namespace Ui {
class SearchBox;
}

class SearchBox : public QDialog
{
    Q_OBJECT

public:
    explicit SearchBox(QWidget *parent = 0);
    ~SearchBox();

signals:
    void sendCmd(const QString &cmd);

private slots:
    void on_btn_go_clicked();

private:
    Ui::SearchBox *ui;
};

#endif // SEARCHBOX_H
