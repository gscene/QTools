﻿#ifndef TORUS_H
#define TORUS_H

#include "t_dialog.h"
#include <QString>
#include <QTimer>

#define sk_gre 0x0f00
#define sk_blu 0x0f01   //0x0f01=3841
#define sk_pik 0x0f02

#define TORUS_BLU ui->body->setStyleSheet("border-image: url(:/res/bg_blue.png)")
#define TORUS_PIK ui->body->setStyleSheet("border-image: url(:/res/bg_pink.png)")
#define TORUS_GRE ui->body->setStyleSheet("border-image: url(:/res/bg_green.png)")

namespace Ui {
class Torus;
}

class Torus : public TransDialog
{
    Q_OBJECT

public:
    explicit Torus(QWidget *parent = 0);
    ~Torus();

    void mouseDoubleClickEvent(QMouseEvent *);

    void setStyle(int);
    void display(const QString &,unsigned sec=4);

private:
    Ui::Torus *ui;
};

#endif // TORUS_H
