﻿#include "note.h"
#include "ui_note.h"
#include <QDebug>

Note::Note(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Note)
{
    ui->setupUi(this);
}

Note::~Note()
{
    delete ui;
}

void Note::setUserLand(const QString &location)
{
    userLand=location;
}

void Note::firstLoad()
{
    QFile file(userLand + e_note);
    if(file.exists())
    {
        QTextStream text(&file);
        file.open(QFile::ReadWrite | QFile::Text);
        QString content=text.readAll();
        ui->textEdit->setPlainText(content);
        file.close();
    }
}

void Note::save()
{
    QFile file(userLand + e_note);
    file.open(QFile::WriteOnly | QFile::Text);
    QTextStream text(&file);
    text << ui->textEdit->toPlainText();
    file.close();
}

void Note::closeEvent(QCloseEvent *)
{
    save();
    accept();
}

void Note::on_in_date_clicked()
{
    ui->textEdit->appendPlainText(QDate::currentDate().toString("yyyy-MM-dd"));
}

void Note::on_in_time_clicked()
{
    ui->textEdit->appendPlainText(QTime::currentTime().toString());
}

