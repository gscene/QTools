#-------------------------------------------------
#
# Project created by QtCreator 2017-12-04T09:42:46
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VideoPicker
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        widget.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/element/locationsynchronizer.h

FORMS += \
        widget.ui

RESOURCES += \
    res.qrc

RC_ICONS = picker.ico

win32:VERSION = 1.0.4.2 # major.minor.patch.build
  else:VERSION = 1.0.4    # major.minor.patch
