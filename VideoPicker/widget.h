﻿#ifndef WIDGET_H
#define WIDGET_H

#include "common/baseeditor.h"
#include "element/locationsynchronizer.h"

namespace Ui {
class Widget;
}

class Widget : public BaseEditor
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void updateTopicView();
    void generateMenu();
    void updateTopicLocation();
    void updateLocationView();
    void openTopicLocation();
    void startSynchronizer();
    bool updateItem(const QString &tag);
    void addPath();
    void removePath();
    void showUnfinished();

signals:
    void add_path(const QString &path);
    void remove_path(const QString &path);

private slots:
    void on_btn_save_clicked();
    void on_search_returnPressed();
    void on_topicView_doubleClicked(const QModelIndex &index);
    void on_listView_clicked(const QModelIndex &index);
    void on_listView_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QString locationTable;
    QString selectTopic;
    QString selectLabel;
    QString selectTag;

    QMap<QString,QString> topicDetails;
    QMap<QString,QString> itemDetails;

    QStringListModel *topicModel;
    QStringListModel *listModel;

    LocationSynchronizer *synchronizer;
};

#endif // WIDGET_H
