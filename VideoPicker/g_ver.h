﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "VideoPicker"
#define SP_VER 1003
#define SP_UID "{UUID}"
#define SP_TYPE "prop"

#define SP_CFG "VideoPicker.ini"
#define SP_INFO "VideoPicker.json"
#define SP_LINK "VideoPicker.lnk"

/*
 * 1002
 * 1003 LocationSynchronizer
 */
#endif // G_VER_H
