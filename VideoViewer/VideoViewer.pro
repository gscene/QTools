#-------------------------------------------------
#
# Project created by QtCreator 2019-01-07T17:43:44
#
#-------------------------------------------------

QT       += core gui network sql multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VideoViewer
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += ../../elfproj

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h \
    g_ver.h \
    m_fhs.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
