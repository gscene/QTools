#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "VideoViewer"
#define SP_VER 1004
#define SP_UID "{b0e8a17f-f236-4152-bddd-b2d9ebb7a8ca}"
#define SP_TYPE "prop"

#define SP_CFG "VideoViewer.ini"
#define SP_INFO "VideoViewer.json"
#define SP_LINK "VideoViewer.lnk"

#endif // G_VER_H
