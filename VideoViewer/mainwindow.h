#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <head/g_pch.h>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QVideoProbe>
#include <QVideoFrame>

#include "m_fhs.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void positionChanged(qint64 position);
    void currentMediaChanged(const QMediaContent &media);
    void sliderMoved(int position);

    void toggleState();
    void toggleEscape();
    void showSlider(int maximum);

private slots:
    void on_actionOpen_triggered();
    void on_actionInfo_triggered();
    void on_actionProgress_triggered();

protected:
    void keyPressEvent(QKeyEvent *event);
    void closeEvent(QCloseEvent *event);
    void moveEvent(QMoveEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    Ui::MainWindow *ui;

    QSlider *slider;
    QMediaPlayer *player;
    QVideoWidget *videoWidget;
    QString videoLocation;
};

#endif // MAINWINDOW_H
