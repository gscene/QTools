#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    videoLocation=QStandardPaths::writableLocation(QStandardPaths::MoviesLocation);

    slider=new QSlider(Qt::Horizontal);
    slider->setWindowFlag(Qt::FramelessWindowHint);
    slider->setTracking(false);
    slider->setFocusPolicy(Qt::NoFocus);
    connect(slider,&QSlider::sliderMoved,
            this,&MainWindow::sliderMoved);

    player=new QMediaPlayer(this);
    connect(player,&QMediaPlayer::currentMediaChanged,
            this,&MainWindow::currentMediaChanged);
    connect(player,&QMediaPlayer::positionChanged,
            this,&MainWindow::positionChanged);

    videoWidget=new QVideoWidget(this);
    player->setVideoOutput(videoWidget);
    setCentralWidget(videoWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::toggleState()
{
    if(player->state() == QMediaPlayer::PlayingState)
        player->pause();
    else if(player->state() == QMediaPlayer::PausedState)
        player->play();
}

void MainWindow::toggleEscape()
{
    if(player->state() == QMediaPlayer::PlayingState)
        player->pause();
    else
        close();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Space:
        toggleState();
        break;
    case Qt::Key_Escape:
        toggleEscape();
        break;
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event)
    if(slider->isVisible())
        slider->close();
}

void MainWindow::moveEvent(QMoveEvent *event)
{
    Q_UNUSED(event)
    if(slider->isVisible())
        slider->move(x(),y() + frameGeometry().height());
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    if(slider->isVisible())
        slider->setGeometry(x(),y() + frameGeometry().height(),
                            frameGeometry().width(),28);
}

void MainWindow::positionChanged(qint64 position)
{
    if(slider->isVisible())
        slider->setValue(position / 1000);
}

void MainWindow::sliderMoved(int position)
{
    if(player->state() == QMediaPlayer::PlayingState)
        player->setPosition(position * 1000);
}

void MainWindow::currentMediaChanged(const QMediaContent &media)
{
    QString mediaFile=media.canonicalUrl().toLocalFile();
    setWindowTitle(QFileInfo(mediaFile).fileName());
}

void MainWindow::on_actionOpen_triggered()
{
    QString path=QFileDialog::getOpenFileName(this,"选择视频文件",
                                              videoLocation,
                                              "视频文件 (*.mp4 *.mkv *.avi)");
    if(path.isEmpty())
        return;

    player->setMedia(QUrl::fromLocalFile(path));
    player->play();
}

void MainWindow::on_actionInfo_triggered()
{
    if(player->state() == QMediaPlayer::PlayingState)
    {
        int AudioBitRate=player->metaData("AudioBitRate").toInt();
        QString AudioCodec=player->metaData("AudioCodec").toString();
        int SampleRate=player->metaData("SampleRate").toInt();
        QSize Resolution=player->metaData("Resolution").toSize();
        double VideoFrameRate=player->metaData("VideoFrameRate").toDouble();
        int VideoBitRate=player->metaData("VideoBitRate").toInt();
        QString VideoCodec=player->metaData("VideoCodec").toString();
        int duration=player->duration();

        QStringList summary;
        summary << "音频信息——"
                << "比特率： " + QString::number(AudioBitRate / 1000) + " Kbps"
                << "编码格式： " + AudioCodec
                << "采样率： " + SampleRate
                << "视频信息——"
                << "分辨率： " + QString::number(Resolution.width()) + " X " + QString::number(Resolution.height())
                << "帧率： " + QString::number(VideoFrameRate)
                << "比特率: " + QString::number(VideoBitRate / 1000 / 1000) + " Mbps"
                << "编码格式： " + VideoCodec
                << "时长： " + QString::number(duration / 1000) + "秒";

        QMessageBox::information(this,"媒体信息",summary.join("\n"));
    }
}

void MainWindow::showSlider(int maximum)
{
    if(player->isSeekable())
    {
        slider->setGeometry(x(),y() + frameGeometry().height(),
                            frameGeometry().width(),28);

        slider->setMaximum(maximum);
        slider->show();
    }
}

void MainWindow::on_actionProgress_triggered()
{
    if(slider->isVisible())
        slider->close();
    else {
        if(player->state() == QMediaPlayer::PlayingState ||
                player->state() == QMediaPlayer::PausedState)
            showSlider(player->duration() / 1000);
    }
}