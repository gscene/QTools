#-------------------------------------------------
#
# Project created by QtCreator 2017-10-04T16:07:35
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = QNoteLite
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        widget.cpp \
    ../../elfproj/support/sp_env.cpp \
    main.cpp

HEADERS += \
        widget.h \
    m_fhs.h \
    g_ver.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/common/baseeditor.h

FORMS += \
        widget.ui

RESOURCES += \
    res.qrc

RC_ICONS = note.ico
