﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QNoteLite"
#define SP_VER 1003
#define SP_UID "{a78929e9-9d7f-4b29-ac63-9a72df7e36ab}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "QNoteLite.ini"
#define SP_INFO "QNoteLite.json"
#define SP_LINK "QNoteLite.lnk"

/*
 * 1002
 * 1003 tray
*/

#endif // G_VER_H
