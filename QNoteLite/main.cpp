﻿#include "widget.h"
#include <QtSingleApplication>
#include "boot/e_boot.h"

int main(int argc, char *argv[])
{
    SP_Single_Boot
            a.setQuitOnLastWindowClosed(false);
    Widget w;
    a.setActivationWindow(&w);
    w.show();

    return a.exec();
}
