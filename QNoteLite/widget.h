﻿#ifndef WIDGET_H
#define WIDGET_H

#include <head/g_pch.h>
#include <head/g_functionbase.h>
#include <common/baseeditor.h>
#include <template/m_sql.h>

namespace Ui {
class Widget;
}

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void detectDB();
    bool db_try();
    void updateView();
    void generateMenu();
    void newItem();
    void removeItem();
    void clear();
    bool addItem(const QString &label,const QString &detail,
                 const QString &addition);
    bool updateItem(const QString &label,const QString &detail,
                    const QString &addition);
    void createTray();

public slots:
    void trayActivated(QSystemTrayIcon::ActivationReason reason);

private slots:
    void on_tableView_doubleClicked(const QModelIndex &index);
    void on_btn_copy_clicked();
    void on_detail_textChanged();
    void on_btn_clear_clicked();
    void on_btn_save_clicked();

    void on_label_textChanged(const QString &arg1);
    void on_addition_textChanged(const QString &arg1);

private:
    Ui::Widget *ui;

    QSqlDatabase db;
    QString dbFile;

    bool updateFlag;
    QString currentUid;
    QString currentLabel,currentAddition;
    QSystemTrayIcon *tray;
    QMenu *trayMenu;
};

#endif // WIDGET_H
