﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    value=0;
    baseTime=QTime::fromString(ZERO_TIME,"hh:mm:ss");
    ui->lcd->display(ZERO_TIME);

    QString musicLocation=QStandardPaths::writableLocation(QStandardPaths::MusicLocation);
    recordLocation = musicLocation + "/Record";
    QDir dir(recordLocation);
    if(!dir.exists())
        dir.mkpath(recordLocation);

    ui->location->setText(QString(LINKS).arg(recordLocation).arg(QStringLiteral("打开目录")));
    initAudioRecorder();

    skin_start;
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::setCurrentOutputFile()
{
    QString currentTime=QDateTime::currentDateTime().toString("yyMMdd-hhmmss");
    QString location=recordLocation + "/" + currentTime + QString(".wav");
    audioRecorder->setOutputLocation(QUrl::fromUserInput(location));
}

void Dialog::initAudioRecorder()
{
    audioRecorder=new QAudioRecorder(this);

    QAudioEncoderSettings audioSettings;
    audioSettings.setCodec("audio/pcm");
    audioSettings.setQuality(QMultimedia::HighQuality);
 //   audioSettings.setSampleRate(16000);
    audioRecorder->setEncodingSettings(audioSettings);
    audioRecorder->setContainerFormat("wav");

    //   audioRecorder->setAudioInput(audioRecorder->defaultAudioInput());
    //  默认 file:///C:/Users/mud/Documents/clip_0001.wav
    setCurrentOutputFile();
    connect(audioRecorder,&QAudioRecorder::stateChanged,
            this,&Dialog::stateChanged);

    this->setWindowTitle(audioRecorder->defaultAudioInput());
}

void Dialog::on_btn_control_clicked()
{
    detectState();
}

//主动行为
void Dialog::detectState()
{
    switch (audioRecorder->state()) {
    case QMediaRecorder::RecordingState:
        audioRecorder->stop();
        setCurrentOutputFile();
        break;
    case QMediaRecorder::PausedState:
        break;
    case QMediaRecorder::StoppedState:
        audioRecorder->record();
        timerId=startTimer(1000);
        break;
    }
}

void Dialog::stateChanged(QMediaRecorder::State state)
{
    switch (state) {
    case QMediaRecorder::RecordingState:
        skin_stop;
        ui->status->setText(QStringLiteral("录制中……"));
        break;
    case QMediaRecorder::PausedState:
        break;
    case QMediaRecorder::StoppedState:
        skin_start;
        value=0;
        ui->lcd->display(ZERO_TIME);
        killTimer(timerId);
        ui->status->setText(QStringLiteral("已停止"));
        break;
    }
}

void Dialog::timerEvent(QTimerEvent *event)
{
    if(event->timerId() == timerId)
    {
        value +=1;
        QTime time=baseTime.addSecs(value);
        ui->lcd->display(time.toString("hh:mm:ss"));
    }
}

void Dialog::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Space:
        detectState();
        break;
    case Qt::Key_Escape:
        if(audioRecorder->state() == QMediaRecorder::RecordingState)
            audioRecorder->stop();
        else
            close();
        break;
    }
}
