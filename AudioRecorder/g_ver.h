﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "AudioRecorder"
#define SP_VER 1004
#define SP_UID "{c0647a70-c0c2-47b5-a836-a138c16dc048}"
#define SP_TYPE "shell"

#define SP_CFG "AudioRecorder.ini"
#define SP_INFO "AudioRecorder.json"
#define SP_LINK "AudioRecorder.lnk"

/*
 * 1001
 * 1002 装饰
 * 1003 时间指示器
 * 1004 快捷键
 */
#endif // G_VER_H
