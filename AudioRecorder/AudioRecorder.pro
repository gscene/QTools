#-------------------------------------------------
#
# Project created by QtCreator 2017-02-12T20:54:00
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = AudioRecorder
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        dialog.cpp

HEADERS  += dialog.h \
    g_ver.h

FORMS    += dialog.ui

RESOURCES += \
    res.qrc

RC_ICONS = recorder.ico
