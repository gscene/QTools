﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QString>
#include <QStringList>
#include <QFileDialog>
#include <QUrl>
#include <QDir>
#include <QTime>
#include <QTimerEvent>
#include <QKeyEvent>

#include <QStandardPaths>
#include <QDateTime>

#include <QMultimedia>
#include <QAudioRecorder>
#include <QAudioEncoderSettings>
#include <QMediaPlayer>

#include "head/g_function.h"
#include <QDebug>

#define LINKS QStringLiteral("<a href=\"%1\">%2</a>")
#define skin_start ui->btn_control->setStyleSheet("border-image: url(:/res/start.png);")
#define skin_stop ui->btn_control->setStyleSheet("border-image: url(:/res/stop.png);")
#define ZERO_TIME "00:00:00"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void initAudioRecorder();
    void setCurrentOutputFile();
    void detectState();
    void stateChanged(QMediaRecorder::State state);
    void timerEvent(QTimerEvent *event);
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_btn_control_clicked();

private:
    Ui::Dialog *ui;

    QAudioRecorder *audioRecorder;
    QTime baseTime;
    int timerId;
    int value;

    QString recordLocation;
};

#endif // DIALOG_H
