#ifndef MESSAGEHELPER_H
#define MESSAGEHELPER_H

#include <QString>
#include <head/g_functionbase.h>
#include "message.pb.h"
#include "m_fhs.h"

#define MT_DEBUG "Debug"
#define MT_LOG "Log"

#define LV_NONE "None"
#define LV_INFO "Info"
#define LV_WARN "Warning"
#define LV_ERR "Error"

class MessageHelper
{
public:
    MessageHelper(){}
    MessageHelper(const QString &host_,
                  const QString &uid_) {
        host=host_;
        message.set_source(uid_.toStdString());
    }

    void setHostInfo(const QString &host_,
                     const QString &uid_)
    {
        host=host_;
        message.set_source(uid_.toStdString());
    }

    void info(const QString &content)
    {
        pushMessage(MT_LOG,LV_INFO,content);
    }

    void debug(const QString &content)
    {
        pushMessage(MT_DEBUG,LV_NONE,content);
    }

    void log(const QString &content)
    {
        pushMessage(MT_LOG,LV_INFO,content);
    }

    void warning(const QString &content)
    {
        pushMessage(MT_INFO,LV_WARN,content);
    }

    void error(const QString &content)
    {
        pushMessage(MT_INFO,LV_ERR,content);
    }

    void pushMessage(const QString &type,
                     const QString &level,
                     const QString &content)
    {
        message.set_type(type.toStdString());
        message.set_level(level.toStdString());
        message.set_content(content.toStdString());

        if(host.isEmpty())
            return;

        std::string buf{};
        message.SerializeToString(&buf);
        sp_sendData(buf.c_str(),host,PORT_MESSAGE);
    }

private:
    elfproj::proto::Message message;
    QString host;
};

#endif // MESSAGEHELPER_H
