﻿#include "widget.h"
#include <QtSingleApplication>
#include "boot/e_boot.h"
#include "support/mysql_helper.h"

int main(int argc, char *argv[])
{
    SP_Single_Boot

            if(!quickConnectFromSHM(SHM_KEY))
    {
        MESSAGE_CRITICAL_ERROR
    }

    QString folderPak=P_FHS_ICON + RES_FOLDER_FILE;
    if(QFileInfo::exists(folderPak))
        QResource::registerResource(folderPak);
    else
    {
        QMessageBox::critical(0,QStringLiteral("致命错误"),QStringLiteral("缺少资源文件，无法启动！"));
        return EXIT_FAILURE;
    }

    a.setQuitOnLastWindowClosed(false);
    Widget w;
    a.setActivationWindow(&w);
    w.show();

    return a.exec();
}
