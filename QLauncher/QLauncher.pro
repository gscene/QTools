#-------------------------------------------------
#
# Project created by QtCreator 2018-02-23T20:52:25
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QLauncher
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
        main.cpp \
        widget.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/element/urlinvoker.h \
    ../../elfproj/head/db_helper.h

FORMS += \
        widget.ui

RESOURCES += \
    res.qrc

RC_ICONS = launcher.ico
