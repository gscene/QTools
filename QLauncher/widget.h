﻿#ifndef WIDGET_H
#define WIDGET_H

#include <head/g_pch.h>
#include <common/baseeditor.h>
#include <extra/folder_icon.h>
#include <element/urlinvoker.h>
#include <head/m_message.h>

namespace Ui {
class Widget;
}

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void updateMenu();
    void createMenu();
    void do_updateView();
    void updateView();
    void updateView(int index);
    void updateAppView();
    void updateLocationView();
    void updateFileView();

    void createTray();

    void do_setIconSizeS();
    void do_setIconSizeM();
    void do_setIconSizeL();

    void setAppIconSize(int index);
    void setLocationIconSize(int index);
    void setFileIconSize(int index);

    void do_addItem();
    void do_addStandardPaths();
    void addStandardPath(const QString &path);
    void do_openItemLocation();
    void do_removeItem();
    void do_setItemIcon();
    void do_setItemLabel();

    void loadLocation();
    void saveLocation();
    void do_autoSaveLocation();

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    void contextMenuEvent(QContextMenuEvent *);
    void keyPressEvent(QKeyEvent *event);
    void timerEvent(QTimerEvent *);

public slots:
    void trayActivated(QSystemTrayIcon::ActivationReason reason);

private slots:
    void on_appWidget_itemDoubleClicked(QListWidgetItem *item);
    void on_locationWidget_itemDoubleClicked(QListWidgetItem *item);
    void on_fileWidget_itemDoubleClicked(QListWidgetItem *item);
    void on_kw_returnPressed();

private:
    Ui::Widget *ui;

    QSystemTrayIcon *tray;
    QMenu *trayMenu;
    UrlInvoker invoker;

    QMap<QString,QString> appItems;
    QMap<QString,QString> locationItems;
    QMap<QString,QString> fileItems;

    bool isConfigChanged;
    int appSizeIndicator;
    int locationSizeIndicator;
    int fileSizeIndicator;
};

#endif // WIDGET_H
