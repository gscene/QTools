﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QLauncher"
#define SP_VER 1017
#define SP_UID "{41e826ff-3741-4879-b082-2bd06c1f2327}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "QLauncher.ini"
#define SP_INFO "QLauncher.json"
#define SP_LINK "QLauncher.lnk"

/*
 * 1002
 * 1003 托盘图标
 * 1004 集成了默认图标
 * 1005 菜单设计
 * 1006 lastPage,lastSize
 * 1007 symlink识别
 * 1008 统一视图
 * 1009 folderIcon,do_addStandardPaths
 * 1010 菜单修正
 * 1011 F2重命名，自动保存,配置状态判断
 * 1012 重命名时自动输入原名
 * 1013 更多的快键键
 * 1014 输入即达,sp_removeItem
 * 1015 fetchData(MT_LINK)
 * 1016 for mingw64
 * 1017 quickConnectFromSHM
*/

#endif // G_VER_H
