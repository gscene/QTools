﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define P_LAUNCHER "p_launcher"

#define DEF_APP_ICON ":/res/app.png"
#define DEF_FILE_ICON ":/res/file.png"
#define DEF_DIR_ICON ":/res/folder.png"

#define IDX_APP 0
#define IDX_LOCATION 1
#define IDX_FILE 2

#define IDX_SIZE_S 0
#define IDX_SIZE_M 1
#define IDX_SIZE_L 2

#define SIZE_S QSize(48,48)
#define SIZE_M QSize(72,72)
#define SIZE_L QSize(96,96)

#endif // M_FHS_H
