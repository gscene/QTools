﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    padManager=QGamepadManager::instance();
    connect(padManager,&QGamepadManager::gamepadButtonPressEvent,
            this,&Widget::onGamepadButtonPressEvent);
    connect(padManager,&QGamepadManager::gamepadAxisEvent,
            this,&Widget::onGamepadAxisEvent);

    keyType=QMetaEnum::fromType<Qt::Key>();

    // 这里的方法没用处，因为这2个Enum不是纯数值，即使找到了也没用
    /*
    const QMetaObject padObject=QGamepadManager::staticMetaObject;
    int padIndex=padObject.indexOfEnumerator("GamepadButton");
    if(padIndex == -1)
        qDebug() << "GamepadButton NOT FIND";
    padType=padObject.enumerator(padIndex);

    int axisIndex=padObject.indexOfEnumerator("GamepadAxis");
    if(axisIndex == -1)
        qDebug() << "GamepadAxis NOT FIND";

    axisType=padObject.enumerator(axisIndex);
    */
}

Widget::~Widget()
{
    delete ui;
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    QString summary=keyType.valueToKey(event->key());
    if(!summary.isEmpty())
        ui->label->setText(summary);

    event->accept();
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    QString button;
    switch (event->button()) {
    case Qt::LeftButton:
        button="LeftButton";
        break;
    case Qt::RightButton:
        button="RightButton";
        break;
    case Qt::MidButton:
        button="MidButton";
        break;
    case Qt::BackButton:
        button="BackButton";
        break;
    case Qt::ForwardButton:
        button="ForwardButton";
        break;
    case Qt::TaskButton:
        button="TaskButton";
        break;
    }

    QPoint pos=event->pos();
    QString summary=button + ":" + QString::number(pos.x()) + QString(",") + QString::number(pos.y());
    ui->label->setText(summary);

    event->accept();
}

void Widget::wheelEvent(QWheelEvent *event)
{
    QPoint numPixels = event->pixelDelta();
    QPoint numDegrees = event->angleDelta() / 8;    // 都是15度

    QString summary;
    if(!numPixels.isNull())
    {
        QString pixSummary=QString("pixelDelta:") + QString::number(numPixels.x()) + ","
                + QString::number(numPixels.y());

        summary = pixSummary;
    }

    if(!numDegrees.isNull())
    {
        QString degSummary=QString("angleDelta:") + QString::number(numDegrees.x()) + ","
                + QString::number(numDegrees.y());

        if(!summary.isEmpty())
            summary += "\n";
        summary += degSummary;
    }

    if(!summary.isEmpty())
        ui->label->setText(summary);

    /*
    if (!numPixels.isNull()) {
        scrollWithPixels(numPixels);
    } else if (!numDegrees.isNull()) {
        QPoint numSteps = numDegrees / 15;
        scrollWithDegrees(numSteps);
    }
    */

    event->accept();
}

void Widget::onGamepadAxisEvent(int deviceId, QGamepadManager::GamepadAxis axis, double value)
{
    Q_UNUSED(deviceId)

    QString axis_;
    switch (axis) {
    /*
    case QGamepadManager::AxisInvalid:
        axis_ = "AxisInvalid";
        break;
        */
    case QGamepadManager::AxisLeftX:
        axis_ = "AxisLeftX";
        break;
    case QGamepadManager::AxisLeftY:
        axis_ = "AxisLeftY";
        break;
    case QGamepadManager::AxisRightX:
        axis_ = "AxisRightX";
        break;
    case QGamepadManager::AxisRightY:
        axis_ = "AxisRightY";
        break;
    default:
        axis_ = "AxisInvalid";
        break;
    }

    QString summary=axis_ + ":" + QString::number(value);
    ui->label->setText(summary);
}

void Widget::onGamepadButtonPressEvent(int deviceId, QGamepadManager::GamepadButton button, double value)
{
    Q_UNUSED(deviceId)
    Q_UNUSED(value)

    QString button_;
    switch (button) {
    /*
    case QGamepadManager::ButtonInvalid:
        button_ = "ButtonInvalid";
        break;
        */
    case QGamepadManager::ButtonA:
        button_ = "ButtonA";
        break;
    case QGamepadManager::ButtonB:
        button_ = "ButtonB";
        break;
    case QGamepadManager::ButtonX:
        button_ = "ButtonX";
        break;
    case QGamepadManager::ButtonY:
        button_ = "ButtonY";
        break;
    case QGamepadManager::ButtonL1:
        button_ = "ButtonL1";
        break;
    case QGamepadManager::ButtonR1:
        button_ = "ButtonR1";
        break;
    case QGamepadManager::ButtonL2:
        button_ = "ButtonL2";
        break;
    case QGamepadManager::ButtonR2:
        button_ = "ButtonR2";
        break;
    case QGamepadManager::ButtonSelect:
        button_ = "ButtonSelect";
        break;
    case QGamepadManager::ButtonStart:
        button_ = "ButtonStart";
        break;
    case QGamepadManager::ButtonL3:
        button_ = "ButtonL3";
        break;
    case QGamepadManager::ButtonR3:
        button_ = "ButtonR3";
        break;
    case QGamepadManager::ButtonUp:
        button_ = "ButtonUp";
        break;
    case QGamepadManager::ButtonDown:
        button_ = "ButtonDown";
        break;
    case QGamepadManager::ButtonRight:
        button_ = "ButtonRight";
        break;
    case QGamepadManager::ButtonLeft:
        button_ = "ButtonLeft";
        break;
    case QGamepadManager::ButtonCenter:
        button_ = "ButtonCenter";
        break;
    case QGamepadManager::ButtonGuide:
        button_ = "ButtonGuide";
        break;
    default:
        button_ = "ButtonInvalid";
        break;
    }

    ui->label->setText(button_);
}
