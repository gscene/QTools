﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QMetaEnum>
#include <QMetaObject>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QPoint>
#include <QGamepad>
#include <QGamepadManager>
#include <QDebug>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void onGamepadAxisEvent(int deviceId, QGamepadManager::GamepadAxis axis, double value);
    void onGamepadButtonPressEvent(int deviceId, QGamepadManager::GamepadButton button, double value);

protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    Ui::Widget *ui;

    QMetaEnum keyType;
    QGamepadManager *padManager;
};

#endif // WIDGET_H
