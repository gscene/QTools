#ifndef SECTION_H
#define SECTION_H

#include <QtGlobal>
#include <QString>

struct Section
{
    int id;
    QString name;
};

#endif // SECTION_H
