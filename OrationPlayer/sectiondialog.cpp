#include "sectiondialog.h"
#include "ui_sectiondialog.h"

SectionDialog::SectionDialog(QWidget *parent) :
    BaseEditorDialog(parent),
    ui(new Ui::SectionDialog)
{
    ui->setupUi(this);

    table=TD_SECTION;

    model=new QSqlTableModel(this);
    ui->tableView->setModel(model);

    updateView();
}

SectionDialog::~SectionDialog()
{
    delete ui;
}

void SectionDialog::updateView()
{
    model->setTable(table);
    model->select();

    model->setHeaderData(1,Qt::Horizontal,"ID");
    model->setHeaderData(2,Qt::Horizontal,"名称");
}

void SectionDialog::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        Section item;
        item.id=sp_fetchId(index);
        item.name=sp_fetchString(index,1);
        picked(item);
        accept();
    }
}
