#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define DEF_TITLE_SIZE 32
#define MIN_TITLE_SIZE 32
#define MAX_TITLE_SIZE 128

#define DEF_SUBTITLE_SIZE 18.0
#define MIN_SUBTITLE_SIZE 18.0
#define MAX_SUBTITLE_SIZE 72.0

#define TD_ARTICLE "td_article"
#define TD_SECTION "td_section"
#define TD_SITE "ref_article_site"

#endif // M_FHS_H
