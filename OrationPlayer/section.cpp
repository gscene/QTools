#include "section.h"
#include "ui_section.h"

SectionDialog::SectionDialog(QWidget *parent) :
    BaseEditorDialog(parent),
    ui(new Ui::Section)
{
    ui->setupUi(this);

    table=TD_SECTION;

    model=new QSqlTableModel(this);
    ui->tableView->setModel(model);

    updateView();
}

SectionDialog::~SectionDialog()
{
    delete ui;
}

void SectionDialog::updateView()
{
    model->setTable(table);
    model->select();

    model->setHeaderData(1,Qt::Horizontal,"ID");
    model->setHeaderData(2,Qt::Horizontal,"名称");
}

void SectionDialog::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        int id=sp_fetchId(index);
        picked(id);
        accept();
    }
}

