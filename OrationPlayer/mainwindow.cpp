#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    table=TD_ARTICLE;
    targetPos = 0;

    splashFrame();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_P)
        on_actionPickSection_triggered();
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_T)
        on_actionToggleView_triggered();
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_Q)
        close();
    switch (event->key()) {
    case Qt::Key_Plus:
        on_actionTitleUp_triggered();
        break;
    case Qt::Key_Minus:
        on_actionTitleDown_triggered();
        break;
    case Qt::Key_F11:
        toggleFullScreen();
        break;
    case Qt::Key_PageDown:
        playNext();
        break;
    case Qt::Key_PageUp:
        playPrevious();
        break;
    case Qt::Key_Down:
        playNext();
        break;
    case Qt::Key_Up:
        playPrevious();
        break;
    case Qt::Key_Left:
        on_actionSubtitleDown_triggered();
        break;
    case Qt::Key_Right:
        on_actionSubtitleUp_triggered();
        break;
    }
}

void MainWindow::onPicked(const Section &item)
{
    currentSection=item;
    loadArticle();
}

void MainWindow::splashFrame()
{
    auto font=ui->title->font();
    font.setPointSize(MAX_TITLE_SIZE);
    ui->title->setFont(font);
    ui->title->setText("联网找有人");

    ui->subtitle->setFontPointSize(MAX_SUBTITLE_SIZE * 3);
    ui->subtitle->setPlainText("靠谱");
    ui->subtitle->setAlignment(Qt::AlignCenter);
}

void MainWindow::resetFontSize()
{
    auto font=ui->title->font();
    font.setPointSize(DEF_TITLE_SIZE);
    ui->title->setFont(font);

    ui->subtitle->setFontPointSize(DEF_SUBTITLE_SIZE);
}

void MainWindow::toggleFullScreen()
{
    if(windowState() != Qt::WindowFullScreen)
    {
        showFullScreen();
        ui->menubar->setVisible(false);
    }
    else
    {
        showNormal();
        ui->menubar->setVisible(true);
    }
}

void MainWindow::on_actionToggleView_triggered()
{
    toggleFullScreen();
}

void MainWindow::append(const QString &message)
{
    ui->subtitle->append(message);
}

void MainWindow::playNext()
{
    if(seqList.isEmpty())
        return;

    if(targetPos == 0)
    {
        resetFontSize();
        playFrame();
        return;
    }

    if(targetPos == seqList.size() - 1)
    {
        ui->statusbar->showMessage("已到达最后一帧");
        return;
    }

    targetPos += 1;
    playFrame();
}

void MainWindow::playPrevious()
{
    if(seqList.isEmpty())
        return;

    if(targetPos == 0)
    {
        ui->statusbar->showMessage("当前为第一帧");
        return;
    }

    targetPos -=1;
    playFrame();
}

void MainWindow::loadArticle()
{
    if(!seqList.isEmpty())
        seqList.clear();

    if(!cache.isEmpty())
        cache.clear();

    QSqlQuery query;
    query.exec(QString("select seq,title,subtitle,prompt from %1 where section_id=%2")
               .arg(table)
               .arg(currentSection.id));
    while(query.next())
    {
        Frame item;
        item.seq=query.value(0).toLongLong();
        item.title=query.value(1).toString();
        item.subtitle=query.value(2).toString();
        item.prompt=query.value(3).toString();

        seqList.append(item.seq);
        cache.insert(item.seq,item);
    }

    if(!seqList.isEmpty())
        ui->statusbar->showMessage(QString("已加载 %1").arg(currentSection.name));
}

void MainWindow::playFrame()
{
    if(targetPos > seqList.size() - 1 || targetPos < 0)
        return;

    auto seq=seqList.at(targetPos);
    if(cache.contains(seq))
    {
        currentFrame=cache.value(seq);
        ui->title->setText(currentFrame.title);
        ui->subtitle->setPlainText(currentFrame.subtitle);
        ui->statusbar->showMessage(QString("当前帧: %1").arg(targetPos));
    }
}

void MainWindow::wheelEvent(QWheelEvent *event)
{
    if(event->angleDelta().y() > 0)
        playPrevious();
    else
        playNext();
}

void MainWindow::on_actionTitleUp_triggered()
{
    auto font=ui->title->font();
    if(font.pointSize() > MAX_TITLE_SIZE)
        return;

    font.setPointSize(font.pointSize() + 2);
    ui->title->setFont(font);
}

void MainWindow::on_actionTitleDown_triggered()
{
    auto font=ui->title->font();
    if(font.pointSize() < MIN_TITLE_SIZE)
        return;

    font.setPointSize(font.pointSize() - 2);
    ui->title->setFont(font);
}


void MainWindow::on_actionSubtitleUp_triggered()
{
    auto fontSize=ui->subtitle->fontPointSize();
    if(fontSize >= MAX_SUBTITLE_SIZE)
        return;

    if(!currentFrame.subtitle.isEmpty())
    {
        ui->subtitle->setPlainText(currentFrame.subtitle);
        ui->subtitle->setFontPointSize(fontSize + 2);
    }
}


void MainWindow::on_actionSubtitleDown_triggered()
{
    auto fontSize=ui->subtitle->fontPointSize();
    if(fontSize <= MIN_SUBTITLE_SIZE)
        return;

    if(!currentFrame.subtitle.isEmpty())
    {
        ui->subtitle->setPlainText(currentFrame.subtitle);
        ui->subtitle->setFontPointSize(fontSize - 2);
    }
}

void MainWindow::on_actionPickSection_triggered()
{
    SectionDialog dialog(this);
    connect(&dialog,&SectionDialog::picked,
            this,&MainWindow::onPicked);
    dialog.exec();
}
