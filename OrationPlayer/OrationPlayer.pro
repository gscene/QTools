QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
    ../../elfproj/support/sp_env.cpp \
    main.cpp \
    mainwindow.cpp \
    sectiondialog.cpp

HEADERS += \
    ../../elfproj/common/baseeditordialog.h \
    ../../elfproj/support/sp_env.h \
    frame.h \
    g_ver.h \
    m_fhs.h \
    mainwindow.h \
    section.h \
    sectiondialog.h

FORMS += \
    mainwindow.ui \
    sectiondialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
