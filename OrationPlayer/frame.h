#ifndef FRAME_H
#define FRAME_H

#include <QtGlobal>
#include <QString>

struct Frame
{
    qint64 seq;
    QString title;
    QString subtitle;
    QString prompt;
};

#endif // FRAME_H
