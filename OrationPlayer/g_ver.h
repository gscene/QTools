#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "OrationPlayer"
#define SP_VER 1004
#define SP_UID "{37e2a89d-8895-415f-a5e3-aa3183ad7a6d}"
#define SP_TYPE "prop"

#define SP_CFG "OrationPlayer.ini"
#define SP_INFO "OrationPlayer.json"
#define SP_LINK "OrationPlayer.lnk"

#endif // G_VER_H
