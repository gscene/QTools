#ifndef SECTIONDIALOG_H
#define SECTIONDIALOG_H

#include <common/baseeditordialog.h>
#include "m_fhs.h"
#include "section.h"

namespace Ui {
class SectionDialog;
}

class SectionDialog : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit SectionDialog(QWidget *parent = nullptr);
    ~SectionDialog();

    void updateView();

signals:
    void picked(const Section &item);

private slots:
    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    Ui::SectionDialog *ui;
};

#endif // SECTIONDIALOG_H
