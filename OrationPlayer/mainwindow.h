#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <head/db_helper.h>
#include "frame.h"
#include "sectiondialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void toggleFullScreen();

    void loadSection(int id);
    void loadArticle();

    void playFrame();
    void playNext();
    void playPrevious();
    void append(const QString &message);

    void homeFrame();   // 首页
    void endFrame();    // 尾页
    void splashFrame(); // 启动页
    void resetFontSize();

    void onPicked(const Section &item);

protected:
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *event);

private slots:
    void on_actionToggleView_triggered();
    void on_actionTitleUp_triggered();
    void on_actionTitleDown_triggered();
    void on_actionSubtitleUp_triggered();
    void on_actionSubtitleDown_triggered();

    void on_actionPickSection_triggered();

private:
    Ui::MainWindow *ui;

    QString table,refSiteTable;
    Section currentSection;
    Frame currentFrame;
    int targetPos;
    QList<qint64> seqList;
    QMap<qint64,Frame> cache;
};
#endif // MAINWINDOW_H
