#-------------------------------------------------
#
# Project created by QtCreator 2015-09-14T18:43:15
#
#-------------------------------------------------

QT       += core gui av avwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = VideoPlayer
TEMPLATE = app

INCLUDEPATH += ../../elfproj
include(../qtsingleapplication/src/qtsingleapplication.pri)

SOURCES += main.cpp\
        videoplayer.cpp \
    captureview.cpp

HEADERS  += videoplayer.h \
    g_ver.h \
    captureview.h

RESOURCES += \
    res.qrc

RC_ICONS = player.ico
