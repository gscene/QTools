﻿#include "videoplayer.h"
#include <QApplication>
#include "head/g_bootfunction.h"

int main(int argc, char *argv[])
{
    QApplication  a(argc, argv);
    if(sp_parseArgument(argc,argv) == EXIT_SUCCESS)
        return EXIT_SUCCESS;

    QtAV::Widgets::registerRenderers();

    VideoPlayer w;
    w.show();

    return a.exec();
}
