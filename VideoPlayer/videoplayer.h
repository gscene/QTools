﻿#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H

#include <QWidget>
#include <QtAV>
#include <QtAVWidgets>
#include <QSlider>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QFileDialog>

#include <QStandardPaths>
#include <QDir>
#include <QString>
#include <QFile>
#include <QFileInfo>

#include <QMouseEvent>
#include <QKeyEvent>
#include <QContextMenuEvent>

#include <QMenu>
#include <QAction>
#include <QCursor>

#include "captureview.h"

using namespace QtAV;

class VideoPlayer : public QWidget
{
    Q_OBJECT

public:
    explicit VideoPlayer(QWidget *parent = 0);
    ~VideoPlayer();

    // void mouseReleaseEvent(QMouseEvent *);
    void mouseDoubleClickEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *event);
    void contextMenuEvent(QContextMenuEvent *);
    void createMenu();

    void open();
    void seek(int);
    void detectState();
    void capture();

private slots:
    void updateSlider();

private:
    QMenu *menu;

    VideoOutput *m_vo;
    AVPlayer *m_player;
    QSlider *m_slider;

    QString videoLocation;
    QString captureLocation;

    CaptureView *preview;
};

#endif // VIDEOPLAYER_H
