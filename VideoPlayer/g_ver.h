﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "VideoPlayer"
#define SP_VER 1003
#define SP_UID "{5278eaae-31ed-4e0c-8474-31a6b27dfd70}"
#define SP_TYPE "shell"

#define SP_INFO "VideoPlayer.json"
#define SP_LINK "VideoPlayer.lnk"

/*
 * 1001 项目开始
 * 1002 capture
 * 1003 右键菜单
 *
*/
#endif // G_VER_H
