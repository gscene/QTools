﻿#include "videoplayer.h"

VideoPlayer::VideoPlayer(QWidget *parent)
    : QWidget(parent)
{
    this->setWindowTitle("VideoPlayer");
    this->setWindowIcon(QIcon(":/player.ico"));
    this->resize(960,540);

    videoLocation=QStandardPaths::writableLocation(QStandardPaths::MoviesLocation);
    QString pictureLocation=QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
    captureLocation=pictureLocation + "/Capture";

    m_player = new AVPlayer(this);
    m_vo = new VideoOutput(this);
    if (!m_vo->widget()) {
        QMessageBox::critical(0,QStringLiteral("致命错误"), QStringLiteral("无法创建视频渲染器！"));
        return;
    }
    m_player->setRenderer(m_vo);
    m_player->videoCapture()->setCaptureDir(captureLocation);

    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->setMargin(1);
  //  vbox->setSpacing(1);

    setLayout(vbox);
    vbox->addWidget(m_vo->widget());

    m_slider = new QSlider();
    m_slider->setOrientation(Qt::Horizontal);
    vbox->addWidget(m_slider);

    connect(m_slider,&QSlider::sliderMoved,this,&VideoPlayer::seek);
    connect(m_player,&AVPlayer::positionChanged,this,&VideoPlayer::updateSlider);
    connect(m_player,&AVPlayer::started,this,&VideoPlayer::updateSlider);

    preview=nullptr;
    createMenu();
}

VideoPlayer::~VideoPlayer()
{
    if(m_player != nullptr)
        m_player->stop();
}

void VideoPlayer::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::ControlModifier && event->key()==Qt::Key_O)
        open();
    else if(event->modifiers() == Qt::ControlModifier && event->key()==Qt::Key_Q)
        this->close();
    else
    {
        switch (event->key()) {
        case Qt::Key_Space:
            detectState();
            break;
        case Qt::Key_F2:
            capture();
            break;
        case Qt::Key_Escape:
            if(m_player->isPlaying() || m_player->isPaused())
                m_player->stop();
            else
                close();
            break;
        }
    }
}

void VideoPlayer::contextMenuEvent(QContextMenuEvent *)
{
    menu->exec(QCursor::pos());
}

void VideoPlayer::createMenu()
{
    menu=new QMenu(this);
    menu->addAction(QStringLiteral("打开 (Ctrl+O)..."),this,&VideoPlayer::open);
    menu->addAction(QStringLiteral("捕捉 (F2)"),this,&VideoPlayer::capture);
    menu->addSeparator();
    menu->addAction(QStringLiteral("退出 (Ctrl+Q)"),this,&QWidget::close);
}

/*
void VideoPlayer::mouseReleaseEvent(QMouseEvent *)
{
    if(m_player->isPlaying())
        detectState();
}
*/

void VideoPlayer::mouseDoubleClickEvent(QMouseEvent *)
{
    if(m_player->isPlaying())
        detectState();
    else
        open();
}

void VideoPlayer::open()
{
    QString file = QFileDialog::getOpenFileName(this, QStringLiteral("选择媒体文件"),videoLocation);
    if (file.isEmpty())
        return;

    QFileInfo fi(file);
    QString fileName=fi.baseName();
    this->setWindowTitle(fileName);

    m_player->play(file);
}

void VideoPlayer::capture()
{
    if(preview == nullptr)
    {
        preview = new CaptureView(this);
        connect(m_player->videoCapture(),&VideoCapture::imageCaptured,preview,&CaptureView::updateView);
    }

    QDir dir(captureLocation);
    if(!dir.exists())
        dir.mkpath(captureLocation);

    int xPos=this->x() + this->frameGeometry().width() + 10;
    preview->move(xPos,this->y());

    m_player->videoCapture()->capture();
}

void VideoPlayer::seek(int pos)
{
    if (!m_player->isPlaying())
        return;
    m_player->seek(pos*1000LL);
}

void VideoPlayer::detectState()
{
    if (!m_player->isPlaying()) {
        m_player->play();
        return;
    }
    m_player->pause(!m_player->isPaused());
}

void VideoPlayer::updateSlider()
{
    m_slider->setRange(0, int(m_player->duration()/1000LL));
    m_slider->setValue(int(m_player->position()/1000LL));
}
