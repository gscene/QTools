﻿#include "captureview.h"
#include <QDebug>

CaptureView::CaptureView(QWidget *parent) : QDialog(parent)
{
    this->setWindowTitle(QStringLiteral("预览"));
    this->resize(480,270);
    this->setFocusPolicy(Qt::NoFocus);

    QVBoxLayout *layout=new QVBoxLayout();
    layout->setMargin(1);
    this->setLayout(layout);

    preview=new QLabel(this);
    layout->addWidget(preview);
}

void CaptureView::updateView(const QImage &image)
{
    if(!this->isVisible())
        this->show();

    preview->setPixmap(QPixmap::fromImage(image).scaled(preview->size()));
}
