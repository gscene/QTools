﻿#ifndef CAPTUREVIEW_H
#define CAPTUREVIEW_H

#include <QDialog>
#include <QWidget>
#include <QString>
#include <QImage>
#include <QPixmap>
#include <QLabel>
#include <QVBoxLayout>

class CaptureView : public QDialog
{
public:
    explicit CaptureView(QWidget *parent = 0);

    void updateView(const QImage &image);

private:
    QLabel *preview;
};

#endif // CAPTUREVIEW_H
