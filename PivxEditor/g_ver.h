﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "PivxEditor"
#define SP_VER 1002
#define SP_UID "{634bb289-7011-4aac-874a-e710d2c92476}"
#define SP_TYPE "shell"

#define SP_CFG "PivxEditor.ini"
#define SP_INFO "PivxEditor.json"
#define SP_LINK "PivxEditor.lnk"

#endif // G_VER_H
