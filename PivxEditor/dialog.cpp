﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    model=nullptr;
    saveFlag=0;
    helper=new PivxModel(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_btn_add_clicked()
{
    if(model == nullptr)
        model=new QStandardItemModel(this);

    head=ui->head->text();
    tail=ui->tail->text();
    if(head.isEmpty() || tail.isEmpty())
    {
        QMessageBox::warning(this,QStringLiteral("不能为空"),QStringLiteral("键名、值名不能为空！"),
                             QMessageBox::Ok);
        return;
    }

    key=ui->key->text();
    value=ui->value->text();
    if(key.isEmpty() || value.isEmpty())
    {
        QMessageBox::warning(this,QStringLiteral("不能为空"),QStringLiteral("键、值不能为空！"),
                             QMessageBox::Ok);
        return;
    }

    if(headList.contains(key))
    {
        QMessageBox::warning(this,QStringLiteral("名称不能重复"),key + QStringLiteral("已经存在！"),
                             QMessageBox::Ok);
        return;
    }

    saveFlag=-1;
    headList.append(key);

    QList<QStandardItem *> item;
    item.append(new QStandardItem(key));
    item.append(new QStandardItem(value));
    model->appendRow(item);
    model->setHeaderData(0,Qt::Horizontal,head);
    model->setHeaderData(1,Qt::Horizontal,tail);
    ui->tableView->setModel(model);

    helper->setElementTag(head,tail);
    helper->append(key,value);

    ui->key->clear();
    ui->value->clear();
    if(!ui->lbl_status->text().isEmpty())
        ui->lbl_status->clear();
}

void Dialog::on_btn_open_clicked()
{
    head=ui->head->text();
    tail=ui->tail->text();
    if(head.isEmpty() || tail.isEmpty())
    {
        QMessageBox::warning(this,QStringLiteral("不能为空"),QStringLiteral("键名、值名不能为空！"),
                             QMessageBox::Ok);
        return;
    }

    _fileName=QFileDialog::getOpenFileName(this,QStringLiteral("选择一个文件"),"..","*.json");
    if(_fileName.isEmpty())
        return;

    helper->setFileName(_fileName);
    helper->setElementTag(head,tail);
    if(helper->load() && helper->loadModel())
    {
        headList=helper->getStringList();
        model=helper->getModel();
        model->setHeaderData(0,Qt::Horizontal,head);
        model->setHeaderData(1,Qt::Horizontal,tail);
        ui->tableView->setModel(model);
    }
}

void Dialog::on_btn_del_clicked()
{
    int row=ui->tableView->currentIndex().row();
    headList.removeAt(row);
    model->removeRow(row);
    helper->remove(row);
    saveFlag=-1;

    if(!ui->lbl_status->text().isEmpty())
        ui->lbl_status->clear();
}

void Dialog::on_btn_save_clicked()
{
    if(model == nullptr)
        return;

    switch (saveFlag) {
    case 0:
        QMessageBox::warning(this,QStringLiteral("未修改"),QStringLiteral("未修改，无需保存！"),
                             QMessageBox::Ok);
        break;
    case -1:
    {
        if(_fileName.isEmpty())
        {
            _fileName=QInputDialog::getText(this,QStringLiteral("输入文件名"),QStringLiteral("文件名:"));
            QString desktopPath=QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
            _fileName=desktopPath +"/" +  _fileName + QString(".json");
        }
        helper->setFileName(_fileName);
        helper->save();
        saveFlag=1;
        QString word=QStringLiteral("文件已保存，具体位置： \n") +  _fileName;
        QMessageBox::information(this,QStringLiteral("已保存"),word,
                                 QMessageBox::Ok);
        break;
    }
    case 1:
        QMessageBox::warning(this,QStringLiteral("未修改"),QStringLiteral("未修改，无需保存！"),
                             QMessageBox::Ok);
        break;
    }
}

void Dialog::on_btn_clear_clicked()
{
    saveFlag=0;
    ui->head->clear();
    ui->tail->clear();
    ui->key->clear();
    ui->value->clear();

    _fileName.clear();
    headList.clear();
    model->clear();
}
