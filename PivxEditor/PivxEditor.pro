#-------------------------------------------------
#
# Project created by QtCreator 2016-06-20T19:16:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PivxEditor
TEMPLATE = app
INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        dialog.cpp \
    ../../elfproj/support/pivx.cpp \
    ../../elfproj/model/pivxmodel.cpp \
    ../../elfproj/support/piv.cpp

HEADERS  += dialog.h \
    ../../elfproj/support/pivx.h \
    ../../elfproj/model/pivxmodel.h \
    ../../elfproj/support/piv.h \
    g_ver.h

FORMS    += dialog.ui
