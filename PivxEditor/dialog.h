﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QStandardPaths>

#include "model/pivxmodel.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_btn_add_clicked();

    void on_btn_open_clicked();

    void on_btn_del_clicked();

    void on_btn_save_clicked();

    void on_btn_clear_clicked();

private:
    Ui::Dialog *ui;

    PivxModel *helper;

    QString _fileName;

    QString head,tail;
    QStringList headList;
    QString key,value;

    int saveFlag;       // 0 未修改，-1 未保存，1已保存
    QStandardItemModel *model;
};

#endif // DIALOG_H
