QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QuickLocate
TEMPLATE = app

CONFIG += c++11
INCLUDEPATH += ../../elfproj
include(../qtsingleapplication/src/qtsingleapplication.pri)

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
    ../../elfproj/support/sp_env.cpp \
    main.cpp \
    widget.cpp

HEADERS += \
    ../../elfproj/support/pie.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/common/baseeditor.h \
    g_ver.h \
    m_fhs.h \
    widget.h

FORMS += \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc

RC_ICONS = locate.ico
