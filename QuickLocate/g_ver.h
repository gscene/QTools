﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QuickLocate"
#define SP_VER 1005
#define SP_UID "{f8ee0d9a-302a-4922-a837-e81d4d0b5041}"
#define SP_TYPE "prop"

#define SP_CFG "QuickLocate.ini"
#define SP_INFO "QuickLocate.json"
#define SP_LINK "QuickLocate.lnk"

/*
 * 1002 基本功能
 * 1003 焦点功能
 * 1004 使用SP_INFO
 * 1005 quickConnectFromSHM
*/

#endif // G_VER_H
