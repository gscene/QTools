#ifndef WIDGET_H
#define WIDGET_H

#include "common/baseeditor.h"
#include "support/pie.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void loadConfig();
    void query(const QString &kw);
    void showConfigException();

protected:
    bool eventFilter(QObject *target, QEvent *event);

private slots:
    void on_btn_paste_clicked();
    void on_kw_returnPressed();

private:
    Ui::Widget *ui;

    int columnIndex;    // column 列索引
    QString column;

    QStringList columns;
    QSet<int> hideColumns;
};
#endif // WIDGET_H
