﻿#ifndef VIEWER_H
#define VIEWER_H

#include "head/g_pch.h"

namespace Ui {
class Viewer;
}

class Viewer : public QDialog
{
    Q_OBJECT

public:
    explicit Viewer(QWidget *parent);
    ~Viewer();

    void setFileInfo(const QFileInfo &fileInfo);
    QString fileName() const;
    QStringList getFilter() const;
    void playNext();
    void playPrevious();
    void play(const QFileInfo &fileInfo);

protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    Ui::Viewer *ui;

    QStringList _nameFilter;
    QFileInfo locator;
    QFileInfoList fileList;

    QPoint beginPoint;
    QPoint endPoint;
};

#endif // VIEWER_H
