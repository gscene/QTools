#-------------------------------------------------
#
# Project created by QtCreator 2017-01-28T15:27:01
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = PictureFolder
TEMPLATE = app

INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common
include(../qtsingleapplication/src/qtsingleapplication.pri)

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
        dialog.cpp \
    viewer.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += dialog.h \
    g_ver.h \
    viewer.h \
    ../../elfproj/support/sp_env.h \
    ../common/m_fhs.h \
    ../common/mediafolder.h

FORMS    += dialog.ui \
    viewer.ui

RESOURCES += \
    res.qrc

RC_ICONS = photo.ico
