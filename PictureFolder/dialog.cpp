﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent,
               const MediaType &type)
    : MediaFolder(parent,type),
      ui(new Ui::Dialog)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Dialog);
    ui->treeView->setModel(model);

    viewer=new Viewer(this);
    filter=viewer->getFilter();

    loadLocation();
    createMenu();
    updateView();
}

Dialog::~Dialog()
{
    saveLocation();
    delete ui;
}

void Dialog::updateView()
{
    model->setRootPath(currentLocation);
    model->setNameFilters(filter);

    ui->treeView->setRootIndex(model->index(currentLocation));
    ui->treeView->hideColumn(1);
    ui->treeView->hideColumn(2);
    ui->treeView->hideColumn(3);
    ui->treeView->setHeaderHidden(true);
}

void Dialog::on_treeView_doubleClicked(const QModelIndex &index)
{
    if(!model->isDir(index))
    {
        viewer->move(this->x() + this->width() + 20,this->y());
        viewer->setFileInfo(model->fileInfo(index));
    }
}
