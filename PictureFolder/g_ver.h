﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "PictureFolder"
#define SP_VER 1005
#define SP_UID "{78002bac-6106-4453-8983-7ba1cc9c3e06}"
#define SP_TYPE "shell"

#define SP_CFG "PictureFolder.ini"
#define SP_INFO "PictureFolder.json"
#define SP_LINK "PictureFolder.lnk"

/*
 * 1001
 * 1002 增加了手势操作
 * 1003 增加了滚轮操作
 * 1005 MediaFolder
*/

#endif // G_VER_H
