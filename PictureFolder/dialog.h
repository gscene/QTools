﻿#ifndef DIALOG_H
#define DIALOG_H

#include "mediafolder.h"
#include "viewer.h"

namespace Ui {
class Dialog;
}

class Dialog : public MediaFolder
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0,
                    const MediaType &type=MT_Picture);
    ~Dialog();

    void updateView();

private slots:
    void on_treeView_doubleClicked(const QModelIndex &index);

private:
    Ui::Dialog *ui;

    Viewer *viewer;
};

#endif // DIALOG_H
