﻿#include "viewer.h"
#include "ui_viewer.h"

Viewer::Viewer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Viewer)
{
    ui->setupUi(this);

    _nameFilter << "*.png" << "*.bmp" << "*.gif"
                << "*.jpg" << "*.jpeg" << "*.jp2"
                << "*.dds" << "*.tga"
                << "*.tif" << "*.tiff";
}

Viewer::~Viewer()
{
    delete ui;
}

void Viewer::wheelEvent(QWheelEvent *event)
{
    QPoint value=event->angleDelta();

    //up
    if(value.y() > 0)
        playPrevious();

    //down
    else if(value.y() < 0)
        playNext();
}

void Viewer::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Left:
        playPrevious();
        break;
    case Qt::Key_Right:
        playNext();
        break;
    case Qt::Key_Up:
        playPrevious();
        break;
    case Qt::Key_Down:
        playNext();
        break;
    }
}

void Viewer::mousePressEvent(QMouseEvent *event)
{
    //    if (event->button() == Qt::LeftButton) {}
    beginPoint = event->pos();
}

void Viewer::mouseReleaseEvent(QMouseEvent *event)
{
    if(beginPoint.isNull())
        return;

    //   if (event->button() == Qt::LeftButton) {}
    endPoint = event->pos();

    QPoint delta=endPoint - beginPoint;
    if(delta.isNull())  //(0,0)
        return;

    // 右下
    if( delta.x() > 0 && delta.y() >= 0)
        playNext();
    // 右上
    else if( delta.x() > 0 && delta.y() < 0)
        playNext();
    //左上
    else if( delta.x() <= 0 && delta.y() < 0)
        playPrevious();
    // 左下
    else if( delta.x() <= 0 && delta.y() > 0 )
        playPrevious();
}

void Viewer::play(const QFileInfo &fileInfo)
{
    setWindowTitle(fileInfo.fileName());

    QString filePath=fileInfo.filePath();
    if(fileInfo.suffix() == "gif")
    {
        QMovie *movie=new QMovie(filePath);
        if(!movie->isValid())
        {
            delete movie;
            return;
        }

        ui->display->setMovie(movie);
        connect(movie,&QMovie::finished,movie,&QMovie::deleteLater);
        movie->start();
    }
    else
    {
        QPixmap pix;
        if(!pix.load(filePath))
            return;

        if(pix.height() > ui->display->height())
            ui->display->setPixmap(pix.scaledToHeight(ui->display->height()));
        else if(pix.width() > ui->display->width())
            ui->display->setPixmap(pix.scaledToWidth(ui->display->width()));
        else
            ui->display->setPixmap(pix);
    }

    if(!isVisible())
        show();
}

void Viewer::setFileInfo(const QFileInfo &fileInfo)
{
    locator=fileInfo;
    play(locator);

    if(!fileList.contains(locator))
    {
        QDir dir=locator.absoluteDir();
        fileList=dir.entryInfoList(_nameFilter,QDir::Files);
    }
}

QStringList Viewer::getFilter() const
{
    return _nameFilter;
}

void Viewer::playNext()
{
    if(!fileList.contains(locator))
        return;

    int index=fileList.indexOf(locator);
    if(index < fileList.size() -1)
        locator=fileList.at(index + 1);
    else if(index == fileList.size() -1)
        locator=fileList.first();

    play(locator);
}

void Viewer::playPrevious()
{
    if(!fileList.contains(locator))
        return;

    int index=fileList.indexOf(locator);
    if(index > 0)
        locator=fileList.at(index - 1);
    else if(index == 0)
        locator=fileList.last();

    play(locator);
}
