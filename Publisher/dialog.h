#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTextBrowser>
#include <QDebug>

#include "support/ref_mosquitto.h"
#include "support/simplepublisher.h"
#include "support/simplesubscriber.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void onReceived(const QMQTT::Message& message);

private slots:
    void on_btn_publish_clicked();

private:
    Ui::Dialog *ui;
    SimplePublisher publisher;
    SimpleSubscriber subscriber;
};

#endif // DIALOG_H
