#-------------------------------------------------
#
# Project created by QtCreator 2017-05-16T19:55:41
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Publisher
TEMPLATE = app

INCLUDEPATH += ../../elfproj

INCLUDEPATH +=D:/usr/include/qmqtt
win32:CONFIG(release, debug|release): LIBS += -LD:/usr/lib/release/ -lqmqtt
else:win32:CONFIG(debug, debug|release): LIBS += -LD:/usr/lib/debug/ -lqmqtt

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += main.cpp\
        dialog.cpp

HEADERS  += dialog.h \
    ../../elfproj/support/simplepublisher.h \
    ../../elfproj/support/simplesubscriber.h

FORMS    += dialog.ui
