#-------------------------------------------------
#
# Project created by QtCreator 2018-01-05T22:36:17
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = IconSet
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += \
        main.cpp \
        widget.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h

FORMS += \
        widget.ui
