﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "IconSet"
#define SP_VER 1002
#define SP_UID "{17b5af08-7172-41d9-825a-f4f9c544be17}"
#define SP_TYPE "prop"

#define SP_CFG "IconSet.ini"
#define SP_INFO "IconSet.json"
#define SP_LINK "IconSet.lnk"


#endif // G_VER_H
