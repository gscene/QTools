﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Bookmarks"
#define SP_VER 1008
#define SP_UID "{7847c73a-3910-484d-8da6-a5efeb5ed400}"
#define SP_TYPE "shell"

#define SP_CFG "Bookmarks.ini"
#define SP_INFO "Bookmarks.json"
#define SP_LINK "Bookmarks.lnk"

/*
 * 1001 项目开始
 * 1002 功能完善
 * 1003 在离线修正
 * 1004 搜索功能
 * 1005 搜索修正
 * 1006 代码精简
 * 1007 支持打开位置
*/

#endif // G_VER_H
