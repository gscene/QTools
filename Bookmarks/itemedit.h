﻿#ifndef ITEMEDIT_H
#define ITEMEDIT_H

#include "common/baseeditor_dialog.h"

namespace Ui {
class ItemEdit;
}

class ItemEdit : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit ItemEdit(QWidget *parent, const QString &table,bool onlineFlag);
    ~ItemEdit();

    void save();
    void updateView();
    void generateMenu();
    void removeItem();

private:
    Ui::ItemEdit *ui;

    bool isOnline;
    QString _table;
};

#endif // ITEMEDIT_H
