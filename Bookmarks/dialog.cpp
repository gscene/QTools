﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    ItemBaseEditor(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog);

    isOnline=true;
    table=TD_BOOKMARKS;
    itemModel=new QStandardItemModel(this);
    ui->treeView->setModel(itemModel);
    ui->treeView->setEditTriggers(QTreeView::NoEditTriggers);
    detectStorage();
    updateView();
    createMenu();
    loadLocation();
}

Dialog::~Dialog()
{
    if(db.isOpen())
        db.close();

    saveLocation();
    delete ui;
}

void Dialog::detectStorage()
{
    if(QFile::exists(M_CONFIG))
    {
        QSettings cfg(M_CONFIG,QSettings::IniFormat);
        if(cfg.value("Main/local").toInt() == 1)
        {
            isOnline=false;
            table=LOCAL_ITEM;
        }
    }
    if(isOnline)
        db=sp_createStorage(DB_CONFIG);
    else
        createLocalStorage();
}

void Dialog::createLocalStorage()
{
    QString dbName=Location::document + DB_FILE;
    db=sp_createLocalStorage(dbName,1);

    if(!QFile::exists(dbName))
    {
        if(db.open())
        {
            QSqlQuery query;
            QString sql=QString(CREATE_ITEM).arg(table);
            if(!query.exec(sql))
            {
                QMessageBox::warning(this,QStringLiteral("异常情况"),
                                     QStringLiteral("无法创建本地数据库"));
                qDebug() << query.lastError().text();
            }
        }
        else
            QMessageBox::warning(this,QStringLiteral("异常情况"),
                                 QStringLiteral("无法打开本地数据库"));
    }
}

bool Dialog::db_try()
{
    if(!db.isOpen())
    {
        if(!db.open())
        {
            QMessageBox::warning(this,QStringLiteral("异常情况"),
                                 QStringLiteral("无法打开数据库"));
            return false;
        }
        else
            return true;
    }
    else
        return true;
}

void Dialog::updateView()
{
    if(!db_try())
        return;

    updateView_One();
    ui->treeView->resizeColumnToContents(0);
}

void Dialog::generateMenu()
{
    menu->addAction(QStringLiteral("添加..."),this,&Dialog::newItem);
    menu->addAction(QStringLiteral("搜索 (F2)"),this,&Dialog::search);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新"),this,&Dialog::updateView);
    menu->addAction(QStringLiteral("全部折叠"),this,&Dialog::collapseAll);
    menu->addSeparator();
    menu->addAction(QStringLiteral("打开编辑器"),this,&Dialog::editItem);
}

void Dialog::collapseAll()
{
    ui->treeView->collapseAll();
}

void Dialog::newItem()
{
    ItemAdd itemAdd(this,table);
    itemAdd.move(this->x() + this->width() + 20,this->y());

    QModelIndex index=ui->treeView->currentIndex();
    if(getCategoryByIndex(index))
        itemAdd.setCategory(category);

    if(itemAdd.exec() == QDialog::Accepted)
        updateView();
}

void Dialog::editItem()
{
    ItemEdit itemEdit(this,table,isOnline);
    itemEdit.move(x() + width() + 20, y());
    if(itemEdit.exec() == QDialog::Accepted)
        updateView();
}

void Dialog::on_treeView_doubleClicked(const QModelIndex &index)
{
    if(!getItemByIndex(index))
        return;

    if(getDetail())
    {
        if(detail.isEmpty())
            return;

        QDesktopServices::openUrl(QUrl(detail));
    }
}
