﻿#ifndef ITEMADD_H
#define ITEMADD_H

#include "head/g_pch.h"

namespace Ui {
class ItemAdd;
}

class ItemAdd : public QDialog
{
    Q_OBJECT

public:
    explicit ItemAdd(QWidget *parent, const QString &table);
    ~ItemAdd();

    void setCategory(const QString &category);
    bool addItem(const QString &category,
                 const QString &label,
                 const QString &detail);

private slots:
    void on_btn_submit_clicked();

    void on_btn_location_clicked();

private:
    Ui::ItemAdd *ui;

    QString _table;
};

#endif // ITEMADD_H
