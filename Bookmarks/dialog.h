﻿#ifndef DIALOG_H
#define DIALOG_H

#include "itembaseeditor.h"
#include "itemadd.h"
#include "itemedit.h"
#include "head/g_functionbase.h"

#define LOCAL_ITEM "bookmarks"
#define DB_FILE "/QVertex/bookmarks.db"

#define PRE_HTTP "http"

namespace Ui {
class Dialog;
}

class Dialog : public ItemBaseEditor
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent=0);
    ~Dialog();

    void updateView();
    void generateMenu();
    void newItem();
    void editItem();
    void collapseAll();

    void detectStorage();
    void createLocalStorage();
    bool db_try();

private slots:
    void on_treeView_doubleClicked(const QModelIndex &index);

private:
    Ui::Dialog *ui;

    bool isOnline;
    QSqlDatabase db;
};

#endif // DIALOG_H
