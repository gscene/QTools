﻿#include "itemadd.h"
#include "ui_itemadd.h"

ItemAdd::ItemAdd(QWidget *parent, const QString &table) :
    QDialog(parent),
    _table(table),
    ui(new Ui::ItemAdd)
{
    ui->setupUi(this);
}

ItemAdd::~ItemAdd()
{
    delete ui;
}

void ItemAdd::setCategory(const QString &category)
{
    ui->category->setText(category);
}

bool ItemAdd::addItem(const QString &category,
                      const QString &label, const QString &detail)
{
    QSqlQuery query;
    query.prepare(QString("insert into %1 (category,label,detail) values (?,?,?)").arg(_table));
    query.addBindValue(category);
    query.addBindValue(label);
    query.addBindValue(detail);

    if(query.exec())
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void ItemAdd::on_btn_submit_clicked()
{
    QString category=ui->category->text().trimmed();
    if(category.isEmpty())
        return;

    QString label=ui->label->text().trimmed();
    if(label.isEmpty())
        return;

    QString detail=ui->detail->text().trimmed();
    if(detail.isEmpty())
        return;

    if(!addItem(category,label,detail))
        QMessageBox::warning(this,QStringLiteral("异常情况"),QStringLiteral("无法提交数据！"));
    else
        this->accept();
}

void ItemAdd::on_btn_location_clicked()
{
    QUrl url=QFileDialog::getExistingDirectoryUrl(this);
    if(url.isEmpty())
        return;

    ui->detail->setText(url.toString());
}
