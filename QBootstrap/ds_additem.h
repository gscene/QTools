﻿#ifndef DS_ADDITEM_H
#define DS_ADDITEM_H

#include <head/g_pch.h>
#include <head/m_message.h>
#include "m_fhs.h"

namespace Ui {
class DS_AddItem;
}

class DS_AddItem : public QDialog
{
    Q_OBJECT

public:
    explicit DS_AddItem(QWidget *parent = nullptr);
    ~DS_AddItem();

    bool addItem(const QString &entry,
                 const QString &host,
                 int port,
                 const QString &user,
                 const QString &password,
                 const QString &dbName);

private slots:
    void on_btn_submit_clicked();

private:
    Ui::DS_AddItem *ui;

    QString table;
};

#endif // DS_ADDITEM_H
