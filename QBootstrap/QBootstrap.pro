QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
    ../../elfproj/support/sp_env.cpp \
    ds_additem.cpp \
    dseditor.cpp \
    main.cpp \
    bootstrap.cpp

HEADERS += \
    ../../elfproj/common/baseeditordialog.h \
    ../../elfproj/element/baseudpreceiver.h \
    ../../elfproj/head/g_functionbase.h \
    ../../elfproj/support/sp_env.h \
    bootstrap.h \
    ds_additem.h \
    dseditor.h \
    g_ver.h \
    m_fhs.h

FORMS += \
    bootstrap.ui \
    ds_additem.ui \
    dseditor.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc

RC_ICONS = home.ico
