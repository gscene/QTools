﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define P_DATASOURCE "p_datasource"

#define COL_ID 0
#define COL_ENTRY 1
#define COL_HOST 2
#define COL_PORT 3
#define COL_USER 4
#define COL_PASSWD 5
#define COL_DB 6

#endif // M_FHS_H
