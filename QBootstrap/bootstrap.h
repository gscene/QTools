﻿#ifndef BOOTSTRAP_H
#define BOOTSTRAP_H

#include <head/g_pch.h>
#include <element/baseudpreceiver.h>
#include <head/m_message.h>
#include "g_ver.h"
#include "dseditor.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Bootstrap; }
QT_END_NAMESPACE

class Bootstrap : public QWidget
{
    Q_OBJECT

public:
    Bootstrap(QWidget *parent = nullptr);
    ~Bootstrap();

    void fetchData();
    void updateData(const QString &entry);
    void updateToMemory();
    void createTray();
    void toggleDSEditor();
    void onSourceChanged();

    void appendLog(const QString &event);

    void onReceived(const QByteArray data);
    void trayActivated(QSystemTrayIcon::ActivationReason reason);
    void toggleState();

protected:
    void timerEvent(QTimerEvent *);

private:
    Ui::Bootstrap *ui;

    QSystemTrayIcon *tray;
    QMenu *trayMenu;
    DSEditor *dsEditor;
    QString dsTable;
    QSharedMemory *sharedMemory;

    BaseUdpReceiver *receiver;
    QThread recvThread;

    QMap<QString,QJsonObject> dataSource;   //JSON
};
#endif // BOOTSTRAP_H
