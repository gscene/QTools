﻿#ifndef DSEDITOR_H
#define DSEDITOR_H

#include <common/baseeditordialog.h>
#include "ds_additem.h"

namespace Ui {
class DSEditor;
}

class DSEditor : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit DSEditor(QWidget *parent = nullptr);
    ~DSEditor();

    void generateMenu();
    void updateView();
    void newItem();
    void removeItem();

signals:
    void sourceChanged();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    Ui::DSEditor *ui;
};

#endif // DSEDITOR_H
