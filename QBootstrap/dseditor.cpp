﻿#include "dseditor.h"
#include "ui_dseditor.h"

DSEditor::DSEditor(QWidget *parent) :
    BaseEditorDialog(parent),
    ui(new Ui::DSEditor)
{
    ui->setupUi(this);

    table=P_DATASOURCE;

    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);
    ui->tableView->setEditTriggers(QTableView::DoubleClicked);

    createMenu();
    updateView();
}

DSEditor::~DSEditor()
{
    delete ui;
}

void DSEditor::updateView()
{
    model->setTable(table);
    model->select();

    ui->tableView->hideColumn(COL_ID);   //id
}

void DSEditor::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::ControlModifier && event->key()==Qt::Key_S)
        save();
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_Z)
        revert();
    else
    {
        switch (event->key()) {
        case Qt::Key_F1:
            newItem();
            break;
        case Qt::Key_F5:
            updateView();
            break;
        case Qt::Key_Delete:
            removeItem();
            break;
        }
    }
}

void DSEditor::generateMenu()
{
    menu->addAction(QStringLiteral("新条目... (F1)"),this,&DSEditor::newItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新 (F5)"),this,&DSEditor::updateView);
    menu->addSeparator();
    QMenu *optMenu=menu->addMenu("选项");
    optMenu->addAction("删除 (Delete)",this,&DSEditor::removeItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("撤销 (Ctrl+Z)"),this,&DSEditor::revert);
    menu->addAction(QStringLiteral("保存 (Ctrl+S)"),this,&DSEditor::save);

}

void DSEditor::newItem()
{
    DS_AddItem itemAdd(this);
    if(itemAdd.exec() == QDialog::Accepted)
    {
        updateView();
        emit sourceChanged();
    }
}

void DSEditor::removeItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
        model->removeRow(index.row());
}
