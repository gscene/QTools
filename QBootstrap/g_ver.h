﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QBootstrap"
#define SP_VER 1006
#define SP_UID "{6eff9548-1c5e-4380-9563-a228eee1c9ff}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "QBootstrap.ini"
#define SP_INFO "QBootstrap.json"
#define SP_LINK "QBootstrap.lnk"

/*
 * 1002 基本功能
 * 1003 DSEditor,DS_AddItem
 * 1004 onSourceChanged,toggleDSEditor,toggleState
 * 1005 DSItem
 * 1006 精简
*/
#endif // G_VER_H
