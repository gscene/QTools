QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
    ../../elfproj/support/sp_env.cpp \
    addframe.cpp \
    main.cpp \
    sectiondialog.cpp \
    sitedialog.cpp \
    widget.cpp

HEADERS += \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/common/baseeditordialog.h \
    ../../elfproj/support/sp_env.h \
    addframe.h \
    frame.h \
    g_ver.h \
    m_fhs.h \
    section.h \
    sectiondialog.h \
    site.h \
    sitedialog.h \
    widget.h

FORMS += \
    addframe.ui \
    sectiondialog.ui \
    sitedialog.ui \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
