#include "sectiondialog.h"
#include "ui_sectiondialog.h"

SectionDialog::SectionDialog(QWidget *parent) :
    BaseEditorDialog(parent), ui(new Ui::SectionDialog)
{
    ui->setupUi(this);

    table=TD_SECTION;

    model=new QSqlTableModel(this);
    ui->tableView->setModel(model);

    createMenu();
    updateView();
}

SectionDialog::~SectionDialog()
{
    delete ui;
}

void SectionDialog::updateView()
{
    model->setTable(table);
    model->select();

    model->setHeaderData(0,Qt::Horizontal,QStringLiteral("ID"));
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("Name"));
}

void SectionDialog::edit_on()
{
    isEdit=true;
    ui->tableView->setEditTriggers(QTableView::DoubleClicked);
    updateMenu();
}

void SectionDialog::edit_off()
{
    isEdit=false;
    ui->tableView->setEditTriggers(QTableView::NoEditTriggers);
    updateMenu();
}

void SectionDialog::updateMenu()
{
    if(!menu->isEmpty())
        menu->clear();

    if(isEdit)
    {
        menu->addAction(QStringLiteral("撤销 (Ctrl+Z)"),this,&SectionDialog::revert);
        menu->addAction(QStringLiteral("保存 (Ctrl+S)"),this,&SectionDialog::save);
        menu->addSeparator();
        menu->addAction(QStringLiteral("删除 (Delete)"),this,&SectionDialog::removeItem);
        menu->addSeparator();
        menu->addAction(QStringLiteral("退出编辑模式"),this,&SectionDialog::edit_off);
    }
    else
        generateMenu();
}

void SectionDialog::generateMenu()
{
    menu->addAction("选择",this,&SectionDialog::pickItem);
    menu->addSeparator();
    auto *optMenu=menu->addMenu("选项");
    optMenu->addAction(QStringLiteral("撤销 (Ctrl+Z)"),this,&SectionDialog::revert);
    optMenu->addAction(QStringLiteral("保存 (Ctrl+S)"),this,&SectionDialog::save);
    optMenu->addSeparator();
    optMenu->addAction(QStringLiteral("删除 (Delete)"),this,&SectionDialog::removeItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("进入编辑模式"),this,&SectionDialog::edit_on);
}

void SectionDialog::pickItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        Section item;
        item.id=sp_fetchId(index);
        item.name=sp_fetchString(index,1);
        picked(item);
        accept();
    }
}

void SectionDialog::removeItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
        model->removeRow(index.row());
}

bool SectionDialog::addItem(const QString &section)
{
    QSqlQuery query;
    QString sql=QString("insert into %1 (name) values ('%2')")
            .arg(table).arg(section);
    return query.exec(sql);
}

void SectionDialog::on_btn_add_clicked()
{
    QString section=ui->section->text().trimmed();
    if(section.isEmpty())
        return;

    if(addItem(section))
    {
        ui->section->clear();
        updateView();
    }
}


void SectionDialog::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(!isEdit && index.isValid())
    {
        Section item;
        item.id=sp_fetchId(index);
        item.name=sp_fetchString(index,1);
        picked(item);
        accept();
    }
}

