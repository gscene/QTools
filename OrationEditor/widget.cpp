#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    BaseEditor(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    table=TD_ARTICLE;

    tableModel=new QStandardItemModel(this);
    tableModel->setColumnCount(2);
    setHeaderData();
    createMenu();

    ui->tableView->setModel(tableModel);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::generateMenu()
{
    menu->addAction("添加 (F1)",this,&Widget::newItem);
    menu->addSeparator();
    menu->addAction("编辑 (F3)",this,&Widget::editItem);
    menu->addSeparator();
    menu->addAction("更新 (F5)",this,&Widget::updateView);
    menu->addSeparator();
    QMenu *opt=menu->addMenu("选项");
    opt->addAction(QStringLiteral("删除 (Delete)"),this,&Widget::removeItem);
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_F1:
        newItem();
        break;
    case Qt::Key_F3:
        editItem();
        break;
    case Qt::Key_F5:
        updateView();
        break;
    case Qt::Key_Delete:
        removeItem();
        break;
    }
}

void Widget::newItem()
{
    AddFrame dialog;
    dialog.setSection(pSection);
    connect(&dialog,&AddFrame::add,this,&Widget::onAddItem);
    dialog.exec();
}

void Widget::editItem()
{
    if(!pFrame.isValid())
        return;

    AddFrame dialog;
    dialog.setSection(pSection);
    dialog.setItem(pFrame);
    dialog.exec();
}

void Widget::removeItem()
{
    auto index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        auto result=MESSAGE_DELETE_CONFIRM
                if(result != QMessageBox::Yes)
                return;

        qint64 seq=sp_fetchData(index,0).toLongLong();
        QSqlQuery query;
        auto sql=QString("delete from %1 where seq=%2")
                .arg(table).arg(seq);
        if(query.exec(sql))
            updateView();
    }
}

void Widget::setHeaderData()
{
    tableModel->setHeaderData(0,Qt::Horizontal,QStringLiteral("序列"));
    tableModel->setHeaderData(1,Qt::Horizontal,QStringLiteral("标题"));
    ui->tableView->setColumnWidth(0,180);
}

void Widget::onPicked(const Section &item)
{
    pSection=item;
    if(pSection.id == 0)
        return;

    setWindowTitle(QString("Oration - %1").arg(pSection.name));

    updateView();
}

void Widget::updateView()
{
    if(tableModel->rowCount() != 0)
        tableModel->clear();

    QSqlQuery query;
    auto sql=QString("select seq,title from %1 where section_id = %2")
            .arg(table)
            .arg(pSection.id);
    query.exec(sql);
    while (query.next()) {
        auto seq=query.value(0).toString();
        auto title=query.value(1).toString();
        tableModel->appendRow({new QStandardItem(seq),
                               new QStandardItem(title)});
    }
    setHeaderData();
}

void Widget::onAddItem(const QString &seq,
                       const QString &title)
{
    tableModel->appendRow({new QStandardItem(seq),
                           new QStandardItem(title)});
}

void Widget::on_btn_section_clicked()
{
    SectionDialog dialog;
    connect(&dialog,&SectionDialog::picked,
            this,&Widget::onPicked);
    dialog.exec();
}


void Widget::on_tableView_clicked(const QModelIndex &index)
{
    pFrame.seq=sp_fetchData(index,0).toLongLong();
    pFrame.title=sp_fetchData(index,1).toString();
    QSqlQuery query;
    auto sql=QString("select id,subtitle,prompt from %1 where seq=%2")
            .arg(table)
            .arg(pFrame.seq);
    query.exec(sql);
    if(query.next())
    {
        pFrame.id=query.value(0).toInt();
        pFrame.subtitle=query.value(1).toString();
        pFrame.prompt=query.value(2).toString();

        if(!pFrame.subtitle.isEmpty())
            ui->subtitle->setPlainText(pFrame.subtitle);

        if(!pFrame.prompt.isEmpty())
            ui->prompt->setPlainText(pFrame.prompt);
    }
}


