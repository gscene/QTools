#ifndef SECTIONDIALOG_H
#define SECTIONDIALOG_H

#include <head/g_pch.h>
#include <head/db_helper.h>
#include <head/m_message.h>
#include "m_fhs.h"
#include "section.h"
#include <common/baseeditordialog.h>

namespace Ui {
class SectionDialog;
}

class SectionDialog : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit SectionDialog(QWidget *parent = nullptr);
    ~SectionDialog();

    void updateView();
    void generateMenu();
    void updateMenu();

    void edit_on();
    void edit_off();
    void removeItem();
    bool addItem(const QString &section);
    void pickItem();

signals:
    void picked(const Section &item);

private slots:
    void on_btn_add_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    Ui::SectionDialog *ui;

    QString table;
};

#endif // SECTIONDIALOG_H
