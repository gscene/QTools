#ifndef FRAME_H
#define FRAME_H

#include <QtGlobal>
#include <QString>
#include <QDateTime>

struct Frame
{
    int id;
    qint64 seq;
    QString title;
    QString subtitle;
    QString prompt;

    Frame()
    {
        id=0;
        seq=QDateTime::currentDateTime().toMSecsSinceEpoch();
    }

    bool isValid()
    {
        if(title.isEmpty() || subtitle.isEmpty() || prompt.isEmpty())
            return false;
        else
            return true;
    }

    bool isEqual(const Frame &item)
    {
        if(title == item.title && subtitle == item.subtitle && prompt == item.prompt)
            return true;
        else
            return false;
    }
};

#endif // FRAME_H
