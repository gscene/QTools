#ifndef ADDFRAME_H
#define ADDFRAME_H

#include <head/g_pch.h>
#include <head/db_helper.h>
#include <head/m_message.h>
#include "m_fhs.h"
#include "frame.h"
#include "section.h"
#include "sitedialog.h"

namespace Ui {
class AddFrame;
}

class AddFrame : public QDialog
{
    Q_OBJECT

public:
    explicit AddFrame(QWidget *parent = nullptr);
    ~AddFrame();

    void setSection(const Section &section);
    void setItem(const Frame &item);
    bool addItem(const Frame &item);
    bool updateItem(const Frame &item);

signals:
    void add(const QString &seq,const QString &title);

private slots:
    void on_btn_submit_clicked();

    void on_btn_ref_clicked();

private:
    Ui::AddFrame *ui;

    QString table;
    Frame pItem;
    Section pSection;
    SiteDialog *siteDialog;
};

#endif // ADDFRAME_H
