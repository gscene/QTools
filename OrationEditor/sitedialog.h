#ifndef SITEDIALOG_H
#define SITEDIALOG_H

#include <head/g_pch.h>
#include <head/db_helper.h>
#include <head/m_message.h>
#include "m_fhs.h"
#include <common/baseeditordialog.h>
#include "site.h"

namespace Ui {
class SiteDialog;
}

class SiteDialog : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit SiteDialog(QWidget *parent = nullptr);
    ~SiteDialog();

    void updateView();
    void generateMenu(){}
    void setId(int id);
    bool addItem(const Site &item);

private slots:
    void on_btn_paste_label_clicked();

    void on_btn_paste_url_clicked();

    void on_btn_submit_clicked();

private:
    Ui::SiteDialog *ui;

    int article_id;
};

#endif // SITEDIALOG_H
