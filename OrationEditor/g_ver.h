#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "OrationEditor"
#define SP_VER 1006
#define SP_UID "{1a690279-4603-4be9-98a2-7f9a0551661c}"
#define SP_TYPE "prop"

#define SP_CFG "OrationEditor.ini"
#define SP_INFO "OrationEditor.json"
#define SP_LINK "OrationEditor.lnk"

#endif // G_VER_H
