#ifndef SITE_H
#define SITE_H

#include <QtGlobal>
#include <QString>

struct Site
{
    int id;
    int article_id;
    QString label;
    QString url;

    Site(){
        id=0;
        article_id=0;
    }
};

#endif // SITE_H
