#ifndef WIDGET_H
#define WIDGET_H

#include <common/baseeditor.h>
#include "addframe.h"
#include "sectiondialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void updateView();
    void generateMenu();
    void setHeaderData();

    void newItem();
    void editItem();
    void removeItem();

    void onPicked(const Section &item);
    void onAddItem(const QString &seq,const QString &title);

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_btn_section_clicked();

    void on_tableView_clicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QStandardItemModel *tableModel;

    QString table;
    Section pSection;
    Frame pFrame;
};
#endif // WIDGET_H
