﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QClipboard"
#define SP_VER 1010
#define SP_UID "{8d42b810-d7c6-4f73-a38a-4c6f68bb79e3}"
#define SP_TYPE "prop"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "QClipboard.ini"
#define SP_INFO "QClipboard.json"
#define SP_LINK "QClipboard.lnk"

/*
 * 1002 基本功能
 * 1003 增删改功能
 * 1004 快捷键
 * 1005 resizeColumnToContents,setTopic
 * 1006 do_removeTopic,updateTopicView
 * 1007 Dialog形态
 * 1008 topic更新
 * 1009 取消了addition字段，修正了确定删除时的bug
 * 1010 quickConnectFromSHM
 *
*/
#endif // G_VER_H
