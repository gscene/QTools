﻿#ifndef ADDITEM_H
#define ADDITEM_H

#include <head/g_pch.h>
#include "m_fhs.h"
#include "head/m_message.h"

namespace Ui {
class AddItem;
}

class AddItem : public QDialog
{
    Q_OBJECT

public:
    explicit AddItem(QWidget *parent,int id=-1);
    ~AddItem();

    bool addItem(const QString &topic,
                 const QString &detail);

    bool updateItem(const QString &topic,
                    const QString &detail);

    void setTopic(const QString &topic);

    void setItem(const QString &topic,
                 const QString &detail);

private slots:
    void on_btn_submit_clicked();
    void on_btn_paste_clicked();

    void on_btn_clean_clicked();

private:
    Ui::AddItem *ui;

    QString table;
    int id_;
    QString topic_,detail_,addition_;
};

#endif // ADDITEM_H
