#-------------------------------------------------
#
# Project created by QtCreator 2018-05-28T20:21:20
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QClipboard
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
        main.cpp \
        widget.cpp \
    additem.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    additem.h \
    m_fhs.h \
    ../../elfproj/common/traywidget.h

FORMS += \
        widget.ui \
    additem.ui

RESOURCES += \
    res.qrc

RC_ICONS = clipboard.ico
