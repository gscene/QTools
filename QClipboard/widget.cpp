﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent, Qt::WindowFlags f) :
    TrayWidget(parent,f),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    table=P_CLIPBOARD;
    topicModel=new QStringListModel(this);
    ui->topicView->setModel(topicModel);

    model=new QSqlTableModel(this);
    ui->detailView->setModel(model);

    updateTopicView();
    createTray(QIcon("://clipboard.ico"),SP_VERSION);
    createMenu();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::generateMenu()
{
    menu->addAction(QStringLiteral("添加... (F1)"),this,&Widget::do_addItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("编辑... (F3)"),this,&Widget::do_updateItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新主题 (F4)"),this,&Widget::updateTopicView);
    menu->addSeparator();
    menu->addAction(QStringLiteral("删除 (Delete)"),this,&Widget::do_removeItem);
    menu->addSeparator();
    QMenu *optionMenu=menu->addMenu(QStringLiteral("选项"));
    optionMenu->addAction(QStringLiteral("删除主题(慎用)"),this,&Widget::do_removeTopic);
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_F1:
        do_addItem();
        break;
    case Qt::Key_F3:
        do_updateItem();
        break;
    case Qt::Key_F4:
        updateTopicView();
        break;
    case Qt::Key_Delete:
        do_removeItem();
        break;
    case Qt::Key_Escape:
        close();
        break;
    }
}

void Widget::do_addItem()
{
    AddItem item(this);
    if(!selectTopic.isEmpty())
        item.setTopic(selectTopic);

    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Widget::do_updateItem()
{
    QModelIndex index=ui->detailView->currentIndex();
    if(index.isValid())
    {
        int id=sp_fetchId(index);
        QString topic=sp_fetchString(index,TOP_COL);
        QString detail=sp_fetchString(index,DET_COL);

        AddItem item(this,id);
        item.setItem(topic,detail);
        if(item.exec() == QDialog::Accepted)
            updateView();
    }
}

void Widget::do_removeItem()
{
    QModelIndex index=ui->detailView->currentIndex();
    if(index.isValid())
    {
        QMessageBox::StandardButton result=MESSAGE_DELETE_CONFIRM
                if(result == QMessageBox::Yes)
        {
                // qDebug() << "Function: " << __FUNCTION__;
                if(sp_removeModelIndex(table,index))
                updateView();
                else
                MESSAGE_CANNOT_DELETE
    }
    }
}

void Widget::do_removeTopic()
{
    QModelIndex index=ui->topicView->currentIndex();
    if(index.isValid())
    {
        QMessageBox::StandardButton result=MESSAGE_DELETE_CONFIRM
                if(result == QMessageBox::Yes)
        {
                QString topic=index.data().toString();
                if(topic.isEmpty())
                return;

                if(sp_removeTopic(table,topic))
                updateTopicView();
                else
                MESSAGE_CANNOT_DELETE
    }
    }
}

void Widget::updateTopicView()
{
    if(topicModel->rowCount() != 0)
        topicModel->setStringList(QStringList());

    if(!selectTopic.isEmpty())
        selectTopic.clear();

    QStringList items=sp_getTopic(table);
    topicModel->setStringList(items);

    updateView();
}

void Widget::updateView()
{
    if(model->rowCount() != 0)
        model->clear();

    if(selectTopic.isEmpty())
        return;

    model->setTable(table);
    model->setFilter(QString("topic='%1'").arg(selectTopic));
    model->select();

    ui->detailView->hideColumn(ID_COL);  //ID
    ui->detailView->hideColumn(TOP_COL);  //topic
    ui->detailView->hideColumn(LAB_COL);  //label
    model->setHeaderData(DET_COL,Qt::Horizontal,QStringLiteral("内容")); //detail
    ui->detailView->hideColumn(ADD_COL);
}

void Widget::on_topicView_clicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        selectTopic=index.data().toString();
        updateView();
    }
}

void Widget::on_detailView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QModelIndex detailIndex=
                index.row() == 3 ? index : index.sibling(index.row(),3);
        QString detail=detailIndex.data().toString();
        qApp->clipboard()->setText(detail);
    }
}
