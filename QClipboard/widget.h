﻿#ifndef WIDGET_H
#define WIDGET_H

#include <common/traywidget.h>
#include <head/db_helper.h>
#include <head/g_functionbase.h>
#include "additem.h"
#include "g_ver.h"

namespace Ui {
class Widget;
}

class Widget : public TrayWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent=nullptr,
                    Qt::WindowFlags f=Qt::Dialog);
    ~Widget();

    void generateMenu();
    void updateTopicView();
    void updateView();
    void do_addItem();
    void do_updateItem();
    void do_removeItem();
    void do_removeTopic();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_topicView_clicked(const QModelIndex &index);
    void on_detailView_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QStringListModel *topicModel;
    QSqlTableModel *model;
    QStringList detailList;

    QString table;
    QString selectTopic;
};

#endif // WIDGET_H
