#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : MediaFolder(parent,MT_Video),
      ui(new Ui::Widget)
{
    ui->setupUi(this);

    nameFilter << "*.avi" << "*.mp4"
               << "*.wmv" << "*.mov"
               << "*.mkv" << "*.m4v"
               << "*.ogg" << "*.ogv"
               << "*.flv" << "*.f4v"
               << "*.rmvb" << "*.rm"
               << "*.webm";

    ui->treeView->setModel(model);
    ui->listView->setModel(model);

    loadLocation();
    createMenu();
    updateView();

    ui->btn_continue->setEnabled(false);
    if(!isOffline())
    {
        locator=getLast();
        if(locator == QFileInfo())
            return;
        ui->last->setText(locator.fileName());
        ui->btn_continue->setEnabled(true);
    }
}

Widget::~Widget()
{
    saveLocation();
    delete ui;
}

void Widget::createMenu()
{
    menu=new QMenu(this);
    menu->addAction(QStringLiteral("打开索引文件"),this,
                    &Widget::openIndexFile);
    menu->addSeparator();
    menu->addAction(QStringLiteral("打开媒体文件夹"),this,
                    &MediaFolder::openMediaLocation);
    menu->addAction(QStringLiteral("设置媒体文件夹"),this,
                    &MediaFolder::setMediaLocation);
    menu->addSeparator();
    menu->addAction(QStringLiteral("加入收藏夹"),this,
                    &MediaFolder::addToFavorite);
    menu->addAction(QStringLiteral("添加到播放列表"),this,
                    &MediaFolder::addToPlaylist);
    menu->addSeparator();
    menu->addAction(QStringLiteral("重置媒体文件夹"),this,
                    &MediaFolder::resetMediaLocation);
}

void Widget::openIndexFile()
{
    QModelIndex index=ui->listView->currentIndex();
    if(index.isValid())
    {
        QString path;
        if(model->isDir(index))
            path=model->filePath(index);
        else
            path=model->fileInfo(index).path();

        // QString fileName=path + "/" + "index.txt";
        // player->play(fileName);
    }
}

void Widget::play(const QFileInfo &fileInfo)
{
    if(!isOffline())
        trace(fileInfo);

    ui->last->setText(fileInfo.fileName());
    if(!ui->btn_continue->isEnabled())
        ui->btn_continue->setEnabled(true);

    if(QFile::exists(fileInfo.filePath()))
    {
        if(ui->listView->rootIndex() != model->index(fileInfo.path()))
            ui->listView->setRootIndex(model->index(fileInfo.path()));

        QDesktopServices::openUrl(QUrl::fromLocalFile(fileInfo.filePath()));
    }
}

void Widget::updateView()
{
    model->setRootPath(currentLocation);
    model->setNameFilters(nameFilter);

    ui->treeView->setRootIndex(model->index(currentLocation));
    ui->treeView->hideColumn(1);
    ui->treeView->hideColumn(2);
    ui->treeView->hideColumn(3);
    ui->treeView->setHeaderHidden(true);

    ui->listView->setRootIndex(model->index(currentLocation));
}

void Widget::addToFavorite()
{
    QModelIndex index=ui->listView->currentIndex();
    if(index.isValid())
    {
        _addToFavorite(model->fileInfo(index));
    }
}

void Widget::addToPlaylist()
{
    QModelIndex index=ui->listView->currentIndex();
    if(index.isValid())
    {
        _addToPlaylist(model->fileInfo(index));
    }
}

void Widget::on_treeView_clicked(const QModelIndex &index)
{
    if(model->isDir(index))
        ui->listView->setRootIndex(index);
}

void Widget::on_listView_doubleClicked(const QModelIndex &index)
{
    if(!model->isDir(index))
    {
        locator=model->fileInfo(index);
        play(locator);
    }
}

void Widget::on_btn_continue_clicked()
{
    if(locator == QFileInfo())
        return;

    QDir dir=locator.absoluteDir();
    QFileInfoList fileList=dir.entryInfoList(nameFilter,
                                             QDir::Files,QDir::LocaleAware);
    if(fileList.size() < 1)
        return;

    int index=fileList.indexOf(locator);
    if(index < fileList.size() - 1)
        locator=fileList.at(index + 1);
    else if(index == fileList.size() - 1)
        locator=fileList.first();

    play(locator);
}