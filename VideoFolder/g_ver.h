﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "VideoFolder"
#define SP_VER 1008
#define SP_UID "{68cbb284-ac48-4370-ac2f-04044a796b52}"
#define SP_TYPE "shell"

#define SP_CFG "VideoFolder.ini"
#define SP_INFO "VideoFolder.json"
#define SP_LINK "VideoFolder.lnk"

/*
 * 1002 Location
 * 1005 MediaFolder
 * 1006 Continue
 * 1008 openIndexFile
*/

#endif // G_VER_H
