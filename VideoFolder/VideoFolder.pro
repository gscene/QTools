#-------------------------------------------------
#
# Project created by QtCreator 2017-05-20T20:24:14
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = VideoFolder
TEMPLATE = app

INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common
include(../qtsingleapplication/src/qtsingleapplication.pri)

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
        widget.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += widget.h \
    g_ver.h \
    ../common/m_fhs.h \
    ../../elfproj/support/sp_env.h \
    ../common/mediafolder.h

FORMS    += widget.ui

RC_ICONS = movie.ico

RESOURCES += \
    res.qrc
