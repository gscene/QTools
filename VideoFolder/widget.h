#ifndef WIDGET_H
#define WIDGET_H

#include "mediafolder.h"

namespace Ui {
class Widget;
}

class Widget : public MediaFolder
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void createMenu();
    void openIndexFile();
    void play(const QFileInfo &fileInfo);
    void invoke(const QString &filePath);
    void updateView();
    void addToFavorite();
    void addToPlaylist();

private slots:
    void on_treeView_clicked(const QModelIndex &index);
    void on_listView_doubleClicked(const QModelIndex &index);
    void on_btn_continue_clicked();

private:
    Ui::Widget *ui;

    QFileInfo locator;
};

#endif // WIDGET_H
